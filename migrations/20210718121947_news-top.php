<?php
class diMigration_20210718121947 extends \diCore\Database\Tool\Migration
{
	public static $idx = '20210718121947';
	public static $name = 'News: top';

	public function up()
	{
		$this->getDb()->q("ALTER TABLE news
            ADD COLUMN top tinyint default '1',
            ADD COLUMN en_top tinyint default '1'
        ");

        $this->getDb()->q("ALTER TABLE news
            DROP index idx,
            DROP index en_idx,
            ADD index idx (`season_id`, visible, top, title, order_num, date),
            ADD index en_idx (`season_id`, en_visible, en_top, en_title, order_num, date)
        ");
	}

	public function down()
	{
        $this->getDb()->q("ALTER TABLE news
            DROP COLUMN top,
            DROP COLUMN en_top
        ");
	}
}
