<?php
class diMigration_20211118092550 extends \diCore\Database\Tool\Migration
{
	public static $idx = '20211118092550';
	public static $name = 'Show: Pushkin card';

    public function up()
    {
        $this->getDb()->q("ALTER TABLE `show` ADD COLUMN pushkin_card tinyint default '0' AFTER qr");
    }

    public function down()
    {
        $this->getDb()->q("ALTER TABLE `show` DROP COLUMN pushkin_card");
    }
}