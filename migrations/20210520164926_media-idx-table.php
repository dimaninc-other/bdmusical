<?php
class diMigration_20210520164926 extends \diCore\Database\Tool\Migration
{
	public static $idx = '20210520164926';
	public static $name = 'Media idx table';

	public function up()
	{
        $this->executeSqlFile(
            'media.sql',
            dirname(\diPaths::fileSystem()) . '/vendor/big-dmitrovka/musical/sql'
        );
	}

	public function down()
	{
		$this->getDb()->q("DROP TABLE IF EXISTS media");
	}
}