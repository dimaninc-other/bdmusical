<?php
class diMigration_20211021114229 extends \diCore\Database\Tool\Migration
{
	public static $idx = '20211021114229';
	public static $name = 'show: qr';

	public function up()
	{
		$this->getDb()->q("ALTER TABLE `show` ADD COLUMN qr tinyint default '0' AFTER moved");
	}

	public function down()
	{
        $this->getDb()->q("ALTER TABLE `show` DROP COLUMN qr");
	}
}