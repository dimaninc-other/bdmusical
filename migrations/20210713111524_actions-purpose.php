<?php
class diMigration_20210713111524 extends \diCore\Database\Tool\Migration
{
	public static $idx = '20210713111524';
	public static $name = 'Actions: purpose';

	public function up()
	{
		$this->getDb()->q("ALTER TABLE action
            ADD COLUMN purpose int default 0 AFTER old_id,
            DROP INDEX idx,
            DROP INDEX en_idx,
            ADD INDEX idx (purpose, visible, title, order_num),
            ADD INDEX en_idx (purpose, en_visible, en_title, order_num)
        ");
	}

	public function down()
	{
        $this->getDb()->q("ALTER TABLE action
            DROP COLUMN purpose
        ");
	}
}
