<?php
class diMigration_20240305172227 extends \diCore\Database\Tool\Migration
{
	public static $idx = '20240305172227';
	public static $name = 'Skater, Solo, Dancer, etc: css class field added';

    protected $tables = [
        'ballet',
        'creator',
        'creative',
        'dancer',
        'musician',
        'skater',
        'solo',
    ];

	public function up()
	{
        foreach ($this->tables as $table) {
            $this->getDb()->q("ALTER TABLE $table ADD COLUMN css_class VARCHAR(100) DEFAULT '' AFTER slug_source");
        }
	}

	public function down()
	{
        foreach ($this->tables as $table) {
            $this->getDb()->q("ALTER TABLE $table DROP COLUMN css_class");
        }
	}
}