<?php
class diMigration_20210414073002 extends \diCore\Database\Tool\Migration
{
	public static $idx = '20210414073002';
	public static $name = 'Initial (do not forget to add _count triggers from core migration!!!)';

	protected $tables = [
        'action',
        'ballet',
        'character',
        'client',
        'creative',
        'creator',
        'dancer',
        'musician',
        'partner',
        'price',
        'review',
        'season',
        'show',
        'show_solo_link',
        'skater',
        'slide',
        'slide_block',
        'smi_press',
        'smi_radio',
        'smi_tv',
        'solo',
    ];

	protected $coreTables = [
        'albums',
        'content',
        'news',
        'photos',
        'videos',
    ];

	public function up()
	{
	    $addSqlExt = function ($table) {
            return $table . '.sql';
        };

        $this->executeSqlFile(
            array_map($addSqlExt, $this->tables),
            dirname(\diPaths::fileSystem()) . '/vendor/big-dmitrovka/musical/sql'
        );

        $this->executeSqlFile(
            array_map($addSqlExt, $this->coreTables),
            dirname(\diPaths::fileSystem()) . '/vendor/big-dmitrovka/musical/sql/core'
        );
	}

	public function down()
	{
        // it's unsafe to kill'em all
        /*
	    foreach (array_merge($this->tables, $this->coreTables) as $table) {
            $this->getDb()->drop($table);
        }
        */
	}
}
