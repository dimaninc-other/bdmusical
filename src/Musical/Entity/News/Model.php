<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:30
 */

namespace Musical\Entity\News;

use diCore\Admin\Submit;
use diCore\Database\FieldType;
use Musical\Traits\Model\DefaultPic;
use Musical\Traits\Model\OldIdInside;
use Musical\Traits\Model\SeasonInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getSlugSource
 * @method string	getMetaTitle
 * @method string	getMetaKeywords
 * @method string	getMetaDescription
 * @method string	getEnSlug
 * @method string	getEnSlugSource
 * @method string	getEnTitle
 * @method string	getEnShortContent
 * @method string	getEnContent
 * @method string	getEnMetaTitle
 * @method string	getEnMetaKeywords
 * @method string	getEnMetaDescription
 * @method integer	getEnVisible
 * @method string	getEnPic
 * @method integer	getEnPicW
 * @method integer	getEnPicH
 * @method integer	getEnPicT
 * @method integer	getEnPicTnW
 * @method integer	getEnPicTnH
 * @method integer	getTop
 * @method integer	getEnTop
 *
 * @method bool hasSlugSource
 * @method bool hasMetaTitle
 * @method bool hasMetaKeywords
 * @method bool hasMetaDescription
 * @method bool hasEnSlug
 * @method bool hasEnSlugSource
 * @method bool hasEnTitle
 * @method bool hasEnShortContent
 * @method bool hasEnContent
 * @method bool hasEnMetaTitle
 * @method bool hasEnMetaKeywords
 * @method bool hasEnMetaDescription
 * @method bool hasEnVisible
 * @method bool hasEnPic
 * @method bool hasEnPicW
 * @method bool hasEnPicH
 * @method bool hasEnPicT
 * @method bool hasEnPicTnW
 * @method bool hasEnPicTnH
 * @method bool hasTop
 * @method bool hasEnTop
 *
 * @method $this setSlugSource($value)
 * @method $this setMetaTitle($value)
 * @method $this setMetaKeywords($value)
 * @method $this setMetaDescription($value)
 * @method $this setEnSlug($value)
 * @method $this setEnSlugSource($value)
 * @method $this setEnTitle($value)
 * @method $this setEnShortContent($value)
 * @method $this setEnContent($value)
 * @method $this setEnMetaTitle($value)
 * @method $this setEnMetaKeywords($value)
 * @method $this setEnMetaDescription($value)
 * @method $this setEnVisible($value)
 * @method $this setEnPic($value)
 * @method $this setEnPicW($value)
 * @method $this setEnPicH($value)
 * @method $this setEnPicT($value)
 * @method $this setEnPicTnW($value)
 * @method $this setEnPicTnH($value)
 * @method $this setTop($value)
 * @method $this setEnTop($value)
 *
 * @method string	localizedSlug
 * @method string	localizedSlugSource
 * @method string	localizedTitle
 * @method string	localizedShortContent
 * @method string	localizedContent
 * @method string	localizedMetaTitle
 * @method string	localizedMetaKeywords
 * @method string	localizedMetaDescription
 * @method integer	localizedVisible
 * @method string	localizedPic
 * @method integer	localizedPicW
 * @method integer	localizedPicH
 * @method integer	localizedPicT
 * @method integer	localizedPicTnW
 * @method integer	localizedPicTnH
 * @method integer	localizedTop
 */
class Model extends \diCore\Entity\News\Model
{
    use DefaultPic;
    use SeasonInside;
    use OldIdInside;

	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'slug_source', 'title', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible', 'pic', 'pic_w', 'pic_h', 'pic_t', 'pic_tn_w', 'pic_tn_h'];

	protected static $fieldTypes = [
        'id' => FieldType::int,
        'season_id' => FieldType::int,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'title' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic_t' => FieldType::int,
        'pic_tn_w' => FieldType::int,
        'pic_tn_h' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'en_pic' => FieldType::string,
        'en_pic_w' => FieldType::int,
        'en_pic_h' => FieldType::int,
        'en_pic_t' => FieldType::int,
        'en_pic_tn_w' => FieldType::int,
        'en_pic_tn_h' => FieldType::int,
        'date' => FieldType::datetime,
        'order_num' => FieldType::int,
        'comments_count' => FieldType::int,
        'comments_last_date' => FieldType::datetime,
        'comments_enabled' => FieldType::int,
        'top' => FieldType::int,
        'en_top' => FieldType::int,
    ];

    public static function getPicStoreSettings($field)
    {
        if ($field === 'en_pic') {
            $field = 'pic';
        }

        $ar = parent::getPicStoreSettings($field);

        foreach ($ar as &$settings) {
            if ($settings['type'] == Submit::IMAGE_TYPE_PREVIEW) {
                $settings['resize'] = \diImage::DI_THUMB_CROP | \diImage::DI_THUMB_TOP;
            }
        }

        return $ar;
    }

    public function getTemplateVars()
    {
        $this->defaultPic = '/assets/images/default/press.svg';

        $this->beforeGet();

        $ar = parent::getTemplateVars();

        $this->afterGet();

        $ar['localized_pic_with_path'] = $this->wrapFileWithPath($this->localized('pic') ?: $this->getPic()) ?: $this->defaultPic;
        $ar['localized_pic_tn_with_path'] = $this->wrapFileWithPath($this->localized('pic') ?: $this->getPic(), 1) ?: $this->defaultPic;

        return $this->fixDefaultPicVars($ar);
    }
}
