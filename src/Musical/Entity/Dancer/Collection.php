<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:30
 */

namespace Musical\Entity\Dancer;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByPic2($value, $operator = null)
 * @method $this filterByPic2W($value, $operator = null)
 * @method $this filterByPic2H($value, $operator = null)
 *
 * @method $this orderByPic2($direction = null)
 * @method $this orderByPic2W($direction = null)
 * @method $this orderByPic2H($direction = null)
 *
 * @method $this selectPic2()
 * @method $this selectPic2W()
 * @method $this selectPic2H()
 */
class Collection extends \Musical\Entity\BaseEntity\Collection
{
    const type = \diTypes::dancer;
	const connection_name = 'default';
    protected $table = 'dancer';
    protected $modelType = 'dancer';
}
