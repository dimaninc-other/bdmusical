<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:30
 */

namespace Musical\Entity\Dancer;

use diCore\Database\FieldType;
use Musical\Traits\Model\CssClassInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getPic2
 * @method integer	getPic2W
 * @method integer	getPic2H
 *
 * @method bool hasPic2
 * @method bool hasPic2W
 * @method bool hasPic2H
 *
 * @method $this setPic2($value)
 * @method $this setPic2W($value)
 * @method $this setPic2H($value)
 */
class Model extends \Musical\Entity\BaseEntity\Model
{
    use CssClassInside;

    const type = \diTypes::dancer;
	const connection_name = 'default';
    const table = 'dancer';
    protected $table = 'dancer';
	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'slug_source', 'title', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible'];

	const PAGE_TYPE_FOR_HREF = 'ensemble';

	protected static $fieldTypes = [
        'id' => FieldType::int,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'css_class' => FieldType::string,
        'title' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic2' => FieldType::string,
        'pic2_w' => FieldType::int,
        'pic2_h' => FieldType::int,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];
}
