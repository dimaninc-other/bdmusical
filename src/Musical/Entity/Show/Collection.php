<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Show;

use Musical\Tool\Calendar\Date;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterByStaffStatus($value, $operator = null)
 * @method $this filterByTicketStatus($value, $operator = null)
 * @method $this filterByStaffComment($value, $operator = null)
 * @method $this filterByDate($value, $operator = null)
 * @method $this filterByWidgetId($value, $operator = null)
 * @method $this filterByFlag($value, $operator = null)
 * @method $this filterByFlagCaption($value, $operator = null)
 * @method $this filterByFlagHref($value, $operator = null)
 * @method $this filterByVisible($value, $operator = null)
 * @method $this filterByEnFlagCaption($value, $operator = null)
 * @method $this filterByEnFlagHref($value, $operator = null)
 * @method $this filterByEnVisible($value, $operator = null)
 * @method $this filterByRevaluation($value, $operator = null)
 * @method $this filterByMoved($value, $operator = null)
 * @method $this filterByQr($value, $operator = null)
 * @method $this filterByPushkinCard($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderByStaffStatus($direction = null)
 * @method $this orderByTicketStatus($direction = null)
 * @method $this orderByStaffComment($direction = null)
 * @method $this orderByDate($direction = null)
 * @method $this orderByWidgetId($direction = null)
 * @method $this orderByFlag($direction = null)
 * @method $this orderByFlagCaption($direction = null)
 * @method $this orderByFlagHref($direction = null)
 * @method $this orderByVisible($direction = null)
 * @method $this orderByEnFlagCaption($direction = null)
 * @method $this orderByEnFlagHref($direction = null)
 * @method $this orderByEnVisible($direction = null)
 * @method $this orderByRevaluation($direction = null)
 * @method $this orderByMoved($direction = null)
 * @method $this orderByQr($direction = null)
 * @method $this orderByPushkinCard($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 *
 * @method $this selectId()
 * @method $this selectStaffStatus()
 * @method $this selectTicketStatus()
 * @method $this selectStaffComment()
 * @method $this selectDate()
 * @method $this selectWidgetId()
 * @method $this selectFlag()
 * @method $this selectFlagCaption()
 * @method $this selectFlagHref()
 * @method $this selectVisible()
 * @method $this selectEnFlagCaption()
 * @method $this selectEnFlagHref()
 * @method $this selectEnVisible()
 * @method $this selectRevaluation()
 * @method $this selectMoved()
 * @method $this selectQr()
 * @method $this selectPushkinCard()
 * @method $this selectCreatedAt()
 *
 * @method $this filterByLocalizedFlagCaption($value, $operator = null)
 * @method $this filterByLocalizedFlagHref($value, $operator = null)
 * @method $this filterByLocalizedVisible($value, $operator = null)
 *
 * @method $this orderByLocalizedFlagCaption($direction = null)
 * @method $this orderByLocalizedFlagHref($direction = null)
 * @method $this orderByLocalizedVisible($direction = null)
 *
 * @method $this selectLocalizedFlagCaption()
 * @method $this selectLocalizedFlagHref()
 * @method $this selectLocalizedVisible()
 */
class Collection extends \diCollection
{
    const type = \diTypes::show;
	const connection_name = 'default';
    protected $table = 'show';
    protected $modelType = 'show';

    public function filterByDay(Date $dt)
    {
        return $this
            ->filterBy('date', '>=', $dt->year() . '-' . $dt->month(true) . '-' . $dt->day(true) . ' 00:00:00')
            ->filterBy('date', '<=', $dt->year() . '-' . $dt->month(true) . '-' . $dt->day(true) . ' 23:59:59');
    }

    public function filterByMonth(Date $dt)
    {
        $d2 = lead0(\diDateTime::getDaysInMonth($dt->month(), $dt->year()));

        return $this
            ->filterBy('date', '>=', $dt->year() . '-' . $dt->month(true) . '-01 00:00:00')
            ->filterBy('date', '<=', $dt->year() . '-' . $dt->month(true) . '-' . $d2 . ' 23:59:59');
    }
}
