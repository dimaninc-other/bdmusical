<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Show;

use diCore\Base\CMS;
use diCore\Database\FieldType;
use Musical\Tool\Calendar\Date;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getStaffStatus
 * @method integer	getTicketStatus
 * @method string	getStaffComment
 * @method string	getDate
 * @method integer	getWidgetId
 * @method integer	getFlag
 * @method string	getFlagCaption
 * @method string	getFlagHref
 * @method integer	getVisible
 * @method string	getEnFlagCaption
 * @method string	getEnFlagHref
 * @method integer	getEnVisible
 * @method integer	getRevaluation
 * @method integer	getMoved
 * @method integer	getQr
 * @method integer	getPushkinCard
 * @method string	getCreatedAt
 *
 * @method bool hasStaffStatus
 * @method bool hasTicketStatus
 * @method bool hasStaffComment
 * @method bool hasDate
 * @method bool hasWidgetId
 * @method bool hasFlag
 * @method bool hasFlagCaption
 * @method bool hasFlagHref
 * @method bool hasVisible
 * @method bool hasEnFlagCaption
 * @method bool hasEnFlagHref
 * @method bool hasEnVisible
 * @method bool hasRevaluation
 * @method bool hasMoved
 * @method bool hasQr
 * @method bool hasPushkinCard
 * @method bool hasCreatedAt
 *
 * @method $this setStaffStatus($value)
 * @method $this setTicketStatus($value)
 * @method $this setStaffComment($value)
 * @method $this setDate($value)
 * @method $this setWidgetId($value)
 * @method $this setFlag($value)
 * @method $this setFlagCaption($value)
 * @method $this setFlagHref($value)
 * @method $this setVisible($value)
 * @method $this setEnFlagCaption($value)
 * @method $this setEnFlagHref($value)
 * @method $this setEnVisible($value)
 * @method $this setRevaluation($value)
 * @method $this setMoved($value)
 * @method $this setQr($value)
 * @method $this setPushkinCard($value)
 * @method $this setCreatedAt($value)
 *
 * @method string	localizedFlagCaption
 * @method string	localizedFlagHref
 * @method integer	localizedVisible
 */
class Model extends \diModel
{
    const type = \diTypes::show;
	const connection_name = 'default';
    const table = 'show';
    protected $table = 'show';
	protected $localizedFields = ['flag_caption', 'flag_href', 'visible'];

	protected static $fieldTypes = [
        'id' => FieldType::int,
        'staff_status' => FieldType::int,
        'ticket_status' => FieldType::int,
        'staff_comment' => FieldType::string,
        'date' => FieldType::datetime,
        'widget_id' => FieldType::int,
        'flag' => FieldType::int,
        'flag_caption' => FieldType::string,
        'flag_href' => FieldType::string,
        'visible' => FieldType::int,
        'en_flag_caption' => FieldType::string,
        'en_flag_href' => FieldType::string,
        'en_visible' => FieldType::int,
        'revaluation' => FieldType::int,
        'moved' => FieldType::int,
        'qr' => FieldType::int,
        'pushkin_card' => FieldType::int,
        'created_at' => FieldType::timestamp,
    ];

    const STAFF_UNDEFINED = 0;
    const STAFF_DEFINED = 1;

    public static $staffStatuses = [
        self::STAFF_UNDEFINED => 'Состав актеров не определен',
        self::STAFF_DEFINED => 'Состав актеров определен',
    ];

    const TICKETS_UNAVAILABLE = 0;
    const TICKETS_ON_SALE = 1;
    const TICKETS_SOLD_OUT = 2;

    public static $ticketStatuses = [
        self::TICKETS_UNAVAILABLE => 'Билеты еще не поступили в продажу',
        self::TICKETS_ON_SALE => 'Билеты в продаже',
        self::TICKETS_SOLD_OUT => 'Все билеты проданы',
    ];

    public static $engTicketStatuses = [
        self::TICKETS_UNAVAILABLE => 'Tickets unavailable',
        self::TICKETS_ON_SALE => 'Tickets on sale',
        self::TICKETS_SOLD_OUT => 'Tickets sold out',
    ];

    const FLAG_NONE = 0;
    const FLAG_EVENT = 1;
    const FLAG_ACTION = 2;
    const FLAG_PREMIERE = 3;
    const FLAG_SOLD_OUT = 4;

    public static $flags = [
        self::FLAG_NONE => 'Нет',
        self::FLAG_EVENT => 'Событие',
        self::FLAG_ACTION => 'Акция',
        self::FLAG_PREMIERE => 'Премьера',
        self::FLAG_SOLD_OUT => 'Все билеты проданы',
    ];

    public static $engFlags = [
        self::FLAG_NONE => '',
        self::FLAG_EVENT => 'Event',
        self::FLAG_ACTION => 'Action',
        self::FLAG_PREMIERE => 'Premiere',
        self::FLAG_SOLD_OUT => 'Sold out',
    ];

    public static $classes = [
        self::FLAG_NONE => '',
        self::FLAG_EVENT => 'events',
        self::FLAG_ACTION => 'sale',
        self::FLAG_PREMIERE => 'premiere',
        self::FLAG_SOLD_OUT => 'display-none',
    ];

    public static function createNextNearest()
    {
        return Collection::create()
            ->filterByVisible(1)
            ->filterByExpression('date', '>', 'NOW()')
            ->orderByDate('ASC')
            ->getFirstItem();
    }

    public static function createNearestByMonth(Date $nextMonth, $direction)
    {
        return Collection::create()
            ->filterByVisible(1)
            ->filterByMonth($nextMonth)
            ->orderByDate($direction > 0 ? 'ASC' : 'DESC')
            ->getFirstItem();
    }

    public function isStaffDefined()
    {
        return $this->getStaffStatus() == self::STAFF_DEFINED;
    }

    public function getStaffStatusStr()
    {
        if (!isset(self::$staffStatuses[$this->getStaffStatus()])) {
            return null;
        }

        return self::$staffStatuses[$this->getStaffStatus()];
    }

    public function getTicketStatusStr()
    {
        if (!isset(self::$ticketStatuses[$this->getTicketStatus()])) {
            return null;
        }

        $lang = self::normalizeLang();

        $ar = $lang == 'ru'
            ? self::$ticketStatuses
            : self::$engTicketStatuses;

        return $ar[$this->getTicketStatus()];
    }

    public function getFlagClass()
    {
        return self::$classes[$this->getFlag()] ?? null;
    }

    public function getFlagTitle()
    {
        return self::$flags[$this->getFlag()] ?? null;
    }

    public function getEnFlagTitle()
    {
        return self::$engFlags[$this->getFlag()] ?? null;
    }

    public function getFlagLocalizedTitle()
    {
        return static::normalizeLang() == 'ru'
            ? $this->getFlagTitle()
            : $this->getEnFlagTitle();
    }

    public function getCustomTemplateVars()
    {
        return [
            'staff_status_str' => $this->getStaffStatusStr(),
            'ticket_status_str' => $this->getTicketStatusStr(),
            'date_day' => \diDateTime::format('d', $this->getDate()),
            'date_month_str_rus' => \diDateTime::format('%месяца%', $this->getDate()),
            'date_month_short_str_rus' => \diDateTime::format('%мес%', $this->getDate()),
            'date_wd_short' => \diDateTime::format('%дн%', $this->getDate()),
            'flag_localized_title' => $this->getFlagLocalizedTitle(),
            'flag_class' => $this->getFlagClass(),
            'day_href' => $this->getDayHref(),
        ];
    }

    public function setCalendarMode($mode)
    {
        return $this->setRelated('calendarMode', $mode);
    }

    public function getDayHref()
    {
        return $this->getRelated('calendarMode') == 'staff'
            ? $this->__getPrefixForHref() . '/' . CMS::ct('staff') . '/' .
                \diDateTime::format('Y/m/d/', $this->getDate())
            : $this->__getPrefixForHref() . '/' . CMS::ct('tickets_online') . '/' .
            \diDateTime::format('Y/m/', $this->getDate());
    }

    public function getHref()
    {
        $suffix = $this->getRelated('calendarMode') == 'staff'
            ? '#' . \diDateTime::format('H:i', $this->getDate())
            : '#show:' . $this->getWidgetId() . ';id:' . $this->getId();

        return $this->getDayHref() . $suffix;
    }

    public function storeStaff($isNew = false)
    {
        $ids = \diRequest::post('staff', []);

        if ($this->getId() && !$isNew) {
            self::db()->delete('show_solo_link', "WHERE show_id = '{$this->getId()}'");
        }

        foreach ($ids as $soloId) {
            $soloId = (int)$soloId;

            if ($soloId) {
                self::db()->insert('show_solo_link', [
                    'show_id' => $this->getId(),
                    'solo_id' => $soloId,
                ]);

                self::db()->dierror();
            }
        }
    }
}
