<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:29
 */

namespace Musical\Entity\Character;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterByHomeCssClass($value, $operator = null)
 * @method $this filterByTitle($value, $operator = null)
 * @method $this filterByShortContent($value, $operator = null)
 * @method $this filterByContent($value, $operator = null)
 * @method $this filterByMetaTitle($value, $operator = null)
 * @method $this filterByMetaKeywords($value, $operator = null)
 * @method $this filterByMetaDescription($value, $operator = null)
 * @method $this filterByVisible($value, $operator = null)
 * @method $this filterByEnTitle($value, $operator = null)
 * @method $this filterByEnShortContent($value, $operator = null)
 * @method $this filterByEnContent($value, $operator = null)
 * @method $this filterByEnMetaTitle($value, $operator = null)
 * @method $this filterByEnMetaKeywords($value, $operator = null)
 * @method $this filterByEnMetaDescription($value, $operator = null)
 * @method $this filterByEnVisible($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 * @method $this filterByOrderNum($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderByHomeCssClass($direction = null)
 * @method $this orderByTitle($direction = null)
 * @method $this orderByShortContent($direction = null)
 * @method $this orderByContent($direction = null)
 * @method $this orderByMetaTitle($direction = null)
 * @method $this orderByMetaKeywords($direction = null)
 * @method $this orderByMetaDescription($direction = null)
 * @method $this orderByVisible($direction = null)
 * @method $this orderByEnTitle($direction = null)
 * @method $this orderByEnShortContent($direction = null)
 * @method $this orderByEnContent($direction = null)
 * @method $this orderByEnMetaTitle($direction = null)
 * @method $this orderByEnMetaKeywords($direction = null)
 * @method $this orderByEnMetaDescription($direction = null)
 * @method $this orderByEnVisible($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 * @method $this orderByOrderNum($direction = null)
 *
 * @method $this selectId()
 * @method $this selectHomeCssClass()
 * @method $this selectTitle()
 * @method $this selectShortContent()
 * @method $this selectContent()
 * @method $this selectMetaTitle()
 * @method $this selectMetaKeywords()
 * @method $this selectMetaDescription()
 * @method $this selectVisible()
 * @method $this selectEnTitle()
 * @method $this selectEnShortContent()
 * @method $this selectEnContent()
 * @method $this selectEnMetaTitle()
 * @method $this selectEnMetaKeywords()
 * @method $this selectEnMetaDescription()
 * @method $this selectEnVisible()
 * @method $this selectCreatedAt()
 * @method $this selectOrderNum()
 *
 * @method $this filterByLocalizedTitle($value, $operator = null)
 * @method $this filterByLocalizedShortContent($value, $operator = null)
 * @method $this filterByLocalizedContent($value, $operator = null)
 * @method $this filterByLocalizedMetaTitle($value, $operator = null)
 * @method $this filterByLocalizedMetaKeywords($value, $operator = null)
 * @method $this filterByLocalizedMetaDescription($value, $operator = null)
 * @method $this filterByLocalizedVisible($value, $operator = null)
 *
 * @method $this orderByLocalizedTitle($direction = null)
 * @method $this orderByLocalizedShortContent($direction = null)
 * @method $this orderByLocalizedContent($direction = null)
 * @method $this orderByLocalizedMetaTitle($direction = null)
 * @method $this orderByLocalizedMetaKeywords($direction = null)
 * @method $this orderByLocalizedMetaDescription($direction = null)
 * @method $this orderByLocalizedVisible($direction = null)
 *
 * @method $this selectLocalizedTitle()
 * @method $this selectLocalizedShortContent()
 * @method $this selectLocalizedContent()
 * @method $this selectLocalizedMetaTitle()
 * @method $this selectLocalizedMetaKeywords()
 * @method $this selectLocalizedMetaDescription()
 * @method $this selectLocalizedVisible()
 */
class Collection extends \diCollection
{
    const type = \diTypes::character;
	const connection_name = 'default';
    protected $table = 'character';
    protected $modelType = 'character';
}
