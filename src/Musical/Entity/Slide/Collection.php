<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Slide;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterByBlockId($value, $operator = null)
 * @method $this filterByTitle($value, $operator = null)
 * @method $this filterByHref($value, $operator = null)
 * @method $this filterByContent($value, $operator = null)
 * @method $this filterByVisible($value, $operator = null)
 * @method $this filterByEnTitle($value, $operator = null)
 * @method $this filterByEnHref($value, $operator = null)
 * @method $this filterByEnContent($value, $operator = null)
 * @method $this filterByEnVisible($value, $operator = null)
 * @method $this filterByOrderNum($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderByBlockId($direction = null)
 * @method $this orderByTitle($direction = null)
 * @method $this orderByHref($direction = null)
 * @method $this orderByContent($direction = null)
 * @method $this orderByVisible($direction = null)
 * @method $this orderByEnTitle($direction = null)
 * @method $this orderByEnHref($direction = null)
 * @method $this orderByEnContent($direction = null)
 * @method $this orderByEnVisible($direction = null)
 * @method $this orderByOrderNum($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 *
 * @method $this selectId()
 * @method $this selectBlockId()
 * @method $this selectTitle()
 * @method $this selectHref()
 * @method $this selectContent()
 * @method $this selectVisible()
 * @method $this selectEnTitle()
 * @method $this selectEnHref()
 * @method $this selectEnContent()
 * @method $this selectEnVisible()
 * @method $this selectOrderNum()
 * @method $this selectCreatedAt()
 *
 * @method $this filterByLocalizedTitle($value, $operator = null)
 * @method $this filterByLocalizedHref($value, $operator = null)
 * @method $this filterByLocalizedContent($value, $operator = null)
 * @method $this filterByLocalizedVisible($value, $operator = null)
 *
 * @method $this orderByLocalizedTitle($direction = null)
 * @method $this orderByLocalizedHref($direction = null)
 * @method $this orderByLocalizedContent($direction = null)
 * @method $this orderByLocalizedVisible($direction = null)
 *
 * @method $this selectLocalizedTitle()
 * @method $this selectLocalizedHref()
 * @method $this selectLocalizedContent()
 * @method $this selectLocalizedVisible()
 */
class Collection extends \diCollection
{
    const type = \diTypes::slide;
	const connection_name = 'default';
    protected $table = 'slide';
    protected $modelType = 'slide';
}
