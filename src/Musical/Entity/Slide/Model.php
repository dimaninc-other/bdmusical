<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Slide;

use diCore\Database\FieldType;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getBlockId
 * @method string	getTitle
 * @method string	getHref
 * @method string	getContent
 * @method integer	getVisible
 * @method string	getEnTitle
 * @method string	getEnHref
 * @method string	getEnContent
 * @method integer	getEnVisible
 * @method integer	getOrderNum
 * @method string	getCreatedAt
 *
 * @method bool hasBlockId
 * @method bool hasTitle
 * @method bool hasHref
 * @method bool hasContent
 * @method bool hasVisible
 * @method bool hasEnTitle
 * @method bool hasEnHref
 * @method bool hasEnContent
 * @method bool hasEnVisible
 * @method bool hasOrderNum
 * @method bool hasCreatedAt
 *
 * @method $this setBlockId($value)
 * @method $this setTitle($value)
 * @method $this setHref($value)
 * @method $this setContent($value)
 * @method $this setVisible($value)
 * @method $this setEnTitle($value)
 * @method $this setEnHref($value)
 * @method $this setEnContent($value)
 * @method $this setEnVisible($value)
 * @method $this setOrderNum($value)
 * @method $this setCreatedAt($value)
 *
 * @method string	localizedTitle
 * @method string	localizedHref
 * @method string	localizedContent
 * @method integer	localizedVisible
 */
class Model extends \diModel
{
    const type = \diTypes::slide;
	const connection_name = 'default';
    const table = 'slide';
    protected $table = 'slide';
	protected $localizedFields = ['title', 'href', 'content', 'visible'];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'block_id' => FieldType::int,
        'title' => FieldType::string,
        'href' => FieldType::string,
        'content' => FieldType::string,
        'visible' => FieldType::int,
        'en_title' => FieldType::string,
        'en_href' => FieldType::string,
        'en_content' => FieldType::string,
        'en_visible' => FieldType::int,
        'order_num' => FieldType::int,
        'created_at' => FieldType::timestamp,
    ];

    /**
     * Returns query conditions array for order_num calculating
     *
     * @return array
     */
    public function getQueryArForMove()
    {
        $ar = [
            "block_id = '{$this->getBlockId()}'",
        ];

        return $ar;
    }
}
