<?php
/**
 * Created by \diModelsManager
 * Date: 20.05.2021
 * Time: 16:51
 */

namespace Musical\Entity\Media;

use diCore\Traits\Collection\TargetInside;
use Musical\Data\Types;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterByPicType($value, $operator = null)
 * @method $this filterBySeasonId($value, $operator = null)
 * @method $this filterBySlug($value, $operator = null)
 * @method $this filterByTitle($value, $operator = null)
 * @method $this filterByContent($value, $operator = null)
 * @method $this filterByShortContent($value, $operator = null)
 * @method $this filterByVisible($value, $operator = null)
 * @method $this filterByTop($value, $operator = null)
 * @method $this filterByEnSlug($value, $operator = null)
 * @method $this filterByEnTitle($value, $operator = null)
 * @method $this filterByEnContent($value, $operator = null)
 * @method $this filterByEnShortContent($value, $operator = null)
 * @method $this filterByEnVisible($value, $operator = null)
 * @method $this filterByEnTop($value, $operator = null)
 * @method $this filterByVendor($value, $operator = null)
 * @method $this filterByVendorVideoUid($value, $operator = null)
 * @method $this filterByPic($value, $operator = null)
 * @method $this filterByPicW($value, $operator = null)
 * @method $this filterByPicH($value, $operator = null)
 * @method $this filterByDate($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderByPicType($direction = null)
 * @method $this orderBySeasonId($direction = null)
 * @method $this orderBySlug($direction = null)
 * @method $this orderByTitle($direction = null)
 * @method $this orderByContent($direction = null)
 * @method $this orderByShortContent($direction = null)
 * @method $this orderByVisible($direction = null)
 * @method $this orderByTop($direction = null)
 * @method $this orderByEnSlug($direction = null)
 * @method $this orderByEnTitle($direction = null)
 * @method $this orderByEnContent($direction = null)
 * @method $this orderByEnShortContent($direction = null)
 * @method $this orderByEnVisible($direction = null)
 * @method $this orderByEnTop($direction = null)
 * @method $this orderByVendor($direction = null)
 * @method $this orderByVendorVideoUid($direction = null)
 * @method $this orderByPic($direction = null)
 * @method $this orderByPicW($direction = null)
 * @method $this orderByPicH($direction = null)
 * @method $this orderByDate($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 *
 * @method $this selectId()
 * @method $this selectPicType()
 * @method $this selectSeasonId()
 * @method $this selectSlug()
 * @method $this selectTitle()
 * @method $this selectContent()
 * @method $this selectShortContent()
 * @method $this selectVisible()
 * @method $this selectTop()
 * @method $this selectEnSlug()
 * @method $this selectEnTitle()
 * @method $this selectEnContent()
 * @method $this selectEnShortContent()
 * @method $this selectEnVisible()
 * @method $this selectEnTop()
 * @method $this selectVendor()
 * @method $this selectVendorVideoUid()
 * @method $this selectPic()
 * @method $this selectPicW()
 * @method $this selectPicH()
 * @method $this selectDate()
 * @method $this selectCreatedAt()
 *
 * @method $this filterByLocalizedSlug($value, $operator = null)
 * @method $this filterByLocalizedTitle($value, $operator = null)
 * @method $this filterByLocalizedContent($value, $operator = null)
 * @method $this filterByLocalizedShortContent($value, $operator = null)
 * @method $this filterByLocalizedVisible($value, $operator = null)
 * @method $this filterByLocalizedTop($value, $operator = null)
 *
 * @method $this orderByLocalizedSlug($direction = null)
 * @method $this orderByLocalizedTitle($direction = null)
 * @method $this orderByLocalizedContent($direction = null)
 * @method $this orderByLocalizedShortContent($direction = null)
 * @method $this orderByLocalizedVisible($direction = null)
 * @method $this orderByLocalizedTop($direction = null)
 *
 * @method $this selectLocalizedSlug()
 * @method $this selectLocalizedTitle()
 * @method $this selectLocalizedContent()
 * @method $this selectLocalizedShortContent()
 * @method $this selectLocalizedVisible()
 * @method $this selectLocalizedTop()
 */
class Collection extends \diCollection
{
    use TargetInside;

    const type = \diTypes::media;
	const connection_name = 'default';
    protected $table = 'media';
    protected $modelType = 'media';

    public static function rebuild()
    {
        $stat = [];

        foreach (Model::$targetTypes as $targetType => $title) {
            $col = \diCollection::create($targetType);

            switch ($targetType) {
                case Types::album:
                    $col
                        ->filterBy('photos_count', '>', 0);
                    break;
            }

            foreach ($col as $item) {
                Model::forceUpdateOrCreate($item);
            }

            $stat[Types::getName($targetType)] = $col->count();
        }

        return $stat;
    }
}