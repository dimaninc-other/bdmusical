<?php
/**
 * Created by \diModelsManager
 * Date: 20.05.2021
 * Time: 16:51
 */

namespace Musical\Entity\Media;

use diCore\Base\CMS;
use diCore\Database\FieldType;
use diCore\Entity\Video\Vendor;
use diCore\Helper\ArrayHelper;
use diCore\Traits\Model\TargetInside;
use Musical\Data\Config;
use Musical\Data\Types;
use Musical\Entity\Photo\Collection as Photos;
use Musical\Entity\Photo\Model as Photo;
use Musical\Traits\Model\DefaultPic;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getPicType
 * @method integer	getSeasonId
 * @method string	getTitle
 * @method string	getContent
 * @method string	getShortContent
 * @method integer	getVisible
 * @method integer	getTop
 * @method string	getEnSlug
 * @method string	getEnTitle
 * @method string	getEnContent
 * @method string	getEnShortContent
 * @method integer	getEnVisible
 * @method integer	getEnTop
 * @method integer	getVendor
 * @method string	getVendorVideoUid
 * @method string	getPic
 * @method integer	getPicW
 * @method integer	getPicH
 * @method string	getDate
 * @method string	getCreatedAt
 *
 * @method bool hasPicType
 * @method bool hasSeasonId
 * @method bool hasTitle
 * @method bool hasContent
 * @method bool hasShortContent
 * @method bool hasVisible
 * @method bool hasTop
 * @method bool hasEnSlug
 * @method bool hasEnTitle
 * @method bool hasEnContent
 * @method bool hasEnShortContent
 * @method bool hasEnVisible
 * @method bool hasEnTop
 * @method bool hasVendor
 * @method bool hasVendorVideoUid
 * @method bool hasPic
 * @method bool hasPicW
 * @method bool hasPicH
 * @method bool hasDate
 * @method bool hasCreatedAt
 *
 * @method $this setPicType($value)
 * @method $this setSeasonId($value)
 * @method $this setTitle($value)
 * @method $this setContent($value)
 * @method $this setShortContent($value)
 * @method $this setVisible($value)
 * @method $this setTop($value)
 * @method $this setEnSlug($value)
 * @method $this setEnTitle($value)
 * @method $this setEnContent($value)
 * @method $this setEnShortContent($value)
 * @method $this setEnVisible($value)
 * @method $this setEnTop($value)
 * @method $this setVendor($value)
 * @method $this setVendorVideoUid($value)
 * @method $this setPic($value)
 * @method $this setPicW($value)
 * @method $this setPicH($value)
 * @method $this setDate($value)
 * @method $this setCreatedAt($value)
 *
 * @method string	localizedSlug
 * @method string	localizedTitle
 * @method string	localizedContent
 * @method string	localizedShortContent
 * @method integer	localizedVisible
 * @method integer	localizedTop
 */
class Model extends \diModel
{
    use TargetInside {
        TargetInside::setTarget as protected _set_target;
    }
    use DefaultPic;

    const type = \diTypes::media;
	const connection_name = 'default';
    const table = 'media';
    protected $table = 'media';
	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'title', 'content', 'short_content', 'visible', 'top'];

	public static $targetTypes = [
	    Types::album => 'Фото',
        Types::video => 'Видео',
        Types::smi_tv => 'ТВ',
        Types::smi_radio => 'Радио',
        Types::smi_press => 'Пресса',
    ];

    public static $enTargetTypes = [
        Types::album => 'Photo',
        Types::video => 'Video',
        Types::smi_tv => 'TV',
        Types::smi_radio => 'Radio',
        Types::smi_press => 'Press',
    ];

    protected static $parentFields = [
        'season_id',
        'slug',
        'title',
        'content',
        'short_content',
        'visible',
        'top',
        'en_slug',
        'en_title',
        'en_content',
        'en_short_content',
        'en_visible',
        'en_top',
        'vendor',
        'vendor_video_uid',
        'pic',
        'pic_w',
        'pic_h',
        'date',
    ];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'target_type' => FieldType::int,
        'target_id' => FieldType::int,
        'pic_type' => FieldType::int,
        'season_id' => FieldType::int,
        'slug' => FieldType::string,
        'title' => FieldType::string,
        'content' => FieldType::string,
        'short_content' => FieldType::string,
        'visible' => FieldType::int,
        'top' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_title' => FieldType::string,
        'en_content' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_visible' => FieldType::int,
        'en_top' => FieldType::int,
        'vendor' => FieldType::int,
        'vendor_video_uid' => FieldType::string,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'date' => FieldType::datetime,
        'created_at' => FieldType::timestamp,
    ];

	public static function forceUpdateOrCreate(\diModel $target)
    {
        if (!isset(static::$targetTypes[$target->modelType()])) {
            throw new \Exception('Unsupported media type: ' . $target->modelType());
        }

        return static::createByTarget($target)
            ->setTarget($target)
            ->updateOrCreate()
            ->save();
    }

    public function setTarget(\diModel $target)
    {
        if (!isset(static::$targetTypes[$target->modelType()])) {
            throw new \Exception('Unsupported media type: ' . $target->modelType());
        }

        return $this->_set_target($target);
    }

    public function updateOrCreate()
    {
        foreach (static::$parentFields as $field) {
            $value = $this->getTargetModel()->get($field);
            $value = static::tuneFieldValueByTypeBeforeDb($field, $value);

            if (
                $this->getTargetModel()->modelType() == Types::album &&
                $field == 'pic' &&
                !$value
            ) {
                /** @var Photo $photo */
                $photo = Photos::create()
                    ->filterByAlbumId($this->getTargetModel()->getId())
                    ->filterByLocalized('visible', 1)
                    ->getFirstItem();

                if ($photo->exists()) {
                    $this
                        ->setPicType(Types::photo)
                        ->setPicW($photo->getPicW())
                        ->setPicH($photo->getPicH());

                    $value = $photo->getPic();
                }
            }

            $this->set($field, $value);
        }

        return $this;
    }

    public function getTemplateVars()
    {
        if ($this->isVideo()) {
            return parent::getTemplateVars();
        }

        $this->beforeGet();

        $ar = parent::getTemplateVars();

        $this->afterGet();

        return $this->fixDefaultPicVars($ar);
    }

    public function isVideo()
    {
        return in_array($this->getTargetType(), [
            Types::video,
            Types::smi_tv,
        ]);
    }

    public function isAlbum()
    {
        return in_array($this->getTargetType(), [
            Types::album,
        ]);
    }

    public function getCustomTemplateVars()
    {
        $ar = [
            'target_type_name' => $this->getTargetTypeName(),
            'target_type_title' => $this->getTargetTypeTitle(),
            'localized_target_type_title' => $this->getLocalizedTargetTypeTitle(),
        ];

        if ($this->isVideo() && $this->getVendor() != Vendor::OWN) {
            $video = \diModel::create($this->getTargetType())
                ->initFrom($this)
                ->setId($this->getTargetId());

            $ar = extend($ar, ArrayHelper::filterByKey($video->getCustomTemplateVars(), [
                'pic_safe',
                'pic_tn',
                'pic_tn_safe',
                'vendor_link',
                'vendor_embed_link',
            ]));

            $ar['pic_with_path'] = $ar['pic_tn_with_path'] = $ar['pic_tn'];
        }

        return extend(parent::getCustomTemplateVars(), $ar);
    }

    public function getLocalizedTargetTypeTitle()
    {
        $lang = self::normalizeLang();

        $ar = $lang == 'en'
            ? self::$enTargetTypes
            : self::$targetTypes;

        return $ar[$this->getTargetType()];
    }

    public function getPicsFolder()
    {
        $type = $this->getPicType() ?: $this->getTargetType();

        return $type
            ? get_pics_folder(Types::getTable($type), Config::getUserAssetsFolder())
            : parent::getPicsFolder();
    }

    public function getHref()
    {
        return static::mediaHref($this->getTargetModel());
    }

    public static function mediaHref(\diModel $target)
    {
        $type = Types::getName($target->modelType());

        return $target->__getPrefixForHref() . '/' . CMS::ct('media') . '/' . $type . '/' . $target->getSlug() . '/';
    }
}
