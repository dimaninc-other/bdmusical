<?php
/**
 * Created by \diModelsManager
 * Date: 27.04.2021
 * Time: 18:14
 */

namespace Musical\Entity\ShowSoloLink;

use diCore\Database\FieldType;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getShowId
 * @method integer	getSoloId
 *
 * @method bool hasShowId
 * @method bool hasSoloId
 *
 * @method $this setShowId($value)
 * @method $this setSoloId($value)
 */
class Model extends \diModel
{
    const type = \diTypes::show_solo_link;
	const connection_name = 'default';
    const table = 'show_solo_link';
    const id_field_name = null;
    protected $table = 'show_solo_link';

	protected static $fieldTypes = [
        'show_id' => FieldType::int,
        'solo_id' => FieldType::int,
    ];
}
