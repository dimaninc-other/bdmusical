<?php
/**
 * Created by \diModelsManager
 * Date: 27.04.2021
 * Time: 18:14
 */

namespace Musical\Entity\ShowSoloLink;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByShowId($value, $operator = null)
 * @method $this filterBySoloId($value, $operator = null)
 *
 * @method $this orderByShowId($direction = null)
 * @method $this orderBySoloId($direction = null)
 *
 * @method $this selectShowId()
 * @method $this selectSoloId()
 */
class Collection extends \diCollection
{
    const type = \diTypes::show_solo_link;
	const connection_name = 'default';
    protected $table = 'show_solo_link';
    protected $modelType = 'show_solo_link';
    protected $isIdUnique = false;
}
