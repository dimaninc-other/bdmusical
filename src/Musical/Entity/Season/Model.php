<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Season;

use diCore\Base\CMS;
use diCore\Database\FieldType;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getSlugSource
 * @method string	getTitle
 * @method string	getContent
 * @method string	getDate1
 * @method string	getDate2
 * @method string	getCreatedAt
 * @method integer	getOrderNum
 * @method integer	getVisible
 *
 * @method bool hasSlugSource
 * @method bool hasTitle
 * @method bool hasContent
 * @method bool hasDate1
 * @method bool hasDate2
 * @method bool hasCreatedAt
 * @method bool hasOrderNum
 * @method bool hasVisible
 *
 * @method $this setSlugSource($value)
 * @method $this setTitle($value)
 * @method $this setContent($value)
 * @method $this setDate1($value)
 * @method $this setDate2($value)
 * @method $this setCreatedAt($value)
 * @method $this setOrderNum($value)
 * @method $this setVisible($value)
 */
class Model extends \diModel
{
    const type = \diTypes::season;
	const connection_name = 'default';
    const table = 'season';
    protected $table = 'season';
	const slug_field_name = self::SLUG_FIELD_NAME;

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'title' => FieldType::string,
        'content' => FieldType::string,
        'date1' => FieldType::date,
        'date2' => FieldType::date,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
        'visible' => FieldType::int,
    ];

    public function getHref()
    {
        return $this->__getPrefixForHref() . '/' . CMS::ct('news') . '/' . $this->getSlug() . '/';
    }
}
