<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Season;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterBySlug($value, $operator = null)
 * @method $this filterBySlugSource($value, $operator = null)
 * @method $this filterByTitle($value, $operator = null)
 * @method $this filterByContent($value, $operator = null)
 * @method $this filterByDate1($value, $operator = null)
 * @method $this filterByDate2($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 * @method $this filterByOrderNum($value, $operator = null)
 * @method $this filterByVisible($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderBySlug($direction = null)
 * @method $this orderBySlugSource($direction = null)
 * @method $this orderByTitle($direction = null)
 * @method $this orderByContent($direction = null)
 * @method $this orderByDate1($direction = null)
 * @method $this orderByDate2($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 * @method $this orderByOrderNum($direction = null)
 * @method $this orderByVisible($direction = null)
 *
 * @method $this selectId()
 * @method $this selectSlug()
 * @method $this selectSlugSource()
 * @method $this selectTitle()
 * @method $this selectContent()
 * @method $this selectDate1()
 * @method $this selectDate2()
 * @method $this selectCreatedAt()
 * @method $this selectOrderNum()
 * @method $this selectVisible()
 */
class Collection extends \diCollection
{
    const type = \diTypes::season;
	const connection_name = 'default';
    protected $table = 'season';
    protected $modelType = 'season';

    public static function createFor($table)
    {
        return static::create(\diTypes::season, self::db()->q(
            "SELECT s.* FROM season s INNER JOIN $table n ON n.season_id = s.id 
            WHERE s.visible = '1' ORDER BY s.order_num ASC"
        ));
    }
}
