<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:32
 */

namespace Musical\Entity\SmiTv;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByShortContent($value, $operator = null)
 * @method $this filterByEnShortContent($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 *
 * @method $this orderByShortContent($direction = null)
 * @method $this orderByEnShortContent($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 *
 * @method $this selectShortContent()
 * @method $this selectEnShortContent()
 * @method $this selectCreatedAt()
 *
 * @method $this filterByLocalizedShortContent($value, $operator = null)
 *
 * @method $this orderByLocalizedShortContent($direction = null)
 *
 * @method $this selectLocalizedShortContent()
 */
class Collection extends \Musical\Entity\Video\Collection
{
    const type = \diTypes::smi_tv;
	const connection_name = 'default';
    protected $table = 'smi_tv';
    protected $modelType = 'smi_tv';
}
