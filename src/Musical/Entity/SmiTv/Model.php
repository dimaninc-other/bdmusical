<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:32
 */

namespace Musical\Entity\SmiTv;

use diCore\Database\FieldType;
use Musical\Traits\Model\SeasonInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getShortContent
 * @method string	getEnShortContent
  * @method string	getCreatedAt
 *
 * @method bool hasShortContent
 * @method bool hasEnShortContent
 * @method bool hasCreatedAt
 *
 * @method $this setShortContent($value)
 * @method $this setEnShortContent($value)
 * @method $this setCreatedAt($value)
 *
 * @method string	localizedShortContent
 */
class Model extends \Musical\Entity\Video\Model
{
    use SeasonInside;

    const type = \diTypes::smi_tv;
	const connection_name = 'default';
    const table = 'smi_tv';
    protected $table = 'smi_tv';
	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'slug_source', 'title', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible', 'top'];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'album_id' => FieldType::string,
        'season_id' => FieldType::int,
        'vendor' => FieldType::int,
        'vendor_video_uid' => FieldType::string,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'title' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'top' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'en_top' => FieldType::int,
        'embed' => FieldType::string,
        'video_mp4' => FieldType::string,
        'video_m4v' => FieldType::string,
        'video_ogv' => FieldType::string,
        'video_webm' => FieldType::string,
        'video_w' => FieldType::int,
        'video_h' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic_t' => FieldType::int,
        'pic_tn_w' => FieldType::int,
        'pic_tn_h' => FieldType::int,
        'views_count' => FieldType::string,
        'date' => FieldType::datetime,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
        'comments_enabled' => FieldType::int,
        'comments_last_date' => FieldType::datetime,
        'comments_count' => FieldType::int,
    ];
}
