<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Review;

use diCore\Base\CMS;
use diCore\Database\FieldType;
use Musical\Traits\Model\OldIdInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getType
 * @method string	getEmail
 * @method string	getPosition
 * @method string	getEnPosition
 *
 * @method bool hasType
 * @method bool hasEmail
 * @method bool hasPosition
 * @method bool hasEnPosition
 *
 * @method $this setType($value)
 * @method $this setEmail($value)
 * @method $this setPosition($value)
 * @method $this setEnPosition($value)
 *
 * @method string	localizedPosition
 */
class Model extends \Musical\Entity\BaseEntity\Model
{
    use OldIdInside;

    const type = \diTypes::review;
	const connection_name = 'default';
    const table = 'review';
    protected $table = 'review';
	const slug_field_name = self::SLUG_FIELD_NAME;
    protected $localizedFields = ['slug', 'slug_source', 'title', 'position', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible'];

	protected static $fieldTypes = [
        'id' => FieldType::int,
        'type' => FieldType::int,
        'email' => FieldType::string,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'title' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'position' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'visible' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_position' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];

    const TYPE_GENERAL = 1;
    const TYPE_CELEBRITY = 2;

    public static $types = [
        self::TYPE_GENERAL => 'Зрителей',
        self::TYPE_CELEBRITY => 'Звезд',
    ];

    public function getHref()
    {
        return $this->__getPrefixForHref() . '/' . CMS::ct('reviews') . '/#' . $this->getSlug();
    }

    public function isGeneral()
    {
        return $this->getType() == static::TYPE_GENERAL;
    }

    public function isCelebrity()
    {
        return $this->getType() == static::TYPE_CELEBRITY;
    }

    public function getTypeTitle()
    {
        return static::$types[$this->getType()] ?? null;
    }

    public function validate()
    {
        if (!$this->hasTitle()) {
            $this->addValidationError('Введите ФИО', 'title');
        }

        if (!$this->hasContent()) {
            $this->addValidationError('Введите текст отзыва', 'content');
        }

        if ($this->hasEmail() && !\diEmail::isValid($this->getEmail())) {
            $this->addValidationError('Введите корректный email', 'email');
        }

        return $this;
    }
}
