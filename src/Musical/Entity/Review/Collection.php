<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Review;

use Musical\Traits\Collection\OldIdInside;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByType($value, $operator = null)
 * @method $this filterByEmail($value, $operator = null)
 * @method $this filterByPosition($value, $operator = null)
 * @method $this filterByEnPosition($value, $operator = null)
 *
 * @method $this orderByType($direction = null)
 * @method $this orderByEmail($direction = null)
 * @method $this orderByPosition($direction = null)
 * @method $this orderByEnPosition($direction = null)
 *
 * @method $this selectType()
 * @method $this selectEmail()
 * @method $this selectPosition()
 * @method $this selectEnPosition()
 *
 * @method $this filterByLocalizedPosition($value, $operator = null)
 *
 * @method $this orderByLocalizedPosition($direction = null)
 *
 * @method $this selectLocalizedPosition()
 */
class Collection extends \Musical\Entity\BaseEntity\Collection
{
    use OldIdInside;

    const type = \diTypes::review;
	const connection_name = 'default';
    protected $table = 'review';
    protected $modelType = 'review';
}
