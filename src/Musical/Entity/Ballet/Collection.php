<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:29
 */

namespace Musical\Entity\Ballet;

/**
 * Class Collection
 * Methods list for IDE
 *
 */
class Collection extends \Musical\Entity\BaseEntity\Collection
{
    const type = \diTypes::ballet;
	const connection_name = 'default';
    protected $table = 'ballet';
    protected $modelType = 'ballet';
}
