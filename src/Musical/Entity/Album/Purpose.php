<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 08.05.2021
 * Time: 09:42
 */

namespace Musical\Entity\Album;

use diCore\Tool\SimpleContainer;

class Purpose extends SimpleContainer
{
    const promo_about = 1;
    const groups = 2;

    public static $names = [
        self::promo_about => 'about',
        self::groups => 'groups',
    ];

    public static $titles = [
        self::promo_about => 'Промо-альбом для раздела "О мюзикле"',
        self::groups => 'Групповые заявки',
    ];
}