<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Price;

use diCore\Database\FieldType;
use diCore\Helper\ArrayHelper;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getDateType
 * @method integer	getLayoutId
 * @method string	getDate
 * @method string	getPrices
 * @method string	getCreatedAt
 * @method integer	getVisible
 *
 * @method bool hasDateType
 * @method bool hasLayoutId
 * @method bool hasDate
 * @method bool hasPrices
 * @method bool hasCreatedAt
 * @method bool hasVisible
 *
 * @method $this setDateType($value)
 * @method $this setLayoutId($value)
 * @method $this setDate($value)
 * @method $this setCreatedAt($value)
 * @method $this setVisible($value)
 */
class Model extends \diModel
{
    const type = \diTypes::price;
	const connection_name = 'default';
    const table = 'price';
    protected $table = 'price';

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'date_type' => FieldType::int,
        'layout_id' => FieldType::int,
        'date' => FieldType::datetime,
        'prices' => FieldType::string,
        'created_at' => FieldType::timestamp,
        'visible' => FieldType::int,
    ];

    const DATE_TYPE_DEFAULT = 0;
    const DATE_TYPE_YEAR = 1;
    const DATE_TYPE_MONTH = 2;
    const DATE_TYPE_DAY = 3;

    public static $dateTypes = [
        self::DATE_TYPE_DEFAULT => 'Цена по умолчанию',
        self::DATE_TYPE_YEAR => 'Цена на указанный год',
        self::DATE_TYPE_MONTH => 'Цена на указанный месяц',
        self::DATE_TYPE_DAY => 'Цена на указанный день',
    ];

    const LAYOUT_DEFAULT = 0;

    public static $layouts = [
        self::LAYOUT_DEFAULT => [
            'columns' => [
                [
                    'title' => 'ПН-ЧТ,<span>19:00</span>',
                    'note' => '',
                ],
                [
                    'title' => 'ПТ, ВС,',
                    'note' => 'Пред-празд. Дневные',
                ],
                [
                    'title' => 'СБ,',
                    'note' => 'праздн. дни в&nbsp;19:00',
                ],
            ],
            'rows' => [
                1 => [
                    'color' => '#FF0000',
                ],
                2 => [
                    'color' => '#D52AD4',
                ],
                3 => [
                    'color' => '#00B357',
                ],
                4 => [
                    'color' => '#EBC400',
                ],
                5 => [
                    'color' => '#979797',
                ],
                6 => [
                    'color' => '#00BFEE',
                    'title' => '6*',
                ],
            ],
        ],
    ];

    protected $pricesAr = [];

    public function initFrom($r)
    {
        parent::initFrom($r);

        if ($this->hasPrices()) {
            $this->updatePrices();
        }

        return $this;
    }

    public function getPricesArray()
    {
        return $this->pricesAr;
    }

    protected function updatePrices()
    {
        $this->pricesAr = json_decode($this->getPrices());

        return $this;
    }

    public function setPrices($value)
    {
        $this
            ->set('prices', $value)
            ->updatePrices();

        return $this;
    }

    public function getPriceBy($column, $row)
    {
        return ArrayHelper::get($this->pricesAr, [$row, $column]);
    }

    public function getDateTypeStr()
    {
        return self::$dateTypes[$this->getDateType()];
    }
}
