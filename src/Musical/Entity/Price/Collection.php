<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Price;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterByDateType($value, $operator = null)
 * @method $this filterByLayoutId($value, $operator = null)
 * @method $this filterByDate($value, $operator = null)
 * @method $this filterByPrices($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 * @method $this filterByVisible($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderByDateType($direction = null)
 * @method $this orderByLayoutId($direction = null)
 * @method $this orderByDate($direction = null)
 * @method $this orderByPrices($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 * @method $this orderByVisible($direction = null)
 *
 * @method $this selectId()
 * @method $this selectDateType()
 * @method $this selectLayoutId()
 * @method $this selectDate()
 * @method $this selectPrices()
 * @method $this selectCreatedAt()
 * @method $this selectVisible()
 */
class Collection extends \diCollection
{
    const type = \diTypes::price;
	const connection_name = 'default';
    protected $table = 'price';
    protected $modelType = 'price';
}
