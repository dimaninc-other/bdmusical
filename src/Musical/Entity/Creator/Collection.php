<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:29
 */

namespace Musical\Entity\Creator;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByPosition($value, $operator = null)
 * @method $this filterByEnPosition($value, $operator = null)
 * @method $this filterByPic2($value, $operator = null)
 * @method $this filterByPic2W($value, $operator = null)
 * @method $this filterByPic2H($value, $operator = null)
 *
 * @method $this orderByPosition($direction = null)
 * @method $this orderByEnPosition($direction = null)
 * @method $this orderByPic2($direction = null)
 * @method $this orderByPic2W($direction = null)
 * @method $this orderByPic2H($direction = null)
 *
 * @method $this selectPosition()
 * @method $this selectEnPosition()
 * @method $this selectPic2()
 * @method $this selectPic2W()
 * @method $this selectPic2H()
 *
 * @method $this filterByLocalizedPosition($value, $operator = null)
 *
 * @method $this orderByLocalizedPosition($direction = null)
 *
 * @method $this selectLocalizedPosition()
 */
class Collection extends \Musical\Entity\BaseEntity\Collection
{
    const type = \diTypes::creator;
	const connection_name = 'default';
    protected $table = 'creator';
    protected $modelType = 'creator';
}
