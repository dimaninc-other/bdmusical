<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:29
 */

namespace Musical\Entity\Creator;

use diCore\Database\FieldType;
use Musical\Traits\Model\CssClassInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getPosition
 * @method string	getEnPosition
 * @method string	getPic2
 * @method integer	getPic2W
 * @method integer	getPic2H
 *
 * @method bool hasPosition
 * @method bool hasEnPosition
 * @method bool hasPic2
 * @method bool hasPic2W
 * @method bool hasPic2H
 *
 * @method $this setPosition($value)
 * @method $this setPic2($value)
 * @method $this setPic2W($value)
 * @method $this setPic2H($value)
 *
 * @method string	localizedPosition
 */
class Model extends \Musical\Entity\BaseEntity\Model
{
    use CssClassInside;

    const type = \diTypes::creator;
	const connection_name = 'default';
    const table = 'creator';
    protected $table = 'creator';
	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'slug_source', 'title', 'position', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible'];

	const PAGE_TYPE_FOR_HREF = 'creators';

	protected static $fieldTypes = [
        'id' => FieldType::int,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'css_class' => FieldType::string,
        'title' => FieldType::string,
        'position' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_position' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic2' => FieldType::string,
        'pic2_w' => FieldType::int,
        'pic2_h' => FieldType::int,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];
}
