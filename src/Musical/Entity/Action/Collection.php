<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:11
 */

namespace Musical\Entity\Action;

use Musical\Traits\Collection\OldIdInside;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByPurpose($value, $operator = null)
 *
 * @method $this orderByPurpose($direction = null)
 *
 * @method $this selectPurpose()
 */
class Collection extends \Musical\Entity\BaseEntity\Collection
{
    use OldIdInside;

    const type = \diTypes::action;
	const connection_name = 'default';
    protected $table = 'action';
    protected $modelType = 'action';
}
