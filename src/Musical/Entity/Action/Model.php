<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:11
 */

namespace Musical\Entity\Action;

use diCore\Database\FieldType;
use Musical\Traits\Model\OldIdInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getPurpose
 *
 * @method bool hasPurpose
 *
 * @method $this setPurpose($value)
 */
class Model extends \Musical\Entity\BaseEntity\Model
{
    use OldIdInside;

    const type = \diTypes::action;
	const connection_name = 'default';
    const table = 'action';
    protected $table = 'action';
	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'slug_source', 'title', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible'];

	const PAGE_TYPE_FOR_HREF = 'actions';

	protected static $fieldTypes = [
        'id' => FieldType::int,
        'purpose' => FieldType::int,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'title' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];

    /**
     * @param $purpose
     * @return $this
     * @throws \Exception
     */
	public static function createByPurpose($purpose)
    {
        return Collection::create()
            ->filterByPurpose($purpose)
            ->getFirstItem();
    }

    public static function createForPurposeDynamicPrice()
    {
        return static::createByPurpose(Purpose::DYNAMIC_PRICE);
    }

    public static function createForPurposeSafe()
    {
        return static::createByPurpose(Purpose::SAFE);
    }

	public function getPurposeTitle()
    {
        return Purpose::title($this->getPurpose());
    }

    public function getPurposeName()
    {
        return Purpose::name($this->getPurpose());
    }

    public function getCustomTemplateVars()
    {
        return extend(parent::getCustomTemplateVars(), [
            'purpose_title' => $this->getPurposeTitle(),
            'purpose_name' => $this->getPurposeName(),
        ]);
    }
}
