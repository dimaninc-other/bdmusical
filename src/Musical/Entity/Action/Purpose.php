<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 13.07.2021
 * Time: 11:18
 */

namespace Musical\Entity\Action;

use diCore\Tool\SimpleContainer;

class Purpose extends SimpleContainer
{
    const DYNAMIC_PRICE = 1;
    const SAFE = 2;

    public static $names = [
        self::DYNAMIC_PRICE => 'DYNAMIC_PRICE',
        self::SAFE => 'SAFE',
    ];

    public static $titles = [
        self::DYNAMIC_PRICE => 'Динамическое ценообразование',
        self::SAFE => 'Безопасный театр',
    ];
}
