<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\SlideBlock;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterByTitle($value, $operator = null)
 * @method $this filterByOrderNum($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderByTitle($direction = null)
 * @method $this orderByOrderNum($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 *
 * @method $this selectId()
 * @method $this selectTitle()
 * @method $this selectOrderNum()
 * @method $this selectCreatedAt()
 */
class Collection extends \diCollection
{
    const type = \diTypes::slide_block;
	const connection_name = 'default';
    protected $table = 'slide_block';
    protected $modelType = 'slide_block';
}
