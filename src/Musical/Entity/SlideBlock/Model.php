<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\SlideBlock;


use diCore\Database\FieldType;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getTitle
 * @method integer	getOrderNum
 * @method string	getCreatedAt
 *
 * @method bool hasTitle
 * @method bool hasOrderNum
 * @method bool hasCreatedAt
 *
 * @method $this setTitle($value)
 * @method $this setOrderNum($value)
 * @method $this setCreatedAt($value)
 */
class Model extends \diModel
{
    const type = \diTypes::slide_block;
	const connection_name = 'default';
    const table = 'slide_block';
    protected $table = 'slide_block';

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'title' => FieldType::string,
        'order_num' => FieldType::int,
        'created_at' => FieldType::timestamp,
    ];
}
