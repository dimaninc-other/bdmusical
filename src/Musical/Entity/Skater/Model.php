<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Skater;

/**
 * Class Model
 * Methods list for IDE
 */
class Model extends \Musical\Entity\Ballet\Model
{
    const type = \diTypes::skater;
    const table = 'skater';
    protected $table = 'skater';
}
