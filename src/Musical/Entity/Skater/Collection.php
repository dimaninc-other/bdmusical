<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Skater;

/**
 * Class Collection
 * Methods list for IDE
 */
class Collection extends \Musical\Entity\Ballet\Collection
{
    const type = \diTypes::skater;
    protected $table = 'skater';
    protected $modelType = 'skater';
}
