<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 15.04.2021
 * Time: 11:56
 */

namespace Musical\Entity\Partner;

use diCore\Tool\Localization;
use diCore\Tool\SimpleContainer;

class Category extends SimpleContainer
{
    const INFO = 1;
    const TICKET = 2;
    const PARTNER = 3;

    public static $names = [
        self::INFO => 'INFO',
        self::TICKET => 'TICKET',
        self::PARTNER => 'PARTNER',
    ];

    public static $titles = [
        self::INFO => 'Информационные партнеры',
        self::TICKET => 'Билетные партнеры',
        self::PARTNER => 'Партнерские программы',
    ];

    public static function getLocalizedTitles()
    {
        return [
            self::INFO => Localization::get('partner.category.info'),
            self::TICKET => Localization::get('partner.category.ticket'),
            self::PARTNER => Localization::get('partner.category.partner'),
        ];
    }
}
