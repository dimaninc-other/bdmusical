<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Partner;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterByTitle($value, $operator = null)
 * @method $this filterByVisible($value, $operator = null)
 * @method $this filterByEnTitle($value, $operator = null)
 * @method $this filterByEnVisible($value, $operator = null)
 * @method $this filterByHref($value, $operator = null)
 * @method $this filterByCategoryId($value, $operator = null)
 * @method $this filterByPic($value, $operator = null)
 * @method $this filterByPicW($value, $operator = null)
 * @method $this filterByPicH($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 * @method $this filterByOrderNum($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderByTitle($direction = null)
 * @method $this orderByVisible($direction = null)
 * @method $this orderByEnTitle($direction = null)
 * @method $this orderByEnVisible($direction = null)
 * @method $this orderByHref($direction = null)
 * @method $this orderByCategoryId($direction = null)
 * @method $this orderByPic($direction = null)
 * @method $this orderByPicW($direction = null)
 * @method $this orderByPicH($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 * @method $this orderByOrderNum($direction = null)
 *
 * @method $this selectId()
 * @method $this selectTitle()
 * @method $this selectVisible()
 * @method $this selectEnTitle()
 * @method $this selectEnVisible()
 * @method $this selectHref()
 * @method $this selectCategoryId()
 * @method $this selectPic()
 * @method $this selectPicW()
 * @method $this selectPicH()
 * @method $this selectCreatedAt()
 * @method $this selectOrderNum()
 *
 * @method $this filterByLocalizedTitle($value, $operator = null)
 * @method $this filterByLocalizedVisible($value, $operator = null)
 *
 * @method $this orderByLocalizedTitle($direction = null)
 * @method $this orderByLocalizedVisible($direction = null)
 *
 * @method $this selectLocalizedTitle()
 * @method $this selectLocalizedVisible()
 */
class Collection extends \diCollection
{
    const type = \diTypes::partner;
	const connection_name = 'default';
    protected $table = 'partner';
    protected $modelType = 'partner';
}
