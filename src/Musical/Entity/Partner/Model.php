<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:31
 */

namespace Musical\Entity\Partner;

use diCore\Database\FieldType;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getTitle
 * @method integer	getVisible
 * @method string	getEnTitle
 * @method integer	getEnVisible
 * @method integer	getCategoryId
 * @method string	getPic
 * @method integer	getPicW
 * @method integer	getPicH
 * @method string	getCreatedAt
 * @method integer	getOrderNum
 *
 * @method bool hasTitle
 * @method bool hasVisible
 * @method bool hasEnTitle
 * @method bool hasEnVisible
 * @method bool hasHref
 * @method bool hasCategoryId
 * @method bool hasPic
 * @method bool hasPicW
 * @method bool hasPicH
 * @method bool hasCreatedAt
 * @method bool hasOrderNum
 *
 * @method $this setTitle($value)
 * @method $this setVisible($value)
 * @method $this setEnTitle($value)
 * @method $this setEnVisible($value)
 * @method $this setHref($value)
 * @method $this setCategoryId($value)
 * @method $this setPic($value)
 * @method $this setPicW($value)
 * @method $this setPicH($value)
 * @method $this setCreatedAt($value)
 * @method $this setOrderNum($value)
 *
 * @method string	localizedTitle
 * @method integer	localizedVisible
 */
class Model extends \diModel
{
    const type = \diTypes::partner;
	const connection_name = 'default';
    const table = 'partner';
    protected $table = 'partner';
	protected $localizedFields = ['title', 'visible'];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'title' => FieldType::string,
        'visible' => FieldType::int,
        'en_title' => FieldType::string,
        'en_visible' => FieldType::int,
        'href' => FieldType::string,
        'category_id' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];

    public function getHref()
    {
        return $this->get('href');
    }

    /**
     * Returns query conditions array for order_num calculating
     *
     * @return array
     */
    public function getQueryArForMove()
    {
        $ar = [
            "category_id = '{$this->getCategoryId()}'",
        ];

        return $ar;
    }
}
