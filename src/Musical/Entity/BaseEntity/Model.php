<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 30.05.2021
 * Time: 11:35
 */

namespace Musical\Entity\BaseEntity;

use diCore\Admin\Submit;
use diCore\Base\CMS;
use Musical\Traits\Model\DefaultPic;

/**
 * Class Model
 * @package Musical\Entity\BaseEntity
 *
 * @method string	getSlugSource
 * @method string	getTitle
 * @method string	getShortContent
 * @method string	getContent
 * @method string	getMetaTitle
 * @method string	getMetaKeywords
 * @method string	getMetaDescription
 * @method integer	getVisible
 * @method string	getEnSlugSource
 * @method string	getEnSlug
 * @method string	getEnTitle
 * @method string	getEnShortContent
 * @method string	getEnContent
 * @method string	getEnMetaTitle
 * @method string	getEnMetaKeywords
 * @method string	getEnMetaDescription
 * @method integer	getEnVisible
 * @method string	getPic
 * @method integer	getPicW
 * @method integer	getPicH
 * @method string	getCreatedAt
 * @method integer	getOrderNum
 *
 * @method bool hasSlugSource
 * @method bool hasTitle
 * @method bool hasShortContent
 * @method bool hasContent
 * @method bool hasMetaTitle
 * @method bool hasMetaKeywords
 * @method bool hasMetaDescription
 * @method bool hasVisible
 * @method bool hasEnSlug
 * @method bool hasEnSlugSource
 * @method bool hasEnTitle
 * @method bool hasEnShortContent
 * @method bool hasEnContent
 * @method bool hasEnMetaTitle
 * @method bool hasEnMetaKeywords
 * @method bool hasEnMetaDescription
 * @method bool hasEnVisible
 * @method bool hasPic
 * @method bool hasPicW
 * @method bool hasPicH
 * @method bool hasCreatedAt
 * @method bool hasOrderNum
 *
 * @method $this setSlugSource($value)
 * @method $this setTitle($value)
 * @method $this setShortContent($value)
 * @method $this setContent($value)
 * @method $this setMetaTitle($value)
 * @method $this setMetaKeywords($value)
 * @method $this setMetaDescription($value)
 * @method $this setVisible($value)
 * @method $this setEnSlug($value)
 * @method $this setEnSlugSource($value)
 * @method $this setEnTitle($value)
 * @method $this setEnShortContent($value)
 * @method $this setEnContent($value)
 * @method $this setEnMetaTitle($value)
 * @method $this setEnMetaKeywords($value)
 * @method $this setEnMetaDescription($value)
 * @method $this setEnVisible($value)
 * @method $this setPic($value)
 * @method $this setPicW($value)
 * @method $this setPicH($value)
 * @method $this setCreatedAt($value)
 * @method $this setOrderNum($value)
 *
 * @method string	localizedSlugSource
 * @method string	localizedSlug
 * @method string	localizedTitle
 * @method string	localizedShortContent
 * @method string	localizedContent
 * @method string	localizedMetaTitle
 * @method string	localizedMetaKeywords
 * @method string	localizedMetaDescription
 * @method integer	localizedVisible
 */
abstract class Model extends \diModel
{
    use DefaultPic;

    const PAGE_TYPE_FOR_HREF = null;
    const SEPARATOR_FOR_HREF = '';

    protected static $picStoreSettings = [
        'pic' => [
            [
                'type' => Submit::IMAGE_TYPE_MAIN,
                'resize' => \diImage::DI_THUMB_FIT,
            ],
            [
                'type' => Submit::IMAGE_TYPE_PREVIEW,
                'resize' => \diImage::DI_THUMB_FIT,
            ],
        ],

        'pic2' => [
            [
                'type' => Submit::IMAGE_TYPE_MAIN,
                'resize' => \diImage::DI_THUMB_FIT,
            ],
            [
                'type' => Submit::IMAGE_TYPE_PREVIEW,
                'resize' => \diImage::DI_THUMB_FIT,
            ],
        ],

        'pic3' => [
            [
                'type' => Submit::IMAGE_TYPE_MAIN,
                'resize' => \diImage::DI_THUMB_FIT,
            ],
            [
                'type' => Submit::IMAGE_TYPE_PREVIEW,
                'resize' => \diImage::DI_THUMB_FIT,
            ],
        ],
    ];


    public function getHref()
    {
        $suffix = static::SEPARATOR_FOR_HREF === '#' ? '' : '/';

        return $this->__getPrefixForHref() . '/' . CMS::ct(static::PAGE_TYPE_FOR_HREF) . '/' .
            static::SEPARATOR_FOR_HREF . $this->getSlug() . $suffix;
    }

    public function getTemplateVars()
    {
        $this->beforeGet();

        $ar = parent::getTemplateVars();

        $this->afterGet();

        return $this->fixDefaultPicVars($ar);
    }
}