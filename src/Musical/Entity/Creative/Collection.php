<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:29
 */

namespace Musical\Entity\Creative;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByPosition($value, $operator = null)
 * @method $this filterByEnPosition($value, $operator = null)
 *
 * @method $this orderByPosition($direction = null)
 * @method $this orderByEnPosition($direction = null)
 *
 * @method $this selectPosition()
 * @method $this selectEnPosition()
 *
 * @method $this filterByLocalizedPosition($value, $operator = null)
 *
 * @method $this orderByLocalizedPosition($direction = null)
 *
 * @method $this selectLocalizedPosition()
 */
class Collection extends \Musical\Entity\BaseEntity\Collection
{
    const type = \diTypes::creative;
	const connection_name = 'default';
    protected $table = 'creative';
    protected $modelType = 'creative';
}
