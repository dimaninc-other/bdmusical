<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:13
 */

namespace Musical\Entity\Content;

use Musical\Traits\Collection\OldIdInside;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByEnCleanTitle($value, $operator = null)
 * @method $this filterByEnMenuTitle($value, $operator = null)
 * @method $this filterByEnTitle($value, $operator = null)
 * @method $this filterByEnCaption($value, $operator = null)
 * @method $this filterByEnShortContent($value, $operator = null)
 * @method $this filterByEnContent($value, $operator = null)
 * @method $this filterByEnHtmlTitle($value, $operator = null)
 * @method $this filterByEnHtmlKeywords($value, $operator = null)
 * @method $this filterByEnHtmlDescription($value, $operator = null)
 * @method $this filterByEnVisible($value, $operator = null)
 * @method $this filterByEnTop($value, $operator = null)
 *
 * @method $this orderByEnCleanTitle($direction = null)
 * @method $this orderByEnMenuTitle($direction = null)
 * @method $this orderByEnTitle($direction = null)
 * @method $this orderByEnCaption($direction = null)
 * @method $this orderByEnShortContent($direction = null)
 * @method $this orderByEnContent($direction = null)
 * @method $this orderByEnHtmlTitle($direction = null)
 * @method $this orderByEnHtmlKeywords($direction = null)
 * @method $this orderByEnHtmlDescription($direction = null)
 * @method $this orderByEnVisible($direction = null)
 * @method $this orderByEnTop($direction = null)
 *
 * @method $this selectEnCleanTitle()
 * @method $this selectEnMenuTitle()
 * @method $this selectEnTitle()
 * @method $this selectEnCaption()
 * @method $this selectEnShortContent()
 * @method $this selectEnContent()
 * @method $this selectEnHtmlTitle()
 * @method $this selectEnHtmlKeywords()
 * @method $this selectEnHtmlDescription()
 * @method $this selectEnVisible()
 * @method $this selectEnTop()
 *
 * @method $this filterByLocalizedCleanTitle($value, $operator = null)
 * @method $this filterByLocalizedMenuTitle($value, $operator = null)
 * @method $this filterByLocalizedTitle($value, $operator = null)
 * @method $this filterByLocalizedCaption($value, $operator = null)
 * @method $this filterByLocalizedShortContent($value, $operator = null)
 * @method $this filterByLocalizedContent($value, $operator = null)
 * @method $this filterByLocalizedHtmlTitle($value, $operator = null)
 * @method $this filterByLocalizedHtmlKeywords($value, $operator = null)
 * @method $this filterByLocalizedHtmlDescription($value, $operator = null)
 * @method $this filterByLocalizedVisible($value, $operator = null)
 * @method $this filterByLocalizedTop($value, $operator = null)
 *
 * @method $this orderByLocalizedCleanTitle($direction = null)
 * @method $this orderByLocalizedMenuTitle($direction = null)
 * @method $this orderByLocalizedTitle($direction = null)
 * @method $this orderByLocalizedCaption($direction = null)
 * @method $this orderByLocalizedShortContent($direction = null)
 * @method $this orderByLocalizedContent($direction = null)
 * @method $this orderByLocalizedHtmlTitle($direction = null)
 * @method $this orderByLocalizedHtmlKeywords($direction = null)
 * @method $this orderByLocalizedHtmlDescription($direction = null)
 * @method $this orderByLocalizedVisible($direction = null)
 * @method $this orderByLocalizedTop($direction = null)
 *
 * @method $this selectLocalizedCleanTitle()
 * @method $this selectLocalizedMenuTitle()
 * @method $this selectLocalizedTitle()
 * @method $this selectLocalizedCaption()
 * @method $this selectLocalizedShortContent()
 * @method $this selectLocalizedContent()
 * @method $this selectLocalizedHtmlTitle()
 * @method $this selectLocalizedHtmlKeywords()
 * @method $this selectLocalizedHtmlDescription()
 * @method $this selectLocalizedVisible()
 * @method $this selectLocalizedTop()
 */
class Collection extends \diCore\Entity\Content\Collection
{
    use OldIdInside;
}
