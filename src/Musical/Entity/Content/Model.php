<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:13
 */

namespace Musical\Entity\Content;

use diCore\Database\FieldType;
use Musical\Traits\Model\OldIdInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getEnCleanTitle
 * @method string	getEnMenuTitle
 * @method string	getEnTitle
 * @method string	getEnCaption
 * @method string	getEnShortContent
 * @method string	getEnContent
 * @method string	getEnHtmlTitle
 * @method string	getEnHtmlKeywords
 * @method string	getEnHtmlDescription
 * @method integer	getEnVisible
 * @method integer	getEnTop
 *
 * @method bool hasEnCleanTitle
 * @method bool hasEnMenuTitle
 * @method bool hasEnTitle
 * @method bool hasEnCaption
 * @method bool hasEnShortContent
 * @method bool hasEnContent
 * @method bool hasEnHtmlTitle
 * @method bool hasEnHtmlKeywords
 * @method bool hasEnHtmlDescription
 * @method bool hasEnVisible
 * @method bool hasEnTop
 *
 * @method $this setEnCleanTitle($value)
 * @method $this setEnMenuTitle($value)
 * @method $this setEnTitle($value)
 * @method $this setEnCaption($value)
 * @method $this setEnShortContent($value)
 * @method $this setEnContent($value)
 * @method $this setEnHtmlTitle($value)
 * @method $this setEnHtmlKeywords($value)
 * @method $this setEnHtmlDescription($value)
 * @method $this setEnVisible($value)
 * @method $this setEnTop($value)
 *
 * @method string	localizedCleanTitle
 * @method string	localizedMenuTitle
 * @method string	localizedTitle
 * @method string	localizedCaption
 * @method string	localizedShortContent
 * @method string	localizedContent
 * @method string	localizedHtmlTitle
 * @method string	localizedHtmlKeywords
 * @method string	localizedHtmlDescription
 * @method integer	localizedVisible
 * @method integer	localizedTop
 */
class Model extends \diCore\Entity\Content\Model
{
    use OldIdInside;

	protected $localizedFields = ['clean_title', 'menu_title', 'title', 'caption', 'short_content', 'content', 'html_title', 'html_keywords', 'html_description', 'visible', 'top'];

	protected static $fieldTypes = [
        'id' => FieldType::int,
        'old_id' => FieldType::int,
        'parent' => FieldType::int,
        'type' => FieldType::string,
        'clean_title' => FieldType::string,
        'menu_title' => FieldType::string,
        'title' => FieldType::string,
        'caption' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'html_title' => FieldType::string,
        'html_keywords' => FieldType::string,
        'html_description' => FieldType::string,
        'visible' => FieldType::int,
        'top' => FieldType::int,
        'en_clean_title' => FieldType::string,
        'en_menu_title' => FieldType::string,
        'en_title' => FieldType::string,
        'en_caption' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_html_title' => FieldType::string,
        'en_html_keywords' => FieldType::string,
        'en_html_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'en_top' => FieldType::int,
        'links_content' => FieldType::string,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic_t' => FieldType::int,
        'pic2' => FieldType::string,
        'pic2_w' => FieldType::int,
        'pic2_h' => FieldType::int,
        'pic2_t' => FieldType::int,
        'ico' => FieldType::string,
        'ico_w' => FieldType::int,
        'ico_h' => FieldType::int,
        'ico_t' => FieldType::int,
        'color' => FieldType::string,
        'background_color' => FieldType::string,
        'class' => FieldType::string,
        'menu_class' => FieldType::string,
        'level_num' => FieldType::int,
        'visible_top' => FieldType::int,
        'visible_bottom' => FieldType::int,
        'visible_left' => FieldType::int,
        'visible_right' => FieldType::int,
        'visible_logged_in' => FieldType::int,
        'to_show_content' => FieldType::int,
        'order_num' => FieldType::int,
        'comments_count' => FieldType::int,
        'comments_last_date' => FieldType::datetime,
        'comments_enabled' => FieldType::int,
        'ad_block_id' => FieldType::int,
    ];

    public function localized($field, $lang = null)
    {
        $v = parent::localized($field, $lang);

        return $v === null || $v === '' ? $this->get($field) : $v;
    }

    public function getRealHref()
    {
        global $Z;

        if (!empty($Z)) {
            return $Z->getRealContentModel($this)->getHref();
        }

        return $this->getHref();
    }

    public function getCustomTemplateVars()
    {
        return extend(parent::getCustomTemplateVars(), [
            'real_href' => $this->getRealHref(),
        ]);
    }
}
