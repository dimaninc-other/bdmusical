<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:32
 */

namespace Musical\Entity\SmiPress;

use Musical\Traits\Collection\OldIdInside;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterById($value, $operator = null)
 * @method $this filterBySlug($value, $operator = null)
 * @method $this filterBySlugSource($value, $operator = null)
 * @method $this filterBySeasonId($value, $operator = null)
 * @method $this filterByTitle($value, $operator = null)
 * @method $this filterBySource($value, $operator = null)
 * @method $this filterByShortContent($value, $operator = null)
 * @method $this filterByContent($value, $operator = null)
 * @method $this filterByMetaTitle($value, $operator = null)
 * @method $this filterByMetaKeywords($value, $operator = null)
 * @method $this filterByMetaDescription($value, $operator = null)
 * @method $this filterByVisible($value, $operator = null)
 * @method $this filterByEnSlug($value, $operator = null)
 * @method $this filterByEnSlugSource($value, $operator = null)
 * @method $this filterByEnTitle($value, $operator = null)
 * @method $this filterByEnShortContent($value, $operator = null)
 * @method $this filterByEnContent($value, $operator = null)
 * @method $this filterByEnMetaTitle($value, $operator = null)
 * @method $this filterByEnMetaKeywords($value, $operator = null)
 * @method $this filterByEnMetaDescription($value, $operator = null)
 * @method $this filterByEnVisible($value, $operator = null)
 * @method $this filterByPic($value, $operator = null)
 * @method $this filterByPicW($value, $operator = null)
 * @method $this filterByPicH($value, $operator = null)
 * @method $this filterByPicT($value, $operator = null)
 * @method $this filterByPicTnW($value, $operator = null)
 * @method $this filterByPicTnH($value, $operator = null)
 * @method $this filterByDate($value, $operator = null)
 * @method $this filterByCreatedAt($value, $operator = null)
 * @method $this filterByOrderNum($value, $operator = null)
 *
 * @method $this orderById($direction = null)
 * @method $this orderBySlug($direction = null)
 * @method $this orderBySlugSource($direction = null)
 * @method $this orderBySeasonId($direction = null)
 * @method $this orderByTitle($direction = null)
 * @method $this orderBySource($direction = null)
 * @method $this orderByShortContent($direction = null)
 * @method $this orderByContent($direction = null)
 * @method $this orderByMetaTitle($direction = null)
 * @method $this orderByMetaKeywords($direction = null)
 * @method $this orderByMetaDescription($direction = null)
 * @method $this orderByVisible($direction = null)
 * @method $this orderByEnSlug($direction = null)
 * @method $this orderByEnSlugSource($direction = null)
 * @method $this orderByEnTitle($direction = null)
 * @method $this orderByEnShortContent($direction = null)
 * @method $this orderByEnContent($direction = null)
 * @method $this orderByEnMetaTitle($direction = null)
 * @method $this orderByEnMetaKeywords($direction = null)
 * @method $this orderByEnMetaDescription($direction = null)
 * @method $this orderByEnVisible($direction = null)
 * @method $this orderByPic($direction = null)
 * @method $this orderByPicW($direction = null)
 * @method $this orderByPicH($direction = null)
 * @method $this orderByPicT($direction = null)
 * @method $this orderByPicTnW($direction = null)
 * @method $this orderByPicTnH($direction = null)
 * @method $this orderByDate($direction = null)
 * @method $this orderByCreatedAt($direction = null)
 * @method $this orderByOrderNum($direction = null)
 *
 * @method $this selectId()
 * @method $this selectSlug()
 * @method $this selectSlugSource()
 * @method $this selectSeasonId()
 * @method $this selectTitle()
 * @method $this selectSource()
 * @method $this selectShortContent()
 * @method $this selectContent()
 * @method $this selectMetaTitle()
 * @method $this selectMetaKeywords()
 * @method $this selectMetaDescription()
 * @method $this selectVisible()
 * @method $this selectEnSlug()
 * @method $this selectEnSlugSource()
 * @method $this selectEnTitle()
 * @method $this selectEnShortContent()
 * @method $this selectEnContent()
 * @method $this selectEnMetaTitle()
 * @method $this selectEnMetaKeywords()
 * @method $this selectEnMetaDescription()
 * @method $this selectEnVisible()
 * @method $this selectPic()
 * @method $this selectPicW()
 * @method $this selectPicH()
 * @method $this selectPicT()
 * @method $this selectPicTnW()
 * @method $this selectPicTnH()
 * @method $this selectDate()
 * @method $this selectCreatedAt()
 * @method $this selectOrderNum()
 *
 * @method $this filterByLocalizedSlug($value, $operator = null)
 * @method $this filterByLocalizedSlugSource($value, $operator = null)
 * @method $this filterByLocalizedTitle($value, $operator = null)
 * @method $this filterByLocalizedShortContent($value, $operator = null)
 * @method $this filterByLocalizedContent($value, $operator = null)
 * @method $this filterByLocalizedMetaTitle($value, $operator = null)
 * @method $this filterByLocalizedMetaKeywords($value, $operator = null)
 * @method $this filterByLocalizedMetaDescription($value, $operator = null)
 * @method $this filterByLocalizedVisible($value, $operator = null)
 *
 * @method $this orderByLocalizedSlug($direction = null)
 * @method $this orderByLocalizedSlugSource($direction = null)
 * @method $this orderByLocalizedTitle($direction = null)
 * @method $this orderByLocalizedShortContent($direction = null)
 * @method $this orderByLocalizedContent($direction = null)
 * @method $this orderByLocalizedMetaTitle($direction = null)
 * @method $this orderByLocalizedMetaKeywords($direction = null)
 * @method $this orderByLocalizedMetaDescription($direction = null)
 * @method $this orderByLocalizedVisible($direction = null)
 *
 * @method $this selectLocalizedSlug()
 * @method $this selectLocalizedSlugSource()
 * @method $this selectLocalizedTitle()
 * @method $this selectLocalizedShortContent()
 * @method $this selectLocalizedContent()
 * @method $this selectLocalizedMetaTitle()
 * @method $this selectLocalizedMetaKeywords()
 * @method $this selectLocalizedMetaDescription()
 * @method $this selectLocalizedVisible()
 */
class Collection extends \diCollection
{
    use OldIdInside;

    const type = \diTypes::smi_press;
	const connection_name = 'default';
    protected $table = 'smi_press';
    protected $modelType = 'smi_press';
}
