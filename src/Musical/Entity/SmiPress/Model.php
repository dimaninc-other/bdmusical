<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:32
 */

namespace Musical\Entity\SmiPress;

use diCore\Database\FieldType;
use Musical\Entity\Media\Model as Media;
use Musical\Traits\Model\OldIdInside;
use Musical\Traits\Model\SeasonInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getSource
 * @method integer	getPicT
 * @method integer	getPicTnW
 * @method integer	getPicTnH
 * @method string	getDate
 *
 * @method bool hasSource
 * @method bool hasPicT
 * @method bool hasPicTnW
 * @method bool hasPicTnH
 * @method bool hasDate
 *
 * @method $this setSource($value)
 * @method $this setPicT($value)
 * @method $this setPicTnW($value)
 * @method $this setPicTnH($value)
 * @method $this setDate($value)
 */
class Model extends \Musical\Entity\BaseEntity\Model
{
    use SeasonInside;
    use OldIdInside;

    const type = \diTypes::smi_press;
	const connection_name = 'default';
    const table = 'smi_press';
    protected $table = 'smi_press';
	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'slug_source', 'title', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible'];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'season_id' => FieldType::int,
        'title' => FieldType::string,
        'source' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic_t' => FieldType::int,
        'pic_tn_w' => FieldType::int,
        'pic_tn_h' => FieldType::int,
        'date' => FieldType::datetime,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];

    public function getHref()
    {
        return Media::mediaHref($this);
    }

    public function afterSave()
    {
        Media::forceUpdateOrCreate($this);

        return parent::afterSave();
    }

    protected function afterKill()
    {
        Media::forceUpdateOrCreate($this);

        return parent::afterKill();
    }

    public function afterToggleVisible()
    {
        Media::forceUpdateOrCreate($this);
    }

    public function afterToggleEnVisible()
    {
        $this->afterToggleVisible();
    }
}
