<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:29
 */

namespace Musical\Entity\Client;

use diCore\Database\FieldType;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getTitle
 * @method string	getCaption
 * @method string	getContent
 * @method integer	getVisible
 * @method string	getEnTitle
 * @method string	getEnCaption
 * @method string	getEnContent
 * @method integer	getEnVisible
 * @method string	getPic
 * @method integer	getPicW
 * @method integer	getPicH
 * @method string	getCreatedAt
 * @method integer	getOrderNum
 *
 * @method bool hasHref
 * @method bool hasTitle
 * @method bool hasCaption
 * @method bool hasContent
 * @method bool hasVisible
 * @method bool hasEnTitle
 * @method bool hasEnCaption
 * @method bool hasEnContent
 * @method bool hasEnVisible
 * @method bool hasPic
 * @method bool hasPicW
 * @method bool hasPicH
 * @method bool hasCreatedAt
 * @method bool hasOrderNum
 *
 * @method $this setHref($value)
 * @method $this setTitle($value)
 * @method $this setCaption($value)
 * @method $this setContent($value)
 * @method $this setVisible($value)
 * @method $this setEnTitle($value)
 * @method $this setEnCaption($value)
 * @method $this setEnContent($value)
 * @method $this setEnVisible($value)
 * @method $this setPic($value)
 * @method $this setPicW($value)
 * @method $this setPicH($value)
 * @method $this setCreatedAt($value)
 * @method $this setOrderNum($value)
 *
 * @method string	localizedTitle
 * @method string	localizedCaption
 * @method string	localizedContent
 * @method integer	localizedVisible
 */
class Model extends \diModel
{
    const type = \diTypes::client;
	const connection_name = 'default';
    const table = 'client';
    protected $table = 'client';
	protected $localizedFields = ['title', 'caption', 'content', 'visible'];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'href' => FieldType::string,
        'title' => FieldType::string,
        'caption' => FieldType::string,
        'content' => FieldType::string,
        'visible' => FieldType::int,
        'en_title' => FieldType::string,
        'en_caption' => FieldType::string,
        'en_content' => FieldType::string,
        'en_visible' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];

    public function getHref()
    {
        return $this->get('href');
    }
}
