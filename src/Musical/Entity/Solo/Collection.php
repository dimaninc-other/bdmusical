<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:32
 */

namespace Musical\Entity\Solo;

use Musical\Traits\Collection\OldIdInside;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByCharacterId($value, $operator = null)
 * @method $this filterByHomeCssClass($value, $operator = null)
 * @method $this filterByActive($value, $operator = null)
 * @method $this filterByEnActive($value, $operator = null)
 * @method $this filterByPic2($value, $operator = null)
 * @method $this filterByPic2W($value, $operator = null)
 * @method $this filterByPic2H($value, $operator = null)
 * @method $this filterByPic3($value, $operator = null)
 * @method $this filterByShowPic($value, $operator = null)
 *
 * @method $this orderByCharacterId($direction = null)
 * @method $this orderByHomeCssClass($direction = null)
 * @method $this orderByActive($direction = null)
 * @method $this orderByEnActive($direction = null)
 * @method $this orderByPic2($direction = null)
 * @method $this orderByPic2W($direction = null)
 * @method $this orderByPic2H($direction = null)
 * @method $this orderByPic3($direction = null)
 * @method $this orderByShowPic($direction = null)
 *
 * @method $this selectCharacterId()
 * @method $this selectHomeCssClass()
 * @method $this selectActive()
 * @method $this selectEnActive()
 * @method $this selectPic2()
 * @method $this selectPic2W()
 * @method $this selectPic2H()
 * @method $this selectPic3()
 * @method $this selectShowPic()
 *
 * @method $this filterByLocalizedActive($value, $operator = null)
 *
 * @method $this orderByLocalizedActive($direction = null)
 *
 * @method $this selectLocalizedActive()
 */
class Collection extends \Musical\Entity\BaseEntity\Collection
{
    use OldIdInside;

    const type = \diTypes::solo;
	const connection_name = 'default';
    protected $table = 'solo';
    protected $modelType = 'solo';

    public static function createByShowId($showId)
    {
        $c = static::createEmpty();

        $rs = $showId
            ? self::db()->rs(
                "solo s INNER JOIN show_solo_link l ON s.id = l.solo_id",
                "WHERE l.show_id = '$showId'",
                "s.*"
            )
            : null;
        while ($rs && $r = self::db()->fetch($rs)) {
            $c->addItem(Model::create(static::type, $r));
        }

        return $c;
    }
}
