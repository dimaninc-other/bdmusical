<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:32
 */

namespace Musical\Entity\Solo;

use diCore\Database\FieldType;
use diCore\Helper\StringHelper;
use diCore\Tool\CollectionCache;
use Musical\Entity\Character\Model as Character;
use Musical\Traits\Model\CssClassInside;
use Musical\Traits\Model\OldIdInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getCharacterId
 * @method string	getHomeCssClass
 * @method integer	getActive
 * @method integer	getEnActive
 * @method string	getPic2
 * @method integer	getPic2W
 * @method integer	getPic2H
 * @method string	getPic3
 * @method integer	getShowPic
 *
 * @method bool hasCharacterId
 * @method bool hasHomeCssClass
 * @method bool hasActive
 * @method bool hasEnActive
 * @method bool hasPic2
 * @method bool hasPic2W
 * @method bool hasPic2H
 * @method bool hasPic3
 * @method bool hasShowPic
 *
 * @method $this setCharacterId($value)
 * @method $this setHomeCssClass($value)
 * @method $this setActive($value)
 * @method $this setEnActive($value)
 * @method $this setPic2($value)
 * @method $this setPic2W($value)
 * @method $this setPic2H($value)
 * @method $this setPic3($value)
 * @method $this setShowPic($value)
 *
 * @method integer	localizedActive
 */
class Model extends \Musical\Entity\BaseEntity\Model
{
    use CssClassInside;
    use OldIdInside;

    const type = \diTypes::solo;
	const connection_name = 'default';
    const table = 'solo';
    protected $table = 'solo';
	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'slug_source', 'title', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible', 'active'];
    protected $customPicFields = ['pic3'];

	const PAGE_TYPE_FOR_HREF = 'solos';
	const SEPARATOR_FOR_HREF = '#';

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'character_id' => FieldType::string,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'css_class' => FieldType::string,
        'home_css_class' => FieldType::string,
        'title' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'active' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'en_active' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::string,
        'pic_h' => FieldType::string,
        'pic2' => FieldType::string,
        'pic2_w' => FieldType::int,
        'pic2_h' => FieldType::int,
        'pic3' => FieldType::string,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
        'show_pic' => FieldType::int,
    ];

	/** @var Character */
	protected $character;

	public function getCharacter()
    {
        if (!$this->character) {
            $this->character = CollectionCache::getModel(Character::type, $this->getCharacterId(), true);
        }

        return $this->character;
    }

    public static function nlName($name)
    {
        $x = strpos($name, ' ');

        if ($x !== false) {
            $name = substr($name, 0, $x) .
                ' <div class="surname">' . substr($name, $x) . '</div>';
        }

	    return $name;
    }

    public function getCustomTemplateVars()
    {
        return extend(parent::getCustomTemplateVars(), [
            'localized_title_with_nl' => self::nlName($this->localizedTitle()),
        ]);
    }
}
