<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:32
 */

namespace Musical\Entity\SmiRadio;

use diCore\Database\FieldType;
use Musical\Entity\Media\Model as Media;
use Musical\Traits\Model\OldIdInside;
use Musical\Traits\Model\SeasonInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getVendor
 * @method string	getVendorAudioUid
 * @method string	getSource
 * @method string	getEnSource
 * @method string	getEmbed
 * @method integer	getViewsCount
 * @method integer	getPicT
 * @method integer	getPicTnW
 * @method integer	getPicTnH
 * @method string	getDate
 *
 * @method bool hasVendor
 * @method bool hasVendorAudioUid
 * @method bool hasSource
 * @method bool hasEnSource
 * @method bool hasEmbed
 * @method bool hasViewsCount
 * @method bool hasPicT
 * @method bool hasPicTnW
 * @method bool hasPicTnH
 * @method bool hasDate
 *
 * @method $this setVendor($value)
 * @method $this setVendorAudioUid($value)
 * @method $this setSource($value)
 * @method $this setEnSource($value)
 * @method $this setEmbed($value)
 * @method $this setViewsCount($value)
 * @method $this setPicT($value)
 * @method $this setPicTnW($value)
 * @method $this setPicTnH($value)
 * @method $this setDate($value)
 *
 * @method string	localizedSource
 */
class Model extends \Musical\Entity\BaseEntity\Model
{
    use SeasonInside;
    use OldIdInside;

    const type = \diTypes::smi_radio;
	const connection_name = 'default';
    const table = 'smi_radio';
    protected $table = 'smi_radio';
	const slug_field_name = self::SLUG_FIELD_NAME;
	protected $localizedFields = ['slug', 'slug_source', 'title', 'source', 'short_content', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible'];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'season_id' => FieldType::int,
        'vendor' => FieldType::int,
        'vendor_audio_uid' => FieldType::string,
        'title' => FieldType::string,
        'source' => FieldType::string,
        'short_content' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_source' => FieldType::string,
        'en_short_content' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'embed' => FieldType::string,
        'views_count' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic_t' => FieldType::int,
        'pic_tn_w' => FieldType::int,
        'pic_tn_h' => FieldType::int,
        'date' => FieldType::datetime,
        'created_at' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];

    public function getToken()
    {
        return $this->hasId()
            ? '[AUDIO-' . str_pad($this->getId(), 6, '0', STR_PAD_LEFT) . ']'
            : null;
    }

    public function getHref()
    {
        return Media::mediaHref($this);
    }

    /**
     * Custom model template vars
     *
     * @return array
     */
    public function getCustomTemplateVars()
    {
        $ar = [
            'token' => $this->getToken(),
        ];

        if ($this->getVendor() != \diAudioVendors::Own) {
            $ar['vendor_link'] = \diAudioVendors::getLink($this->getVendor(), $this->getVendorAudioUid());
            $ar['vendor_embed_link'] = \diAudioVendors::getEmbedLink($this->getVendor(), $this->getVendorAudioUid());
            $ar['vendor_embed_code'] = \diAudioVendors::getEmbedCode($this->getVendor(), $this->getVendorAudioUid());

            if ($this->hasEmbed() && preg_match('/<iframe/', $this->getEmbed())) {
                $ar['embed_safe'] = $this->getEmbed();
            } else {
                $ar['embed_safe'] = $ar['vendor_embed_code'];
            }
        }

        return $ar;
    }

    public function afterSave()
    {
        Media::forceUpdateOrCreate($this);

        return parent::afterSave();
    }

    protected function afterKill()
    {
        Media::forceUpdateOrCreate($this);

        return parent::afterKill();
    }

    public function afterToggleVisible()
    {
        Media::forceUpdateOrCreate($this);
    }

    public function afterToggleEnVisible()
    {
        $this->afterToggleVisible();
    }
}
