<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:27
 */

namespace Musical\Entity\Photo;

use diCore\Admin\Submit;
use diCore\Database\FieldType;
use Musical\Traits\Model\OldIdInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method string	getMetaTitle
 * @method string	getMetaKeywords
 * @method string	getMetaDescription
 * @method string	getEnSlug
 * @method string	getEnSlugSource
 * @method string	getEnTitle
 * @method string	getEnContent
 * @method string	getEnMetaTitle
 * @method string	getEnMetaKeywords
 * @method string	getEnMetaDescription
 * @method integer	getEnVisible
 * @method integer	getEnTop
 *
 * @method bool hasMetaTitle
 * @method bool hasMetaKeywords
 * @method bool hasMetaDescription
 * @method bool hasEnSlug
 * @method bool hasEnSlugSource
 * @method bool hasEnTitle
 * @method bool hasEnContent
 * @method bool hasEnMetaTitle
 * @method bool hasEnMetaKeywords
 * @method bool hasEnMetaDescription
 * @method bool hasEnVisible
 * @method bool hasEnTop
 *
 * @method $this setMetaTitle($value)
 * @method $this setMetaKeywords($value)
 * @method $this setMetaDescription($value)
 * @method $this setEnSlug($value)
 * @method $this setEnSlugSource($value)
 * @method $this setEnTitle($value)
 * @method $this setEnContent($value)
 * @method $this setEnMetaTitle($value)
 * @method $this setEnMetaKeywords($value)
 * @method $this setEnMetaDescription($value)
 * @method $this setEnVisible($value)
 * @method $this setEnTop($value)
 *
 * @method string	localizedSlug
 * @method string	localizedSlugSource
 * @method string	localizedTitle
 * @method string	localizedContent
 * @method string	localizedMetaTitle
 * @method string	localizedMetaKeywords
 * @method string	localizedMetaDescription
 * @method integer	localizedVisible
 * @method integer	localizedTop
 */
class Model extends \diCore\Entity\Photo\Model
{
    use OldIdInside;

    public static $picOptions = [
        [
            'type' => Submit::IMAGE_TYPE_MAIN,
            'resize' => \diImage::DI_THUMB_FIT,
        ],
        [
            'type' => Submit::IMAGE_TYPE_PREVIEW,
            'resize' => \diImage::DI_THUMB_FIT,
        ],
        [
            'type' => Submit::IMAGE_TYPE_ORIG,
        ],
    ];

	protected $localizedFields = ['slug', 'slug_source', 'title', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible', 'top'];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'album_id' => FieldType::string,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'title' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'top' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'en_top' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic_t' => FieldType::int,
        'pic_tn_w' => FieldType::int,
        'pic_tn_h' => FieldType::int,
        'comments_enabled' => FieldType::int,
        'comments_last_date' => FieldType::datetime,
        'comments_count' => FieldType::int,
        'date' => FieldType::timestamp,
        'order_num' => FieldType::int,
    ];

	public function afterSave()
    {
        parent::afterSave();

        $photos = Collection::create();
        $photos
            ->filterByAlbumId($this->getAlbumId());

        $this->getAlbum()
            ->setPhotosCount($photos->count())
            ->save();

        \Musical\Entity\Media\Model::forceUpdateOrCreate($this->getAlbum());

        return $this;
    }

    protected function afterKill()
    {
        \Musical\Entity\Media\Model::forceUpdateOrCreate($this->getAlbum());

        return parent::afterKill();
    }

    public function afterToggleVisible()
    {
        \Musical\Entity\Media\Model::forceUpdateOrCreate($this->getAlbum());
    }

    public function afterToggleEnVisible()
    {
        $this->afterToggleVisible();
    }
}
