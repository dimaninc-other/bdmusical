<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:27
 */

namespace Musical\Entity\Video;

use Musical\Traits\Collection\OldIdInside;

/**
 * Class Collection
 * Methods list for IDE
 *
 * @method $this filterByPurpose($value, $operator = null)
 * @method $this filterByMetaTitle($value, $operator = null)
 * @method $this filterByMetaKeywords($value, $operator = null)
 * @method $this filterByMetaDescription($value, $operator = null)
 * @method $this filterByEnSlug($value, $operator = null)
 * @method $this filterByEnSlugSource($value, $operator = null)
 * @method $this filterByEnTitle($value, $operator = null)
 * @method $this filterByEnContent($value, $operator = null)
 * @method $this filterByEnMetaTitle($value, $operator = null)
 * @method $this filterByEnMetaKeywords($value, $operator = null)
 * @method $this filterByEnMetaDescription($value, $operator = null)
 * @method $this filterByEnVisible($value, $operator = null)
 * @method $this filterByEnTop($value, $operator = null)
 *
 * @method $this orderByPurpose($direction = null)
 * @method $this orderByMetaTitle($direction = null)
 * @method $this orderByMetaKeywords($direction = null)
 * @method $this orderByMetaDescription($direction = null)
 * @method $this orderByEnSlug($direction = null)
 * @method $this orderByEnSlugSource($direction = null)
 * @method $this orderByEnTitle($direction = null)
 * @method $this orderByEnContent($direction = null)
 * @method $this orderByEnMetaTitle($direction = null)
 * @method $this orderByEnMetaKeywords($direction = null)
 * @method $this orderByEnMetaDescription($direction = null)
 * @method $this orderByEnVisible($direction = null)
 * @method $this orderByEnTop($direction = null)
 *
 * @method $this selectPurpose()
 * @method $this selectMetaTitle()
 * @method $this selectMetaKeywords()
 * @method $this selectMetaDescription()
 * @method $this selectEnSlug()
 * @method $this selectEnSlugSource()
 * @method $this selectEnTitle()
 * @method $this selectEnContent()
 * @method $this selectEnMetaTitle()
 * @method $this selectEnMetaKeywords()
 * @method $this selectEnMetaDescription()
 * @method $this selectEnVisible()
 * @method $this selectEnTop()
 *
 * @method $this filterByLocalizedSlug($value, $operator = null)
 * @method $this filterByLocalizedSlugSource($value, $operator = null)
 * @method $this filterByLocalizedTitle($value, $operator = null)
 * @method $this filterByLocalizedContent($value, $operator = null)
 * @method $this filterByLocalizedMetaTitle($value, $operator = null)
 * @method $this filterByLocalizedMetaKeywords($value, $operator = null)
 * @method $this filterByLocalizedMetaDescription($value, $operator = null)
 * @method $this filterByLocalizedVisible($value, $operator = null)
 * @method $this filterByLocalizedTop($value, $operator = null)
 *
 * @method $this orderByLocalizedSlug($direction = null)
 * @method $this orderByLocalizedSlugSource($direction = null)
 * @method $this orderByLocalizedTitle($direction = null)
 * @method $this orderByLocalizedContent($direction = null)
 * @method $this orderByLocalizedMetaTitle($direction = null)
 * @method $this orderByLocalizedMetaKeywords($direction = null)
 * @method $this orderByLocalizedMetaDescription($direction = null)
 * @method $this orderByLocalizedVisible($direction = null)
 * @method $this orderByLocalizedTop($direction = null)
 *
 * @method $this selectLocalizedSlug()
 * @method $this selectLocalizedSlugSource()
 * @method $this selectLocalizedTitle()
 * @method $this selectLocalizedContent()
 * @method $this selectLocalizedMetaTitle()
 * @method $this selectLocalizedMetaKeywords()
 * @method $this selectLocalizedMetaDescription()
 * @method $this selectLocalizedVisible()
 * @method $this selectLocalizedTop()
 */
class Collection extends \diCore\Entity\Video\Collection
{
    use OldIdInside;
}
