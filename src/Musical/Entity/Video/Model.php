<?php
/**
 * Created by \diModelsManager
 * Date: 12.04.2021
 * Time: 18:27
 */

namespace Musical\Entity\Video;

use diCore\Admin\Submit;
use diCore\Database\FieldType;
use Musical\Entity\Media\Model as Media;
use Musical\Traits\Model\OldIdInside;
use Musical\Traits\Model\SeasonInside;

/**
 * Class Model
 * Methods list for IDE
 *
 * @method integer	getPurpose
 * @method string	getMetaTitle
 * @method string	getMetaKeywords
 * @method string	getMetaDescription
 * @method string	getEnSlug
 * @method string	getEnSlugSource
 * @method string	getEnTitle
 * @method string	getEnContent
 * @method string	getEnMetaTitle
 * @method string	getEnMetaKeywords
 * @method string	getEnMetaDescription
 * @method integer	getEnVisible
 * @method integer	getEnTop
 *
 * @method bool hasPurpose
 * @method bool hasMetaTitle
 * @method bool hasMetaKeywords
 * @method bool hasMetaDescription
 * @method bool hasEnSlug
 * @method bool hasEnSlugSource
 * @method bool hasEnTitle
 * @method bool hasEnContent
 * @method bool hasEnMetaTitle
 * @method bool hasEnMetaKeywords
 * @method bool hasEnMetaDescription
 * @method bool hasEnVisible
 * @method bool hasEnTop
 *
 * @method $this setPurpose($value)
 * @method $this setMetaTitle($value)
 * @method $this setMetaKeywords($value)
 * @method $this setMetaDescription($value)
 * @method $this setEnSlug($value)
 * @method $this setEnSlugSource($value)
 * @method $this setEnTitle($value)
 * @method $this setEnContent($value)
 * @method $this setEnMetaTitle($value)
 * @method $this setEnMetaKeywords($value)
 * @method $this setEnMetaDescription($value)
 * @method $this setEnVisible($value)
 * @method $this setEnTop($value)
 *
 * @method string	localizedSlug
 * @method string	localizedSlugSource
 * @method string	localizedTitle
 * @method string	localizedContent
 * @method string	localizedMetaTitle
 * @method string	localizedMetaKeywords
 * @method string	localizedMetaDescription
 * @method integer	localizedVisible
 * @method integer	localizedTop
 */
class Model extends \diCore\Entity\Video\Model
{
    use SeasonInside;
    use OldIdInside;

	protected $localizedFields = ['slug', 'slug_source', 'title', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'visible', 'top'];

	protected static $fieldTypes = [
        'id' => FieldType::string,
        'season_id' => FieldType::int,
        'album_id' => FieldType::string,
        'purpose' => FieldType::int,
        'vendor' => FieldType::int,
        'vendor_video_uid' => FieldType::string,
        'slug' => FieldType::string,
        'slug_source' => FieldType::string,
        'title' => FieldType::string,
        'content' => FieldType::string,
        'meta_title' => FieldType::string,
        'meta_keywords' => FieldType::string,
        'meta_description' => FieldType::string,
        'visible' => FieldType::int,
        'top' => FieldType::int,
        'en_slug' => FieldType::string,
        'en_slug_source' => FieldType::string,
        'en_title' => FieldType::string,
        'en_content' => FieldType::string,
        'en_meta_title' => FieldType::string,
        'en_meta_keywords' => FieldType::string,
        'en_meta_description' => FieldType::string,
        'en_visible' => FieldType::int,
        'en_top' => FieldType::int,
        'embed' => FieldType::string,
        'video_mp4' => FieldType::string,
        'video_m4v' => FieldType::string,
        'video_ogv' => FieldType::string,
        'video_webm' => FieldType::string,
        'video_w' => FieldType::int,
        'video_h' => FieldType::int,
        'pic' => FieldType::string,
        'pic_w' => FieldType::int,
        'pic_h' => FieldType::int,
        'pic_t' => FieldType::int,
        'pic_tn_w' => FieldType::int,
        'pic_tn_h' => FieldType::int,
        'views_count' => FieldType::string,
        'date' => FieldType::timestamp,
        'order_num' => FieldType::int,
        'comments_enabled' => FieldType::int,
        'comments_last_date' => FieldType::datetime,
        'comments_count' => FieldType::int,
    ];

    protected static $picStoreSettings = [
        'pic' => [
            [
                'type' => Submit::IMAGE_TYPE_MAIN,
                'resize' => \diImage::DI_THUMB_CROP,
                'forceFormat' => 'jpeg',
                'quality' => 84,
            ],
        ],
    ];

    public function getHref()
    {
        return Media::mediaHref($this);
    }

    public function reallyExists()
    {
        return $this->hasVendorVideoUid() || $this->hasVideoMp4();
    }

    public function afterSave()
    {
        Media::forceUpdateOrCreate($this);

        return parent::afterSave();
    }

    protected function afterKill()
    {
        Media::forceUpdateOrCreate($this);

        return parent::afterKill();
    }

    public function afterToggleVisible()
    {
        Media::forceUpdateOrCreate($this);
    }

    public function afterToggleEnVisible()
    {
        $this->afterToggleVisible();
    }
}
