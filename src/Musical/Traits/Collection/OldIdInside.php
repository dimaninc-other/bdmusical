<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 21.04.2021
 * Time: 17:48
 */

namespace Musical\Traits\Collection;

/**
 * Trait OldIdInside
 * @package Musical\Traits\Collection
 *
 * @method $this filterByOldId($value, $operator = null)
 *
 * @method $this orderByOldId($direction = null)
 *
 * @method $this selectOldId()
 */
trait OldIdInside
{
}
