<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 27.06.2016
 * Time: 13:11
 */

namespace Musical\Traits\Model;

use diCore\Helper\StringHelper;

trait DefaultPic
{
    protected $defaultPicPath = '/assets/images/default/';
	protected $defaultPic = '/assets/images/default/basic.svg';
	protected $defaultPicWidth = 365;
	protected $defaultPicHeight = 206;

	protected $revertNeeded = false;
	protected $enRevertNeeded = false;

	protected function beforeGet()
	{
		$this->set('real_pic', $this->getPic());
		$this->set('real_en_pic', $this->getEnPic());

		if (!$this->hasPic()) {
			$this
				->setPicsFolder('')
				->setTnFolder('')
				->setPic($this->defaultPic)
				->setPicW($this->defaultPicWidth)
				->setPicH($this->defaultPicHeight)
				->setPicTnW($this->defaultPicWidth)
				->setPicTnH($this->defaultPicHeight);

			$this->revertNeeded = true;
		}

		/*
		if ($m->isFieldLocalized('pic') && !$m->hasEnPic()) {
			$m
				->setEnPic($this['pic_with_path']);

			$this->enRevertNeeded = true;
		}
		*/

		return $this;
	}

	protected function afterGet()
	{
		if ($this->revertNeeded) {
			$this
				->setPicsFolder(null)
				->setTnFolder(null)
				->setPic('')
				->setPicW(0)
				->setPicH(0)
				->setPicTnW(0)
				->setPicTnH(0);
		}

		if ($this->isFieldLocalized('pic') && $this->enRevertNeeded) {
			$this
				->setEnPic('');
		}

        return $this;
	}

	protected function fixDefaultPicVars($ar)
    {
        $fields = ['pic', 'pic_tn'];

        foreach ($fields as $field) {
            if (
                !empty($ar[$field . '_with_path']) &&
                StringHelper::contains($ar[$field . '_with_path'], $this->defaultPicPath)
                // && StringHelper::contains($ar[$field . '_with_path'], $this->getFolderForField($field))
            ) {
                /*
                $ar[$field . '_with_path'] = mb_substr(
                    $ar[$field . '_with_path'],
                    mb_strlen($this->getFolderForField($field))
                );
                */
                $ar[$field . '_with_path'] = $this->defaultPic;
            }
        }

        return $ar;
    }
}
