<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 16.04.2021
 * Time: 11:50
 */

namespace Musical\Traits\Model;

use diCore\Tool\CollectionCache;
use Musical\Entity\Season\Model;

/**
 * Trait SeasonInside
 * @package Musical\Traits\Model
 *
 * @method integer	getSeasonId
 *
 * @method bool hasSeasonId
 *
 * @method $this setSeasonId($value)
 */
trait SeasonInside
{
    /** @var Model */
    protected $season;

    public function getSeason()
    {
        if (!$this->season) {
            $this->season = CollectionCache::getModel(Model::type, $this->getSeasonId(), true);
        }

        return $this->season;
    }
}
