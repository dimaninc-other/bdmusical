<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 21.04.2021
 * Time: 17:47
 */

namespace Musical\Traits\Model;

/**
 * Trait OldIdInside
 * @package Musical\Traits\Model
 *
 * @method integer	getOldId
 *
 * @method bool hasOldId
 *
 * @method $this setOldId($value)
 */
trait OldIdInside
{
}
