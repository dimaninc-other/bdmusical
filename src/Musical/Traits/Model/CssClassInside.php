<?php

namespace Musical\Traits\Model;

/**
 * Trait SeasonInside
 * @package Musical\Traits\Model
 *
 * @method integer getCssClass
 *
 * @method bool hasCssClass
 *
 * @method $this setCssClass($value)
 */
trait CssClassInside
{
}