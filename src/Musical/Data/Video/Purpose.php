<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 08.01.2017
 * Time: 17:53
 */

namespace Musical\Data\Video;

use diCore\Tool\SimpleContainer;

class Purpose extends SimpleContainer
{
	const NONE = 0;
	const HOME_TRAILER = 1;
	const HOME_ENG_TRAILER = 2;
	const DVD = 3;

	public static $names = [
		self::NONE => 'NONE',
		self::HOME_TRAILER => 'HOME_TRAILER',
		self::HOME_ENG_TRAILER => 'HOME_ENG_TRAILER',
        self::DVD => 'DVD',
	];

	public static $titles = [
		self::NONE => 'Отсутствует (просто видео)',
		self::HOME_TRAILER => 'Русский трейлер',
		self::HOME_ENG_TRAILER => 'Английский трейлер',
        self::DVD => 'DVD',
	];
}
