<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 13.04.2021
 * Time: 18:31
 */

namespace Musical\Data\Content;

class Types extends \diContentTypesBasic
{
    protected static $types = [
        'about' => 'О проекте',
        'press_release' => 'Пресс-релиз',
        'news' => 'Новости',
        'our_theatre' => 'Наш театр',
        'reviews' => 'Отзывы',
        'partners' => 'Партнеры',
        'tickets' => 'Билеты',
        'tickets_online' => 'Билеты онлайн',
        'groups' => 'Групповые заявки',
        'trust' => 'Нам доверяют',
        'staff' => 'Составы',
        'actions' => 'Акции',
        'e_ticket' => 'Электронный билет',
        'creators' => 'Создатели',
        'creative_team' => 'Творческая группа',
        'troupe' => 'Труппа',
        'solos' => 'Солисты',
        'ensemble' => 'Ансамбль',
        'orchestra' => 'Оркестр',
        'ballets' => 'Балет',
        'skaters' => 'Фигуристы / Трюки',
        'media' => 'Медиа',
        'videos' => 'Видео',
        'photos' => 'Фото',
        'tv' => 'Телевидение',
        'press' => 'Пресса',
        'radio' => 'Радио',
        'contacts' => 'Контакты',
        'scheme' => 'Схема зала',
        'applications' => 'Ссылки на моб.приложения',
        'dvd' => 'DVD',
        'home_eng' => 'Английская страница',
        'information' => 'Информация',
        'visit_rules' => 'Правила посещения',
        'ticket_order' => 'Заказ и доставка билетов',
        'reduced_payment' => 'Перечень льгот для отдельных категорий граждан',
        'disabled' => 'Для людей с ограниченными возможностями',
        'public_offer' => 'Публичная оферта',
        'personal_data' => 'Согласие на обработку персональных данных',
        'ticket_return' => 'Порядок возврата билетов',
        'ticket_return_rules' => 'Правила возврата билетов',
        'dynamic_pricing' => 'Динамическое ценообразование',
        'covid_free' => 'COVID-free мероприятия',
    ];
}
