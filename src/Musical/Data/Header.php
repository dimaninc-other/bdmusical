<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 12.05.2021
 * Time: 19:41
 */

namespace Musical\Data;

use diCore\Tool\SimpleContainer;

class Header extends SimpleContainer
{
    const standard = 1;
    const home = 2;

    public static $names = [
        self::standard => 'standard',
        self::home => 'home',
    ];

    public static $titles = [
        self::standard => 'Стандартный',
        self::home => 'Большой для главной',
    ];
}