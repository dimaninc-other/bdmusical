<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 28.02.2023
 * Time: 17:37
 */

namespace Musical\Data;

class Environment extends \diCore\Data\Environment
{
    const booking_vendor = null;
    const booking_test_mode = null;
    const booking_add_utm_suffix = null;

    final public static function getBookingVendor()
    {
        /** @var \Musical\Data\Environment $class */
        $class = self::getClass();

        return $class::booking_vendor;
    }

    final public static function getBookingTestMode()
    {
        /** @var \Musical\Data\Environment $class */
        $class = self::getClass();

        return $class::booking_test_mode;
    }

    final public static function getBookingAddUtmSuffix()
    {
        /** @var \Musical\Data\Environment $class */
        $class = self::getClass();

        return $class::booking_add_utm_suffix;
    }
}
