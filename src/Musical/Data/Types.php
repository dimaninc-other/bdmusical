<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 13.04.2021
 * Time: 18:31
 */

namespace Musical\Data;

class Types extends \diCore\Data\Types
{
    const solo = 1501;
    const character = 1502;
    const show = 1503;
    const slide = 1504;
    const slide_block = 1505;
    const price = 1506;
    const smi_tv = 1507;
    const smi_radio = 1508;
    const smi_press = 1509;
    const partner = 1510;
    const season = 1511;
    const client = 1512;
    const media = 1513;
    const ballet = 1514;
    const dancer = 1515;
    const creator = 1516;
    const musician = 1517;
    const review = 1518;
    const action = 1519;
    const creative = 1520;
    const skater = 1521;
    const show_solo_link = 1522;

    public static $tables = [
        self::solo => 'solo',
        self::character => 'character',
        self::show => 'show',
        self::slide => 'slide',
        self::slide_block => 'slide_block',
        self::price => 'price',
        self::smi_tv => 'smi_tv',
        self::smi_radio => 'smi_radio',
        self::smi_press => 'smi_press',
        self::partner => 'partner',
        self::season => 'season',
        self::client => 'client',
        self::media => 'media',
        self::ballet => 'ballet',
        self::dancer => 'dancer',
        self::creator => 'creator',
        self::musician => 'musician',
        self::review => 'review',
        self::action => 'action',
        self::creative => 'creative',
        self::skater => 'skater',
        self::show_solo_link => 'show_solo_link',
    ];

    public static $names = [
        self::solo => 'solo',
        self::character => 'character',
        self::show => 'show',
        self::slide => 'slide',
        self::slide_block => 'slide_block',
        self::price => 'price',
        self::smi_tv => 'smi_tv',
        self::smi_radio => 'smi_radio',
        self::smi_press => 'smi_press',
        self::partner => 'partner',
        self::season => 'season',
        self::client => 'client',
        self::media => 'media',
        self::ballet => 'ballet',
        self::dancer => 'dancer',
        self::creator => 'creator',
        self::musician => 'musician',
        self::review => 'review',
        self::action => 'action',
        self::creative => 'creative',
        self::skater => 'skater',
        self::show_solo_link => 'show_solo_link',
    ];

    public static $titles = [
        self::solo => 'Солист',
        self::character => 'Персонаж',
        self::show => 'Спектакль',
        self::slide => 'Слайд',
        self::slide_block => 'Блок слайдов',
        self::price => 'Цена',
        self::smi_tv => 'Телесюжет',
        self::smi_radio => 'Радиосюжет',
        self::smi_press => 'Пресса',
        self::partner => 'Партнер',
        self::season => 'Сезон',
        self::client => 'Клиент',
        self::media => 'Медиа (агрегатор)',
        self::ballet => 'Танцор балета',
        self::dancer => 'Член ансамбля',
        self::creator => 'Создатель',
        self::musician => 'Музыкант оркестра',
        self::review => 'Отзыв',
        self::action => 'Акция',
        self::creative => 'Творческая группа',
        self::skater => 'Фигурист',
        self::show_solo_link => 'Связь спектакль-солист',
    ];
}
