<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 20.05.2021
 * Time: 16:05
 */

namespace Musical\Data;

class Config extends \diCore\Data\Config
{
    const vendor_dilyaver = 1;
    const vendor_profticket = 2;

    const show = null;
    const booking_vendor = self::vendor_dilyaver;
    const booking_test_mode = false;
    const booking_add_utm_suffix = true;

    final public static function getShow()
    {
        /** @var self $class */
        $class = self::getClass();

        return $class::show;
    }

    final public static function getBookingVendor()
    {
        /** @var self $class */
        $class = self::getClass();

        return Environment::getBookingVendor() ?? $class::booking_vendor;
    }

    final public static function getBookingTestMode()
    {
        /** @var self $class */
        $class = self::getClass();

        return Environment::getBookingTestMode() ?? $class::booking_test_mode;
    }

    final public static function getBookingAddUtmSuffix()
    {
        /** @var self $class */
        $class = self::getClass();

        return Environment::getBookingAddUtmSuffix() ?? $class::booking_add_utm_suffix;
    }
}
