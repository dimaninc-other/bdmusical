<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 20.05.2021
 * Time: 15:54
 */

namespace Musical\Data;

use diCore\Tool\Localization;
use diCore\Tool\SimpleContainer;

class SiblingShow extends SimpleContainer
{
    const monte = 1;
    const orlov = 2;
    const karenina = 3;

    public static $names = [
        self::monte => 'monte',
        self::orlov => 'orlov',
        self::karenina => 'karenina',
    ];

    public static $titles = [
        self::monte => 'Монте-Кристо',
        self::orlov => 'Граф Орлов',
        self::karenina => 'Анна Каренина',
    ];

    public static $engTitles = [
        self::monte => 'Monte Cristo',
        self::orlov => 'Count Orlov',
        self::karenina => 'Anna Karenina',
    ];

    public static $websites = [
        self::monte => 'https://montecristo-musical.ru/',
        self::orlov => 'https://orlov-musical.ru/',
        self::karenina => 'https://karenina-musical.ru/',
    ];

    public static $engWebsites = [
        self::monte => 'https://montecristo-musical.ru/en/',
        self::orlov => 'https://orlov-musical.ru/en/',
        self::karenina => 'https://karenina-musical.ru/en/',
    ];

    public static function engTitle($id)
    {
        return static::$engTitles[$id] ?? null;
    }

    public static function website($id)
    {
        return static::$websites[$id] ?? null;
    }

    public static function engWebsite($id)
    {
        return static::$engWebsites[$id] ?? null;
    }

    public static function cutWebsite($www)
    {
        $parts = parse_url($www);

        return preg_replace('#^www\.#', '', $parts['host']);
    }

    public static function getOtherCollection($lang)
    {
        $ar = [];

        foreach (static::$names as $id => $name) {
            if ($id == Config::getShow()) {
                continue;
            }

            $ar[] = [
                'id' => $id,
                'name' => static::name($id),
                'title' => Localization::get('sibling.' . $name . '.title') ?:
                    ($lang === 'ru' ? static::title($id) : static::engTitle($id)),
                'intro' => Localization::get('sibling.' . $name . '.intro'),
                'description' => Localization::get('sibling.' . $name . '.description'),
                'website' => $lang === 'ru' ? static::website($id) : static::engWebsite($id),
                'website_cut' => static::cutWebsite($lang === 'ru' ? static::website($id) : static::engWebsite($id)),
            ];
        }

        return $ar;
    }
}
