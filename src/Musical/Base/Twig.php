<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 06.05.2021
 * Time: 16:30
 */

namespace Musical\Base;

use diCore\Tool\Localization;
use Musical\Entity\Action\Collection as Actions;
use Musical\Entity\Action\Model;
use Twig\TwigFilter;

class Twig extends \diTwig
{
    public function __construct($options = [])
    {
        parent::__construct($options);

        $cleanPhone = new TwigFilter('clean_phone', ['diCore\Helper\Phone', 'clean']);
        $this->getEngine()->addFilter($cleanPhone);

        $actions = Actions::create();
        $actionByPurpose = [];
        /** @var Model $action */
        foreach ($actions as $action) {
            $actionByPurpose[$action->getPurpose()] = $action;
        }

        $this->assign([
            'local' => Localization::get(),
            'action_by_purpose' => $actionByPurpose,
        ]);
    }
}
