<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 16.04.2021
 * Time: 17:39
 */

namespace Musical\Base;

use diCore\Data\Configuration;
use Musical\Data\Header;
use Musical\Data\SiblingShow;
use Musical\Entity\Content\Model;
use Musical\Entity\Slide\Collection as Slides;

class CMS extends \diCore\Base\CMS
{
    const MAIN_TEMPLATE_ENGINE = self::TEMPLATE_ENGINE_TWIG;
    const USE_TO_SHOW_CONTENT = true;
    const SENTRY_DSN = '';

    protected $headerType = Header::standard;

    protected $bodyClasses = [];

    protected $bottomScript = [];
    protected $bottomCode = [];

    static public $possibleLanguages = [
        'ru',
        'en',
    ];

    public static $defaultLanguage = 'ru';

    protected static $customSkipGetParams = [
        'placement',
        'rb_clickid',
        'etext',
    ];

    public function go()
    {
        $this
            ->saveReferer()
            ->start()
            ->load_content_rec()
            ->assignHtmlHead()
            ->assign_ct_ar()
            ->initBreadCrumbs()
            ->renderPage()
            ->assign_top_language_links();
    }

    public static function fastInitiate($lang)
    {
        global $Z;

        $Z = new CMS();
        $Z
            ->load_content_table_cache()
            ->setLanguage($lang)
            ->define_language($lang);

        return $Z;
    }

    protected function renderAfterError()
    {
        $this
            ->assignHtmlHead()
            ->assign_ct_ar()
            ->assign_top_language_links()
            ->initTplDefines();

        return $this;
    }

    protected function getTwigBasicsData()
    {
        return extend(parent::getTwigBasicsData(), [
            'main_menu' => array_filter($this->tables['content'], function (Model $m) {
                return $m->getLevelNum() == 0 &&  $m->localizedVisible();
            }),
            'sub_menu' => $this->getSubMenuItems(),
            'top_slides' => $this->getTopSlides(),
            'sibling_shows' => SiblingShow::getOtherCollection($this->getLanguage()),
            'phone' => [
                'main' => Configuration::get('phone_main'),
            ],
            'socials' => [
                'facebook' => Configuration::get('socials[facebook]'),
                'instagram' => Configuration::get('socials[instagram]'),
                'youtube' => Configuration::get('socials[youtube]'),
                'vk' => Configuration::get('socials[vk]'),
                // 'tiktok' => Configuration::get('socials[tiktok]'),
                'telegram' => Configuration::get('socials[telegram]'),
            ],
            'counters' => [
                'head' => [
                    'bottom' => Configuration::get('counters[head_bottom]'),
                ],
                'body' => [
                    'top' => Configuration::get('counters[body_top]'),
                    'bottom' => Configuration::get('counters[body_bottom]'),
                ],
                'uid' => [
                    'metrika' => Configuration::get('counters[metrika_uid]'),
                    'sentry_dsn' => static::SENTRY_DSN,
                ],
            ],
        ]);
    }

    protected function getTopSlides()
    {
        return Slides::create()
            ->filterByBlockId(1)
            ->filterByVisible(1)
            ->orderByOrderNum();
    }

    public static function hasUtm()
    {
        foreach ($_GET as $k => $v) {
            if (strtolower(substr($k, 0, 4)) === 'utm_') {
                return true;
            }
        }

        return false;
    }

    public function isEng()
    {
        return $this->getLanguage() === 'en';
    }

    public function isRus()
    {
        return !$this->isEng();
    }

    protected function saveReferer()
    {
        if (!self::hasUtm()) {
            if (!\diSession::get('_referer') && !empty($_SERVER['HTTP_REFERER'])) {
                \diSession::set('_referer', $_SERVER['HTTP_REFERER']);
            }
        }

        return $this;
    }

    public function addBodyClass($class)
    {
        if (!is_array($class)) {
            $class = [$class];
        }

        $this->bodyClasses = array_unique(array_merge($this->bodyClasses, $class));

        return $this;
    }

    public function getBodyClasses()
    {
        return $this->bodyClasses;
    }

    protected function noIndexNeeded()
    {
        return self::getEnvironment() != self::ENV_PROD;
    }

    protected function shareBlockNeeded()
    {
        return false;
    }

    protected function sentryNeeded()
    {
        return !!static::SENTRY_DSN && $this->countersNeeded();
    }

    protected function getNeededSwitches()
    {
        return extend(parent::getNeededSwitches(), [
            'noindex' => $this->noIndexNeeded(),
            'sentry' => $this->sentryNeeded(),
        ]);
    }

    public function setHeaderType($type)
    {
        $this->headerType = $type;

        return $this;
    }

    public function getHeaderTypeName()
    {
        return Header::name($this->headerType);
    }

    public function isHomeHeader()
    {
        return $this->headerType == Header::home;
    }

    public function isTopSubMenuNeeded()
    {
        return $this->getContentModel()->getType() !== 'home';
    }

    protected function getSubMenuItems()
    {
        $ar = [];
        $parents = [
            $this->getContentModel()->getId(),
            $this->getContentModel()->getParent(),
            $this->getContentFamily()->getMemberByLevel(0)->getId(),
        ];

        /**
         * @var int $subId
         * @var Model $subM
         */
        foreach ($this->tables['content'] as $subId => $subM) {
            if (
                $subM->getParent() > 0 &&
                $subM->getLevelNum() == 1 &&
                $subM->localizedVisible() &&
                in_array($subM->getParent(), $parents)
            ) {
                $ar[] = $subM;
            }
        }

        return $ar;
    }

    public function addBottomScript($line)
    {
        $this->bottomScript[] = $line;

        return $this;
    }

    public function hasBottomScript()
    {
        return count($this->bottomScript) > 0;
    }

    public function getBottomScript()
    {
        return join("\n", $this->bottomScript);
    }

    public function addBottomCode($line)
    {
        $this->bottomCode[] = $line;

        return $this;
    }

    public function hasBottomCode()
    {
        return count($this->bottomCode) > 0;
    }

    public function getBottomCode()
    {
        return join("\n", $this->bottomCode);
    }
}
