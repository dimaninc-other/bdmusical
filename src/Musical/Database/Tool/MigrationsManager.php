<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 09.06.2021
 * Time: 18:45
 */

namespace Musical\Database\Tool;

class MigrationsManager extends \diCore\Database\Tool\MigrationsManager
{
    const FOLDER_MUSICAL_MIGRATIONS = 11;

    public static $customFoldersIdsAr = [
        self::FOLDER_MUSICAL_MIGRATIONS,
    ];

    public static function getFolderById($id)
    {
        if ($id == self::FOLDER_MUSICAL_MIGRATIONS) {
            return static::getMusicalMigrationsFolder();
        }

        return parent::getFolderById($id);
    }

    public static function getMusicalMigrationsFolder()
    {
        return dirname(__FILE__, 5) . '/migrations/';
    }
}