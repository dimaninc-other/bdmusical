<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 11.05.21
 * Time: 15:21
 */

namespace Musical\Tool\Calendar;

class Date
{
	protected $d;
	protected $m;
	protected $y;

	public function __construct($d = null, $m = null, $y = null)
	{
		if ($m === null && $y === null) {
			$this->setDate($d);
		} else {
			$this->d = $d;
			$this->m = $m;
			$this->y = $y;
		}
	}

	public function incDay()
	{
		if (++$this->d > \diDateTime::getDaysInMonth($this->month(), $this->year())) {
			$this
				->incMonth()
				->setDay(1);
		}

		return $this;
	}

	public function decDay()
	{
		if (--$this->d < 1) {
			$this
				->decMonth()
				->setDay(\diDateTime::getDaysInMonth($this->month(), $this->year()));
		}

		return $this;
	}

	public function incMonth()
	{
		if (++$this->m > 12) {
			$this
				->setMonth(1)
				->setYear($this->year() + 1);
		}

		return $this;
	}

	public function decMonth()
	{
		if (--$this->m < 1) {
			$this
				->setMonth(12)
				->setYear($this->year() - 1);
		}

		return $this;
	}

	public function day($lead0 = false)
	{
		return $lead0 ? lead0($this->d) : $this->d;
	}

	public function month($lead0 = false)
	{
		return $lead0 ? lead0($this->m) : $this->m;
	}

	public function year($lead0 = false)
	{
		return $lead0 ? lead0($this->y) : $this->y;
	}

	public function weekDay()
	{
		return \diDateTime::weekDay($this->__toString());
	}

	public function setDay($d)
	{
		$this->d = (int)$d;

		return $this;
	}

	public function setMonth($m)
	{
		$this->m = (int)$m;

		return $this;
	}

	public function setYear($y)
	{
		$this->y = (int)$y;

		return $this;
	}

	public function setDate($date)
	{
		list($this->d, $this->m, $this->y) = explode('-', \diDateTime::format('d-m-Y', $date));

		$this->d = (int)$this->d;
		$this->m = (int)$this->m;
		$this->y = (int)$this->y;

		return $this;
	}

	public function __toString()
	{
		return sprintf('%s.%s.%s', $this->day(true), $this->month(true), $this->year(true));
	}

	public function mysqlString()
	{
		return sprintf('%s-%s-%s', $this->year(true), $this->month(true), $this->day(true));
	}

	public function urlString()
	{
		return sprintf('%s/%s/%s/', $this->year(true), $this->month(true), $this->day(true));
	}
}
