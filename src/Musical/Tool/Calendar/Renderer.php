<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 11.05.2021
 * Time: 16:00
 */

namespace Musical\Tool\Calendar;

use diCore\Base\CMS;
use diCore\Database\Connection;
use Musical\Data\Config;
use Musical\Entity\Show\Collection as Shows;
use Musical\Entity\Show\Model as Show;
use Musical\Helper\Util;
use Musical\Controller\Booking;

class Renderer
{
    /**
     * @var CMS
     */
    protected $Z;

    /**
     * @var string
     */
    protected $lang;

    /**
     * Selected date in calendar (default: 1st of current month)
     * @var Date
     */
    private $selected;

    /** @var Date */
    private $selectedOrig;

    /** @var Date */
    private $worker;

    /** @var callable */
    private $todayChecker;

    /** @var array */
    private $showsByDate = [];

    /**
     * Array month => state, True if at least 1 moved show
     * @var array
     */
    private $movedExist = [];

    /**
     * @var string
     */
    private $calendarMode = 'tickets_online';

    public function __construct(CMS $Z = null)
    {
        $this->cacheShows();

        $this->Z = $Z;
        $this->lang = $Z ? $Z->getLanguage() : 'ru';

        $this->todayChecker = function (Date $worker) {
            return $worker->day(true) . $worker->month(true) . $worker->year(true)
                == date('dmY');
        };

        $this->calculateSelectedDay();
    }

    protected function route($idx = null)
    {
        return $this->Z ? $this->Z->getRoute($idx) : null;
    }

    public function calculateSelectedDay()
    {
        $nextShow = Show::createNextNearest();

        $defaultDay = 1; //\diDateTime::format('d', $nextShow->getDate());

        $this->selected = new Date(
            (int)($this->route(3) ?: $defaultDay),
            (int)($this->route(2) ?: \diDateTime::format('m', $nextShow->getDate())),
            (int)($this->route(1) ?: \diDateTime::format('Y', $nextShow->getDate()))
        );

        $this->selectedOrig = clone $this->selected;

        return $this;
    }

    /**
     * @param string $calendarMode
     */
    public function setCalendarMode($calendarMode)
    {
        $this->calendarMode = $calendarMode;

        foreach ($this->showsByDate as $ar) {
            /** @var Show $show */
            foreach ($ar as $show) {
                $show->setCalendarMode($this->getCalendarMode());
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getCalendarMode()
    {
        return $this->calendarMode;
    }

    public function getData()
    {
        $widgetUriEnding = '';
        /* not needed any more
        if (!\diRequest::get('utm_source') && \diSession::get('_referer')) {
            $widgetUriEnding = '&utm_source=' . urlencode(Util::stripUrlPath(\diSession::get('_referer')));
        }
        $widgetUriEnding .= '&referer_url=' . urlencode(\diRequest::referrer());
        */

        $ending = Config::getBookingAddUtmSuffix()
            ? '&' . \diRequest::requestQueryString() . $widgetUriEnding
            : '';

        return [
            'R' => $this,
            'weekdays' => $this->getWeekDaysData(),
            'short_weekdays' => $this->getShortWeekDaysData(),
            'upcoming_months' => static::getMonthsWithUpcomingShows(),
            'widget_uri_base' => Booking::getWidgetUri($this->lang) . $ending,
        ];
    }

    protected function isRus()
    {
        return $this->lang === 'ru';
    }

    protected function getMonthYearFormat()
    {
        return $this->isRus()
            ? '%месяц% Y'
            : 'F Y';
    }

    protected function getWeekDaysData()
    {
        return $this->isRus()
            ? \diDateTime::$weekDays
            : \diDateTime::$engWeekDays;
    }

    protected function getShortWeekDaysData()
    {
        return $this->isRus()
            ? \diDateTime::$weekDaysShort
            : \diDateTime::$engWeekDaysShort;
    }

    /**
     * @param Date $selected
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;

        return $this;
    }

    /**
     * @return Date
     */
    public function getSelected()
    {
        return $this->selected;
    }

    /**
     * @return Date
     */
    public function getSelectedOrig()
    {
        return $this->selectedOrig;
    }

    /**
     * @param callable $todayChecker
     */
    public function setTodayChecker($todayChecker)
    {
        $this->todayChecker = $todayChecker;

        return $this;
    }

    public function isToday()
    {
        $checker = $this->todayChecker;

        return $checker($this->worker);
    }

    public function reInitWorker($m = null, $y = null)
    {
        if ($m && $y) {
            $this->selected
                ->setMonth($m)
                ->setYear($y);
        }

        $start = \diDateTime::timestamp(
            '01.' .
            $this->selected->month(true) . '.' .
            $this->selected->year(true)
        );

        $this->worker = new Date(
            strtotime('last monday', $start)
            /*
            $this->selected->weekDay() == \diDateTime::MONDAY
                ? $this->selected->__toString()
                : strtotime('last monday', \diDateTime::timestamp($this->selected))
            */
        );

        return $this->worker;
    }

    public function getWorker()
    {
        return $this->worker;
    }

    protected function getMonthsWithUpcomingShows()
    {
        $months = [];

        $db = Connection::get()->getDb();
        $table = Show::table;

        $rs = $db->q("SELECT MONTH(date) AS month,YEAR(date) AS year
            FROM `$table`
            WHERE `visible` = '1' and `date` >= CURDATE()
            GROUP BY MONTH(`date`), YEAR(`date`)
            ORDER BY YEAR(`date`), MONTH(`date`)"
        );

        if ($db->count($rs)) {
            while ($r = $db->fetch_array($rs)) {
                $months[] = $r;
            }
        } else {
            $months[] = [
                'month' => (int)date('m'),
                'year' => (int)date('Y'),
            ];
        }

        foreach ($months as &$m) {
            $m['idx'] = $m['year'] . '-' . lead0($m['month']);
            $m['month_url_suffix'] = $m['year'] . '/' . lead0($m['month']) . '/';
            $m['title'] = \diDateTime::format(
                $this->getMonthYearFormat(),
                '01.' . lead0($m['month']) . '.' . $m['year']
            );
        }

        return $months;
    }

    protected function cacheShows()
    {
        $shows = Shows::createReadOnly()
            ->filterByVisible(1)
            ->filterByExpression('date', '>', 'NOW()')
            ->orderByDate();

        /** @var Show $show */
        foreach ($shows as $show) {
            $date = \diDateTime::simpleDateFormat($show->getDate());
            $time = \diDateTime::simpleTimeFormat($show->getDate());

            if (!isset($this->showsByDate[$date])) {
                $this->showsByDate[$date] = [];
            }

            $this->showsByDate[$date][$time] = $show;
        }

        return $this;
    }

    public function getShowsForWorker()
    {
        return $this->showsByDate[$this->getWorker()->__toString()] ?? [];
    }

    public function setWorkerMoved()
    {
        $this->movedExist[$this->getWorker()->month(true) . '-' . $this->getWorker()->year()] = true;

        return $this;
    }

    public function hasMoved($year, $month)
    {
        return !empty($this->movedExist[lead0($month) . '-' . $year]);
    }
}
