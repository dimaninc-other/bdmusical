<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 28.05.2021
 * Time: 18:21
 */

namespace Musical\Module;

use Musical\Data\Types;

class CreativeTeam extends BaseTroupe
{
    const DATA_TYPE = Types::creative;
    const FOLDER = 'base_troupe';
}