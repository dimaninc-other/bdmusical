<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 28.05.2021
 * Time: 17:49
 */

namespace Musical\Module;

use Musical\Data\Types;

class Creators extends BaseTroupe
{
    const DATA_TYPE = Types::creator;
    const FOLDER = 'creators';
}