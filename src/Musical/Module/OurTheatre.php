<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 29.05.2021
 * Time: 12:12
 */

namespace Musical\Module;

class OurTheatre extends \diModule
{
    public function render()
    {
        if ($this->getRoute(1)) {
            $this->getZ()->errorNotFound();
        }

        $this->getTwig()->renderPage('our_theatre/page', [
        ]);
    }
}