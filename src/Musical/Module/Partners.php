<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 29.05.2021
 * Time: 11:33
 */

namespace Musical\Module;

use Musical\Entity\Partner\Category;
use Musical\Entity\Partner\Collection;

class Partners extends \diModule
{
    public function render()
    {
        if ($this->getRoute(1)) {
            $this->getZ()->errorNotFound();
        }

        $this->getTwig()->renderPage('partners/page', [
            'categories' => Category::getLocalizedTitles(),
            'partners' => static::getCol(),
        ]);
    }

    public static function getCol()
    {
        return Collection::createReadOnly()
            ->filterByLocalizedVisible(1)
            ->orderByOrderNum();
    }
}