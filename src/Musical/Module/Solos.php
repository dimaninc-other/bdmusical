<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 12.05.2021
 * Time: 19:35
 */

namespace Musical\Module;

use diCore\Tool\CollectionCache;
use Musical\Entity\Character\Collection as Characters;
use Musical\Entity\Solo\Collection as SolosCol;

class Solos extends \diModule
{
    public function render()
    {
        if ($this->getRoute(1)) {
            $this->getZ()->errorNotFound();
        }

        $this->getTwig()
            ->renderPage('solos/page', [
                'characters' => static::getCharacters(),
            ]);
    }

    public static function getCharacters()
    {
        if (!CollectionCache::exists(SolosCol::type)) {
            CollectionCache::add([
                SolosCol::create()->orderByOrderNum(),
            ]);
        }

        return Characters::createReadOnly()
            ->filterByVisible(1)
            ->orderByOrderNum();
    }
}
