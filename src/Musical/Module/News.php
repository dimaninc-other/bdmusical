<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 14.05.2021
 * Time: 09:36
 */

namespace Musical\Module;

use diCore\Data\Configuration;
use Musical\Data\Types;
use Musical\Entity\News\Collection;
use Musical\Entity\News\Model;

class News extends \diModule
{
    const TEMPLATE_FOLDER = 'news';
    const ENTITY_TYPE = Types::news;

    /** @var Model */
    protected $model;

    public function render()
    {
        if ($this->getRoute(2)) {
            $this->getZ()
                ->errorNotFound();
        }

        if ($this->getSingle()->exists()) {
            $this->renderSingle();
        } else {
            $this->renderList();
        }
    }

    protected function renderList()
    {
        $this->getTwig()
            ->renderPage(static::TEMPLATE_FOLDER . '/list', [
                'news' => static::getCol(),
                'col_left' => static::colLeft(),
                'entity_type_name' => Types::getName(static::ENTITY_TYPE),
            ]);

        return $this;
    }

    protected function renderSingle()
    {
        $this->getTwig()
            ->renderPage(static::TEMPLATE_FOLDER . '/single', [
                'n' => $this->getSingle(),
                'news' => static::getCol()->filterById($this->getSingle()->getId(), '!='),
                'entity_type_name' => Types::getName(static::ENTITY_TYPE),
            ]);

        $this->getZ()
            ->assignMeta($this->getSingle());

        return $this;
    }

    protected function getSingle()
    {
        if (!$this->model) {
            $this->model = \diModel::create(static::ENTITY_TYPE, $this->getRoute(1), 'slug');

            if ($this->getRoute(1) && !$this->model->exists()) {
                $this->getZ()
                    ->errorNotFound();
            }
        }

        return $this->model;
    }

    public static function pageSize()
    {
        return Configuration::get('per_page[news]');
    }

    public static function getCol()
    {
        $pageSize = static::pageSize();

        /** @var Collection $col */
        $col = \diCollection::createReadOnly(static::ENTITY_TYPE);
        $col->filterByLocalizedVisible(1);
        $col = static::filterCol($col);
        $col = static::sortCol($col);

        if ($pageSize) {
            $col->setPageSize((int)$pageSize);
        }

        return $col;
    }

    public static function colLeft()
    {
        $pageSize = static::pageSize();

        /** @var Collection $col */
        $col = \diCollection::create(static::ENTITY_TYPE);
        $col->filterByLocalizedVisible(1);

        if ($pageSize) {
            $col
                ->setSkip($pageSize)
                ->setPageSize((int)$pageSize)
                ->setPageNumber(2);

            return $col->count() > 0;
        }

        return false;
    }

    /**
     * @param Collection $col
     */
    protected static function filterCol($col)
    {
        return $col->filterByDate(\diDateTime::sqlFormat(), '<=');
    }

    /**
     * @param Collection $col
     */
    protected static function sortCol($col)
    {
        return $col->orderByDate('DESC');
    }
}
