<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 08.05.2021
 * Time: 09:49
 */

namespace Musical\Module;

use Musical\Data\Header;
use Musical\Data\Video\Purpose as VideoPurpose;
use Musical\Entity\Album\Collection as Albums;
use Musical\Entity\Album\Purpose;
use Musical\Entity\Photo\Collection as Photos;
use Musical\Entity\Review\Collection as Reviews;
use Musical\Entity\Review\Model as Review;
use Musical\Entity\Video\Collection as Videos;
use Musical\Tool\Calendar\Renderer;

class Home extends \diModule
{
    public function render()
    {
        if ($this->getRoute(1)) {
            $this->getZ()->errorNotFound();
        }

        $CR = new Renderer($this->getZ());

        $characters = Solos::getCharacters();

        $this->getTwig()
            ->renderPage('home/page', [
                'trailer_video' => static::getTrailerVideo(),
                'promo_photos' => static::getPromoPhotos(),
                'calendar' => $CR->getData(),
                'characters' => $characters,
                'characters2' => clone $characters,
                'reviews' => static::getReviews(),
                'news' => News::getCol()->filterByLocalizedTop(1)->setPageSize(4),
                'media' => Media::getCol()->setPageSize(9),
            ]);

        $this->getZ()
            ->setHeaderType(Header::home);
    }

    public static function getTrailerVideo()
    {
        $trailer = Videos::create()
            ->filterByPurpose(\diModel::normalizeLang() === 'ru'
                ? VideoPurpose::HOME_TRAILER
                : VideoPurpose::HOME_ENG_TRAILER)
            ->filterByLocalizedVisible(1)
            ->orderByOrderNum()
            ->getFirstItem();

        if (!$trailer->exists()) {
            $trailer = Videos::create()
                ->filterByPurpose(VideoPurpose::HOME_TRAILER)
                ->filterByLocalizedVisible(1)
                ->orderByOrderNum()
                ->getFirstItem();
        }

        return $trailer;
    }

    public static function getPromoPhotos()
    {
        $album = Albums::create()
            ->filterByPurpose(Purpose::promo_about)
            ->getFirstItem();

        if ($album->exists()) {
            $photos = Photos::createReadOnly()
                ->filterByAlbumId($album->getId())
                ->filterByVisible(1)
                ->orderByOrderNum();

            return $photos;
        }

        return [];
    }

    public static function getReviews()
    {
        return Reviews::createReadOnly()
            ->filterByLocalizedVisible(1)
            ->filterByType(Review::TYPE_CELEBRITY)
            ->randomOrder()
            ->setPageSize(4);
    }
}
