<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 25.05.2021
 * Time: 19:47
 */

namespace Musical\Module;

use diCore\Base\CMS;
use diCore\Tool\CollectionCache;
use Musical\Entity\Show\Collection as Shows;
use Musical\Entity\Show\Model as Show;
use Musical\Entity\ShowSoloLink\Collection as Links;
use Musical\Entity\ShowSoloLink\Model as Link;
use Musical\Entity\Solo\Collection as SolosCol;
use Musical\Tool\Calendar\Date;
use Musical\Tool\Calendar\Renderer;

class Staff extends \diModule
{
    const CALENDAR_MODE = 'staff';

    /** @var Shows */
    protected $todayShows;

    /**
     * @var Date
     */
    protected $nextMonth;

    /**
     * @var Date
     */
    protected $prevMonth;

    /**
     * @var Renderer
     */
    protected $CR;

    public function __construct(CMS $Z)
    {
        parent::__construct($Z);

        $this->CR = new Renderer($this->getZ());
        $this->CR
            ->setCalendarMode(static::CALENDAR_MODE);

        $this->todayShows = Shows::create()
            ->filterByVisible(1)
            ->filterByDay($this->getSelected());

        $this->nextMonth = clone $this->getSelected();
        $this->nextMonth->incMonth();

        $nextMonthShow = Show::createNearestByMonth($this->nextMonth, 1);

        if ($nextMonthShow->exists()) {
            $this->nextMonth->setDay(\diDateTime::format('d', $nextMonthShow->getDate()));
        } else {
            $this->nextMonth = null;
        }

        $this->prevMonth = clone $this->getSelected();
        $this->prevMonth->decMonth();

        $prevMonthShow = Show::createNearestByMonth($this->prevMonth, -1);

        if ($prevMonthShow->exists()) {
            $this->prevMonth->setDay(\diDateTime::format('d', $prevMonthShow->getDate()));
        } else {
            $this->prevMonth = null;
        }
    }

    public function render()
    {
        $characters = Solos::getCharacters();
        $solos = CollectionCache::get(SolosCol::type);

        $staff = [];

        foreach ($this->todayShows as $show) {
            $staff[$show->getId()] = [];

            $links = Links::create()
                ->filterByShowId($show->getId());
            /** @var Link $link */
            foreach ($links as $link) {
                $staff[$show->getId()][$solos[$link->getSoloId()]->getCharacterId()] = $solos[$link->getSoloId()];
            }
        }

        $this->getTwig()->renderPage('staff/page', [
            'calendar' => $this->CR->getData(),
            'characters' => $characters,
            'shows' => $this->todayShows,
            'solos' => $solos,
            'staff' => $staff,

            'this_month_digits' => $this->getSelected()->month(true) . '-' . $this->getSelected()->year(),

            'prev_month' => $this->prevMonth,
            'next_month' => $this->nextMonth,
        ]);
    }

    protected function getSelected()
    {
        return $this->CR->getSelected();
    }
}
