<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 06.06.2021
 * Time: 12:16
 */

namespace Musical\Module;

use Musical\Entity\Content\Collection;

class Information extends \diModule
{
    public function render()
    {
        $this->getTwig()->renderPage('information/page', [
            'sub_pages' => $this->getSubPages(),
        ]);
    }

    protected function getSubPages()
    {
        /** @var Collection $pages */
        $pages = Collection::create()
            ->filterByLocalizedVisible(1)
            ->filterByParent($this->getZ()->getContentModel()->getId())
            ->orderByOrderNum();

        return $pages;
    }
}