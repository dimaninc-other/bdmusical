<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 26.05.2021
 * Time: 11:06
 */

namespace Musical\Module;

use Musical\Data\Config;

class TicketsOnline extends Staff
{
    const CALENDAR_MODE = 'tickets_online';

    /** @var bool|callable */
    private $dayLink = false; // set link on date, not on times

    public function render()
    {
        $this->getTwig()->renderPage('tickets_online/page', [
            'actions' => Actions::getCol(),
            'calendar' => $this->CR->getData(),
            'widget_settings' => [
                'is_inline' => Config::getBookingVendor() === Config::vendor_dilyaver,
                'is_popup' => Config::getBookingVendor() === Config::vendor_profticket,
                'vendor' => Config::getBookingVendor(),
            ],
        ]);
    }
}
