<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 28.05.2021
 * Time: 17:14
 */

namespace Musical\Module;

class Contacts  extends \diModule
{
    public function render()
    {
        if ($this->getRoute(1)) {
            $this->getZ()->errorNotFound();
        }

        $this->getTwig()->renderPage('contacts/page', [
        ]);
    }
}