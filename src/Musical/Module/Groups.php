<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 28.05.2021
 * Time: 15:33
 */

namespace Musical\Module;

use diCore\Data\Configuration;
use Musical\Entity\Album\Collection;
use Musical\Entity\Album\Purpose;
use Musical\Entity\Photo\Collection as Photos;
use Musical\Helper\FileSizeHelper;

class Groups extends \diModule
{
    public function render()
    {
        if ($this->getRoute(1)) {
            $this->getZ()->errorNotFound();
        }

        $prefix = $this->getZ()->isEng() ? 'en_' : '';
        $fn = Configuration::getFilename($prefix . 'groups_presentation');
        $size = is_file(\diPaths::fileSystem() . $fn)
            ? @filesize(\diPaths::fileSystem() . $fn)
            : 0;

        $this->getTwig()
            ->renderPage('groups/page', [
                'groups_presentation_show' => Configuration::get('groups_presentation_show'),
                'groups_presentation_fn' => $fn,
                'groups_presentation_size' => FileSizeHelper::toStr($size, $this->getZ()->getLanguage()),
                'photos' => static::getPhotos(),
            ]);
    }

    public static function getPhotos()
    {
        $album = Collection::create()
            ->filterByPurpose(Purpose::groups)
            ->getFirstItem();

        if ($album->exists()) {
            $photos = Photos::create()
                ->filterByLocalizedVisible(1)
                ->filterByAlbumId($album->getId())
                ->orderByOrderNum();

            return $photos;
        }

        return [];
    }
}
