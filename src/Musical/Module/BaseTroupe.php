<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 14.05.2021
 * Time: 12:14
 */

namespace Musical\Module;

use Musical\Entity\Ballet\Collection;

class BaseTroupe extends \diModule
{
    const DATA_TYPE = null;
    const FOLDER = 'troupe';

    public function render()
    {
        if ($this->getRoute(1)) {
            $this->getZ()->errorNotFound();
        }

        $this->getTwig()
            ->renderPage(static::FOLDER . '/page', [
                'col' => static::getCol(),
                'troupe_wrapper_extra_class' => $this->getTroupeWrapperExtraClass(),
            ]);
    }

    protected function getTroupeWrapperExtraClass()
    {
        return '';
    }

    public static function getCol()
    {
        return Collection::createReadOnly(static::DATA_TYPE)
            ->filterByLocalizedVisible(1)
            ->orderByOrderNum();
    }
}
