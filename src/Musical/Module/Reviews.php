<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 13.05.2021
 * Time: 19:11
 */

namespace Musical\Module;

use Musical\Entity\Review\Collection;

class Reviews extends \diModule
{
    public function render()
    {
        if ($this->getRoute(1)) {
            $this->getZ()->errorNotFound();
        }

        $this->getTwig()
            ->renderPage('reviews/page', [
                'reviews' => static::getCol(),
            ]);
    }

    public static function getCol()
    {
        return Collection::createReadOnly()
            ->filterByLocalizedVisible(1)
            ->randomOrder();
    }
}