<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 14.05.2021
 * Time: 12:12
 */

namespace Musical\Module;

use Musical\Data\Types;

class Ballets extends BaseTroupe
{
    const DATA_TYPE = Types::ballet;
}