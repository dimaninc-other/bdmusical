<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 28.05.2021
 * Time: 12:33
 */

namespace Musical\Module;

use Musical\Data\Types;
use Musical\Entity\Action\Collection;

class Actions extends News
{
    const ENTITY_TYPE = Types::action;

    public static function pageSize()
    {
        return null;
    }

    /**
     * @param Collection $col
     */
    protected static function filterCol($col)
    {
        return $col;
    }

    /**
     * @param Collection $col
     */
    protected static function sortCol($col)
    {
        return $col->orderByCreatedAt('DESC');
    }
}
