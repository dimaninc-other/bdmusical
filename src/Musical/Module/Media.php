<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 20.05.2021
 * Time: 17:34
 */

namespace Musical\Module;

use diCore\Data\Configuration;
use diCore\Helper\ArrayHelper;
use Musical\Data\Types;
use Musical\Entity\Media\Collection;
use Musical\Entity\Media\Model;
use Musical\Entity\Photo\Collection as Photos;

class Media extends \diModule
{
    const USE_LAZY_LOAD = false;

    /** @var \diModel */
    protected $media;

    public function render()
    {
        if ($this->getRoute(3)) {
            $this->getZ()->errorNotFound();
        }

        if ($this->getSingle()->exists()) {
            $this->renderSingle();
        } else {
            $this->renderList();
        }
    }

    public static function getCol()
    {
        return Collection::createReadOnly()
            ->filterByLocalizedVisible(1)
            ->orderByDate('DESC');
    }

    public static function colLeft()
    {
        if (!static::USE_LAZY_LOAD) {
            return false;
        }

        return static::getCol()
            ->setSkip(static::pageSize())
            ->getFirstItem()
            ->exists();
    }

    protected function getSingle()
    {
        if (!$this->media) {
            $mediaType = $this->getRoute(1);
            $mediaSlug = $this->getRoute(2);

            try {
                $this->media = \diModel::create($mediaType, $mediaSlug, 'slug');

                if ($this->getRoute(2) && !$this->media->exists()) {
                    throw new \Exception('Not found');
                }
            } catch (\Exception $e) {
                $this->getZ()
                    ->errorNotFound();
            }
        }

        return $this->media;
    }

    protected function getMediaTypes()
    {
        return $this->getZ()->isRus()
            ? Model::$targetTypes
            : Model::$enTargetTypes;
    }

    public static function pageSize()
    {
        return Configuration::get('per_page[media]');
    }

    protected function renderList()
    {
        $mediaCol = static::getCol();

        if (static::USE_LAZY_LOAD){
            $mediaCol->setPageSize(static::pageSize());
        }

        $mediaCounts = [];

        /** @var Model $media */
        foreach ($mediaCol as $media) {
            $type = $media->getTargetType();

            if (!isset($mediaCounts[$type])) {
                $mediaCounts[$type] = 0;
            }

            $mediaCounts[$type]++;
        }

        $this->getTwig()->renderPage('media/list', [
            'type_names' => Types::names(),
            'media_types' => $this->getMediaTypes(),
            'media_col' => $mediaCol,
            'media_counts' => $mediaCounts,
            'col_left' => static::colLeft(),
        ]);

        return $this;
    }

    protected function renderSingle()
    {
        if ($this->getSingle()->modelType() == Types::album) {
            $photos = Photos::create()
                ->filterByAlbumId($this->getSingle()->getId())
                ->filterByLocalizedVisible(1)
                ->orderByOrderNum();
        } else {
            $photos = [];
        }

        $related = static::getCol()
            ->filterByTargetType($this->getSingle()->modelType())
            ->filterById($this->getSingle()->getId(), '!=')
            ->setPageSize(30);

        $typeName = Types::getName($this->getSingle()::type);

        $this->getTwig()->renderPage('media/single/' . $typeName, [
            'type_name' => Types::getName($this->getSingle()::type),
            'type_title' => ArrayHelper::get($this->getMediaTypes(), $this->getSingle()::type),
            'm' => $this->getSingle(),
            'photos' => $photos,
            'related' => $related,
        ]);

        $this->getZ()
            ->assignMeta($this->getSingle());

        return $this;
    }
}
