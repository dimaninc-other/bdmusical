<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 13.04.2021
 * Time: 18:06
 */

namespace Musical\Admin;

use Musical\Admin\Page\Dancer;
use Musical\Admin\Page\Musician;
use Musical\Admin\Page\Skater;

class Base extends \diCore\Admin\Base
{
    // protected $wysiwygVendor = \diCore\Admin\Form::wysiwygCK;

    protected function troopNeeded()
    {
        return true;
    }

    protected function getAdminMenuMainTree()
    {
        $ar = [
            'Страницы' => $this->getAdminMenuRow('content'),

            'Спектакли' => self::menuAdd($this->getAdminMenuRow('show'), [
                'items' => [
                    'Синхронизировать' => [
                        'link' => \diLib::getWorkerPath('booking', 'get'),
                    ],
                ],
            ]),

            'Цены' => $this->getAdminMenuRow('price'),
            'Новости' => $this->getAdminMenuRow('news'),
            'Видео' => $this->getAdminMenuRow('videos'),
            'Фото' => $this->getAdminMenuRow('photos'),
            'Альбомы' => $this->getAdminMenuRow('albums'),

            'Рекламные ссылки' => self::menuAdd(
                $this->getAdminMenuRow('slide'),
                $this->getAdminMenuRow('slide_block', [
                    'listTitleSuffix' => ' блоками',
                    'formTitleSuffix' => ' блок',
                ])
            ),

            'Пресса' => [
                'items' => [
                    'Телесюжеты' => 'smi_tv',
                    'Добавить телесюжет' => 'smi_tv/form',
                    'Пресса' => 'smi_press',
                    'Добавить прессу' => 'smi_press/form',
                    'Радиосюжеты' => 'smi_radio',
                    'Добавить радиосюжет' => 'smi_radio/form',
                ],
                'permissions' => ['root'],
                'paths' => ['smi_tv', 'smi_tv_form', 'smi_press', 'smi_press_form', 'smi_radio', 'smi_radio_form'],
            ],

            'Сезоны' => $this->getAdminMenuRow('season'),

            'Персонажи' => $this->getAdminMenuRow('character'),
            'Солисты' => $this->getAdminMenuRow('solo'),
        ];

        if ($this->troopNeeded()) {
            $ar = extend($ar, $this->getTroopTree());
        }

        $ar = extend($ar, [
            'Отзывы' => $this->getAdminMenuRow('review'),
            'Партнеры' => $this->getAdminMenuRow('partner'),
            'Клиенты' => $this->getAdminMenuRow('client'),
            'Акции' => $this->getAdminMenuRow('action'),
            'Медиа' => $this->getAdminMenuRow('media', [
                'showForm' => false,
            ]),
            //'Шрифты' => $this->getAdminMenuRow('fonts'),
            'Локализация' => $this->getAdminMenuRow('localization', [
                'permissions' => ['root'],
            ]),
            'Задачи' => $this->getAdminMenuRow('admin_tasks'),
        ]);

        return $ar;
    }

    protected function getTroopTree()
    {
        /** @var Musician $musicianClass */
        $musicianClass = \diLib::getClassNameFor('musician', \diLib::ADMIN_PAGE);
        /** @var Skater $skaterClass */
        $skaterClass = \diLib::getClassNameFor('skater', \diLib::ADMIN_PAGE);
        /** @var Dancer $dancerClass */
        $dancerClass = \diLib::getClassNameFor('dancer', \diLib::ADMIN_PAGE);

        $musician = $musicianClass::NEEDED ? [
            'Оркестр' => 'musician',
            'Добавить в оркестр' => 'musician/form',
        ] : [];

        $skater = $skaterClass::NEEDED ? [
            $skaterClass::CAPTION_LIST => 'skater',
            $skaterClass::CAPTION_ADD => 'skater/form',
        ] : [];

        return [
            'Труппа' => [
                'items' => extend([
                    'Создатели' => 'creator',
                    'Добавить создателя' => 'creator/form',
                    'Творческая группа' => 'creative',
                    'Добавить в творч.группу' => 'creative/form',
                    'Балет' => 'ballet',
                    'Добавить в балет' => 'ballet/form',
                    $dancerClass::CAPTION_LIST => 'dancer',
                    $dancerClass::CAPTION_ADD => 'dancer/form',
                ], $musician, $skater),
                'permissions' => ['root'],
                'paths' => [
                    'ballet',
                    'ballet_form',
                    'creator',
                    'creator_form',
                    'dancer',
                    'dancer_form',
                    'musician',
                    'musician_form',
                    'creative',
                    'creative_form',
                    'skater',
                    'skater_form',
                ],
            ],
        ];
    }

    protected function getExtraWysiwygSettings()
    {
        return <<<'EOF'
    link_class_list: [
        {title: 'None', value: ''},
        {title: 'Enlarged pic', value: 'js-lightbox'}
    ],
EOF;
    }
}
