<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 13.04.2021
 * Time: 18:51
 */

namespace Musical\Admin;

class AdminFormHelper
{
    public static function getMainRuEnTabs()
    {
        return [
            'ru' => 'Рус',
            'en' => 'Eng',
            'meta' => 'SEO Рус',
            'en_meta' => 'SEO Eng',
        ];
    }
}
