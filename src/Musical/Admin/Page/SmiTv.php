<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use Musical\Entity\SmiTv\Model;

class SmiTv extends Videos
{
    protected function initTable()
    {
        $this->setTable('smi_tv');
    }

    public function orderChangeAllowed()
    {
        return true;
    }

    public function renderForm()
    {
        parent::renderForm();

        $this->getForm()
            ->setHiddenInput('album_id');
    }

    public function getModuleCaption()
    {
        return 'Телесюжеты';
    }
}
