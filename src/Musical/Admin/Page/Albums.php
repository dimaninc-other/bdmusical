<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use diCore\Admin\Base;
use diCore\Admin\Reference\PhotosOfAlbum;
use diCore\Helper\ArrayHelper;
use diCore\Tool\CollectionCache;
use Musical\Admin\AdminFormHelper;
use Musical\Entity\Album\Model;
use Musical\Entity\Album\Purpose;
use Musical\Entity\Season\Collection as Seasons;

class Albums extends \diCore\Admin\Page\Albums
{
    public function __construct(Base $X)
    {
        parent::__construct($X);

        CollectionCache::add([
            Seasons::create()->orderByOrderNum(),
        ]);
    }

    public function renderList()
    {
        parent::renderList();

        $this->getList()
            ->insertColumnsAfter('id', [
                '#href' => '',
            ])
            ->insertColumnsAfter('#visible', [
                '#en_visible' => '',
            ])
            ->insertColumnsAfter('title', [
                'en_title' => [
                    'headAttrs' => [
                        'width' => '40%',
                    ],
                ],
                'purpose' => [
                    'headAttrs' => [
                        'width' => '10%',
                    ],
                    'value' => function (Model $m) {
                        return $m->getPurposeTitle();
                    },
                ],
            ])
            ->setColumnAttr('title', [
                'headAttrs' => [
                    'width' => '40%',
                ],
            ]);
    }

    public function renderForm()
    {
        parent::renderForm();

        $this->getForm()
            ->setHiddenInput([
                //'pic',
                'cover_photo_id',
                'force_pic',
                'comments_enabled',
                'comments_count',
                'token',
                'comments_last_date',
            ])
            ->setSelectFromCollectionInput('season_id', CollectionCache::get(Seasons::type))
            ->setSelectFromArrayInput('purpose', Purpose::$titles, [
                0 => 'Нет',
            ]);
    }

    public function submitForm()
    {
        parent::submitForm();

        $this->getSubmit()
            ->makeSlug(null, 'en_slug');
    }

    public function getFormTabs()
    {
        return extend(parent::getFormTabs(), AdminFormHelper::getMainRuEnTabs(), [
            'photos' => 'Фото',
        ]);
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar = self::setFieldsEditable($ar, ['date']);

        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'date', [
            'season_id' => [
                'type' => 'int',
                'title' => 'Сезон',
                'default' => '',
            ],

            'purpose' => [
                'type' => 'int',
                'title' => 'Спец.предназначение',
                'default' => '',
            ],

            'meta_title' => [
                'type' => 'string',
                'title' => 'Meta-заголовок',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_keywords' => [
                'type' => 'string',
                'title' => 'Meta-ключевые слова',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_description' => [
                'type' => 'string',
                'title' => 'Meta-описание',
                'default' => '',
                'tab' => 'meta',
            ],

            'en_meta_title' => [
                'type' => 'string',
                'title' => 'Meta-заголовок',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_keywords' => [
                'type' => 'string',
                'title' => 'Meta-ключевые слова',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_description' => [
                'type' => 'string',
                'title' => 'Meta-описание',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_title' => [
                'type' => 'string',
                'title' => 'Название',
                'default' => '',
                'tab' => 'en',
            ],

            'en_content' => [
                'type' => 'wysiwyg',
                'title' => 'Описание',
                'default' => '',
                'tab' => 'en',
            ],

            'en_slug_source' => [
                'type' => 'string',
                'title' => 'Название для URL',
                'default' => '',
                'flags' => ['hidden'],
                'tab' => 'en',
            ],

            'en_visible' => [
                'type' => 'checkbox',
                'title' => 'Отображать на сайте',
                'default' => 1,
                'tab' => 'en',
            ],
        ]);

        $ar['title']['tab'] =
        $ar['content']['tab'] =
            //$ar['slug_source']['tab'] =
        $ar['visible']['tab'] =
            'ru';

        $ar = extend($ar, [
            'photos' => PhotosOfAlbum::getFormFieldArray(),
        ]);

        return $ar;
    }

    public function getLocalFields()
    {
        return extend(parent::getLocalFields(), [
            'en_slug' => [
                'type' => 'string',
                'title' => 'Slug',
                'default' => '',
            ],
        ]);
    }
}
