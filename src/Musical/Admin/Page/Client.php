<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use diCore\Admin\Submit;
use Musical\Entity\Client\Model;

class Client extends \diCore\Admin\BasePage
{
    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'order_num',
                'dir' => 'ASC',
            ],
        ],
    ];

    protected function initTable()
    {
        $this->setTable('client');
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            'pic' => [
                'bodyAttrs' => [
                    'class' => 'no-padding',
                ],
                'value' => function (Model $model) {
                    return $model->hasPic()
                        ? "<img src=\"/{$model->wrapFileWithPath($model->getPic())}\" width=\"200\">"
                        : '';
                },
            ],
            'title' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'href' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'created_at' => [
                // 'title' => 'Дата',
                'value' => function (Model $m) {
                    return \diDateTime::simpleFormat($m->getCreatedAt());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
                'bodyAttrs' => [
                    'class' => 'dt',
                ],
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    public function renderForm()
    {
    }

    public function submitForm()
    {
        $this->getSubmit()
            ->storeImage(['pic'], [
                [
                    'type' => Submit::IMAGE_TYPE_MAIN,
                ],
            ]);
    }

    public function getFormTabs()
    {
        return [
            'ru' => 'Рус',
            'en' => 'Eng',
        ];
    }

    public function getFormFields()
    {
        return [
            'title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'ru',
            ],

            'href' => [
                'type' => 'string',
                'title' => 'Ссылка',
                'default' => '',
            ],

            'caption' => [
                'type' => 'string',
                'title' => 'Заголовок отзыва',
                'default' => '',
                'tab' => 'ru',
            ],

            'content' => [
                'type' => 'wysiwyg',
                'title' => '',
                'default' => '',
                'tab' => 'ru',
            ],

            'en_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en',
            ],

            'en_caption' => [
                'type' => 'string',
                'title' => 'Заголовок отзыва',
                'default' => '',
                'tab' => 'en',
            ],

            'en_content' => [
                'type' => 'wysiwyg',
                'default' => '',
                'tab' => 'en',
            ],

            'pic' => [
                'type' => 'pic',
                'title' => 'Логотип',
                'default' => '',
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],
        ];
    }

    public function getLocalFields()
    {
        return [
            'pic_w' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],

            'pic_h' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],

            'order_num' => [
                'type' => 'order_num',
                'title' => '',
                'default' => '',
                'direction' => 1,
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Клиенты';
    }

    public function useEditLog()
    {
        return true;
    }
}
