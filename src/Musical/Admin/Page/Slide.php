<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use diCore\Admin\Base;
use diCore\Tool\CollectionCache;
use Musical\Entity\Slide\Model;
use Musical\Entity\SlideBlock\Collection as Blocks;

class Slide extends \diCore\Admin\BasePage
{
    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'order_num',
                'dir' => 'ASC',
            ],
        ],
    ];

    public function __construct(Base $X)
    {
        parent::__construct($X);

        CollectionCache::add([
            Blocks::create()->orderByOrderNum(),
        ]);
    }

    protected function initTable()
    {
        $this->setTable('slide');
    }

    protected function setupFilters()
    {
        $this->getFilters()
            ->addFilter([
                'field' => 'block_id',
                'type' => 'int',
                'title' => 'Блок',
                'strict' => true,
            ])
            ->buildQuery()
            ->setSelectFromCollectionInput('block_id', CollectionCache::get(Blocks::type));
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            'title' => [
                'headAttrs' => [
                    'width' => '35%',
                ],
                'value' => function (Model $slide) {
                    return '<div>' . $slide->getTitle() . '</div><div class="lite">' . $slide->getContent() . '</div>';
                },
            ],
            'href' => [
                'headAttrs' => [
                    'width' => '15%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '35%',
                ],
                'value' => function (Model $slide) {
                    return '<div>' . $slide->getEnTitle() . '</div><div class="lite">' . $slide->getEnContent() . '</div>';
                },
            ],
            'en_href' => [
                'headAttrs' => [
                    'width' => '15%',
                ],
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    public function renderForm()
    {
        $this->getForm()
            ->setSelectFromCollectionInput('block_id', CollectionCache::get(Blocks::type));
    }

    public function submitForm()
    {
    }

    public function getFormTabs()
    {
        return [
            'ru' => 'Рус',
            'en' => 'Eng',
        ];
    }

    public function getFormFields()
    {
        return [
            'block_id' => [
                'type' => 'int',
                'title' => 'Блок',
                'default' => '',
            ],

            'title' => [
                'type' => 'string',
                'title' => 'Текст',
                'default' => '',
                'tab' => 'ru',
            ],

            'content' => [
                'type' => 'string',
                'title' => 'Текст №2',
                'default' => '',
                'tab' => 'ru',
            ],

            'href' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'ru',
            ],

            'en_title' => [
                'type' => 'string',
                'title' => 'Текст',
                'default' => '',
                'tab' => 'en',
            ],

            'en_content' => [
                'type' => 'string',
                'title' => 'Текст №2',
                'default' => '',
                'tab' => 'en',
            ],

            'en_href' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en',
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],
        ];
    }

    public function getLocalFields()
    {
        return [
            'order_num' => [
                'type' => 'order_num',
                'default' => '',
                'direction' => 1,
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Слайды';
    }

    public function useEditLog()
    {
        return true;
    }
}
