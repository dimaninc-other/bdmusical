<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 16.04.2021
 * Time: 11:40
 */

namespace Musical\Admin\Page;

use diCore\Admin\Base;
use diCore\Tool\CollectionCache;
use Musical\Entity\Season\Collection as Seasons;

class BaseSmi extends BaseEntity
{
    public function __construct(Base $X)
    {
        parent::__construct($X);

        CollectionCache::add([
            Seasons::create()->orderByOrderNum(),
        ]);
    }

    protected function setupFilters()
    {
        parent::setupFilters();

        $this->getFilters()
            ->addFilter([
                'field' => 'season_id',
                'type' => 'int',
                'title' => 'Сезон',
            ])
            ->buildQuery()
            ->setSelectFromCollectionInput(
                'season_id',
                CollectionCache::get(Seasons::type),
                [0 => 'Все сезоны']
            );
    }

    protected function addUpDownToList()
    {
        if ($this->getFilters()->getData('season_id')) {
            $this->getList()->addColumns([
                '#up' => '',
                '#down' => '',
            ]);
        }

        return $this;
    }

    public function renderForm()
    {
        $this->getForm()
            ->setSelectFromCollectionInput('season_id', CollectionCache::get(Seasons::type));
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar = self::setFieldsHidden($ar, [
            'short_content',
            'en_short_content',
        ]);

        $ar = extend([
            'season_id' => [
                'type' => 'int',
                'title' => 'Сезон',
                'default' => '',
            ],
        ], $ar);

        return $ar;
    }

    public function getLocalFields()
    {
        return extend(parent::getLocalFields(), [
            'pic_tn_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic_tn_h' => [
                'type' => 'int',
                'default' => '',
            ],
        ]);
    }
}
