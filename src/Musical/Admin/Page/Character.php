<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use diCore\Helper\ArrayHelper;
use Musical\Entity\Character\Model;

class Character extends BaseEntity
{
    protected function initTable()
    {
        $this->setTable('character');
    }

    public function renderList()
    {
        parent::renderList();

        $this->getList()
            ->insertColumnsAfter('en_title', [
                'home_css_class' => [
                    'bodyAttrs' => [
                        'class' => 'lite',
                    ],
                    'title' => 'CSS-класс',
                ]
            ]);
    }

    public function submitForm()
    {
    }

    public function getFormTabs()
    {
        return [];
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        unset($ar['pic']);
        unset($ar['slug_source']);
        unset($ar['en_slug_source']);
        $ar = self::setFieldsHidden($ar, [
            'short_content',
            'content',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'en_short_content',
            'en_content',
            'en_meta_title',
            'en_meta_keywords',
            'en_meta_description',
        ]);

        $ar['title']['title'] = 'Имя';
        $ar['en_title']['title'] = 'Имя (Eng)';

        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'en_title', [
            'home_css_class' => [
                'type' => 'string',
                'title' => 'CSS-класс для главной',
                'default' => '',
            ],
        ]);

        return $ar;
    }

    public function getLocalFields()
    {
        return [
            'order_num' => [
                'type' => 'order_num',
                'default' => '',
                'direction' => 1,
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Персонажи';
    }
}
