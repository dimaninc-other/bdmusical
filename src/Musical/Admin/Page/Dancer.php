<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use Musical\Entity\Dancer\Model;

class Dancer extends BaseTroupe
{
    const CAPTION_LIST = 'Ансамбль';
    const CAPTION_ADD = 'Добавить в ансамбль';

    protected function initTable()
    {
        $this->setTable('dancer');
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar['title']['title'] = 'ФИО';
        $ar['en_title']['title'] = 'ФИО';

        return $ar;
    }

    public function getModuleCaption()
    {
        return 'Члены ансамбля';
    }
}
