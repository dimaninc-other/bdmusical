<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use Musical\Entity\SmiPress\Model;

class SmiPress extends BaseSmi
{
    protected function initTable()
    {
        $this->setTable('smi_press');
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            '#href' => [],
            'pic' => $this->getPicListEntity(),
            'date' => [
                'value' => function (Model $m) {
                    return \diDateTime::simpleFormat($m->getDate());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
                'bodyAttrs' => [
                    'class' => 'dt',
                ],
            ],
            'title' => [
                'headAttrs' => [
                    'width' => '45%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '45%',
                ],
                'title' => 'Название (Eng)',
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
        ]);

        $this->addUpDownToList();
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        return extend($ar, [
            'source' => [
                'type' => 'string',
                'title' => 'Источник',
                'default' => '',
                'flags' => ['hidden'],
            ],

            'date' => [
                'type' => 'datetime_str',
                'title' => 'Дата публикации',
                'default' => \diDateTime::sqlFormat(),
            ],
        ]);
    }

    public function getModuleCaption()
    {
        return 'Пресса';
    }
}
