<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 14.05.2021
 * Time: 11:58
 */

namespace Musical\Admin\Page;

class BaseTroupe extends BaseEntity
{
    public function renderList()
    {
        parent::renderList();

        $this->getList()->insertColumnsAfter('#href', [
            'pic' => $this->getPicListEntity(),
            'pic2' => $this->getPicListEntity('pic2'),
        ]);
    }

    protected function getSubmittedPicFields()
    {
        return array_merge(parent::getSubmittedPicFields(), ['pic2']);
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar = static::addPic2Field($ar);

        $ar = static::addCssClassField($ar);

        $ar = self::setFieldsHidden($ar, [
            'short_content',
            'en_short_content',
        ]);

        return $ar;
    }

    protected static function orderNumDirection()
    {
        return 1;
    }

    public function getLocalFields()
    {
        return extend(parent::getLocalFields(), [
            'en_slug' => [
                'type' => 'string',
                'default' => '',
            ],

            'pic2_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic2_h' => [
                'type' => 'int',
                'default' => '',
            ],
        ]);
    }
}