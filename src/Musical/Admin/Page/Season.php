<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use Musical\Entity\Season\Model;

class Season extends \diCore\Admin\BasePage
{
    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'order_num',
                'dir' => 'ASC',
            ],
        ],
    ];

    protected function initTable()
    {
        $this->setTable('season');
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            'title' => [
                'headAttrs' => [
                    'width' => '50%',
                ],
            ],
            'date1' => [
                'headAttrs' => [
                    'width' => '25%',
                ],
                'value' => function(Model $m) {
                    return $m->hasDate1() ? \diDateTime::simpleDateFormat($m->getDate1()) : '---';
                },
            ],
            'date2' => [
                'headAttrs' => [
                    'width' => '25%',
                ],
                'value' => function(Model $m) {
                    return $m->hasDate2() ? \diDateTime::simpleDateFormat($m->getDate2()) : '---';
                },
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    public function submitForm()
    {
        $this->getSubmit()
            ->makeSlug();
    }

    public function getFormTabs()
    {
        return [];
    }

    public function getFormFields()
    {
        return extend([
            'slug_source' => [
                'type' => 'string',
                'default' => '',
                'flags' => ['hidden'],
            ],

            'title' => [
                'type' => 'string',
                'default' => '',
            ],

            'content' => [
                'type' => 'wysiwyg',
                'default' => '',
                'flags' => ['hidden'],
            ],

            'date1' => [
                'type' => 'date_str',
                'title' => 'Дата начала сезона',
                'default' => '',
            ],

            'date2' => [
                'type' => 'date_str',
                'title' => 'Дата окончания сезона',
                'default' => '',
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'title' => 'Сезон создан',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],

            'visible' => [
                'type' => 'checkbox',
                'default' => 1,
            ],
        ]);
    }

    public function getLocalFields()
    {
        return [
            'slug' => [
                'type' => 'string',
                'default' => '',
            ],

            'order_num' => [
                'type' => 'order_num',
                'default' => '',
                'direction' => -1,
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Сезоны';
    }

    public function useEditLog()
    {
        return true;
    }
}
