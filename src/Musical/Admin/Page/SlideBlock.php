<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use Musical\Entity\SlideBlock\Model;

class SlideBlock extends \diCore\Admin\BasePage
{
    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'order_num',
                'dir' => 'ASC',
            ],
        ],
    ];

    protected function initTable()
    {
        $this->setTable('slide_block');
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            'title' => [
                'headAttrs' => [
                    'width' => '100%',
                ],
            ],
            '#edit' => '',
            '#del' => '',
            //'#visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    public function renderForm()
    {
    }

    public function submitForm()
    {
    }

    public function getFormTabs()
    {
        return [];
    }

    public function getFormFields()
    {
        return [
            'title' => [
                'type' => 'string',
                'default' => '',
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],
        ];
    }

    public function getLocalFields()
    {
        return [
            'order_num' => [
                'type' => 'order_num',
                'default' => '',
                'direction' => 1,
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Блоки слайдов';
    }

    public function useEditLog()
    {
        return true;
    }
}
