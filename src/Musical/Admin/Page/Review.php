<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use Musical\Entity\Review\Model;

class Review extends BaseEntity
{
    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'id',
                'dir' => 'DESC',
            ],
            'sortByAr' => [
                'title' => 'По ФИО',
                'position' => 'По роду занятий',
                'id' => 'По ID',
            ],
        ],
    ];

    protected function initTable()
    {
        $this->setTable('review');
    }

    protected function setupFilters()
    {
        $this->getFilters()
            ->addFilter([
                'field' => 'type',
                'type' => 'int',
                'title' => 'Тип',
            ])
            ->addFilter([
                'field' => 'title',
                'type' => 'string',
            ])
            ->addFilter([
                'field' => 'position',
                'type' => 'string',
            ])
            ->buildQuery()
            ->setSelectFromArrayInput('type', Model::$types, [
                0 => 'Все',
            ]);
    }

    protected function getPreviewIdxForPicInList()
    {
        return 1;
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            '#href' => [],
            'type' => [
                'value' => function (Model $m) {
                    return $m->getTypeTitle();
                },
            ],
            'title' => [
                'headAttrs' => [
                    'width' => '20%',
                ],
            ],
            'position' => [
                'headAttrs' => [
                    'width' => '20%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '20%',
                ],
            ],
            'en_position' => [
                'headAttrs' => [
                    'width' => '20%',
                ],
            ],
            'pic' => [
                'value' => function (Model $m) {
                    return $m->hasPic()
                        ? sprintf(
                            '<img src="/%s">',
                            $m->wrapFileWithPath($m->getPic(), $this->getPreviewIdxForPicInList())
                        )
                        : '';
                },
                'bodyAttrs' => [
                    'class' => 'no-padding',
                ],
            ],
            'created_at' => [
                'value' => function(Model $m) {
                    return \diDateTime::simpleFormat($m->getCreatedAt());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
                'bodyAttrs' => [
                    'class' => 'dt',
                ],
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    public function renderForm()
    {
        $this->getForm()
            ->setSelectFromArrayInput('type', Model::$types);
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar = extend([
            'type' => [
                'type' => 'int',
                'title' => 'Отзыв от',
                'default' => Model::TYPE_CELEBRITY,
            ],

            'email' => [
                'type' => 'string',
                'default' => '',
            ],
        ], $ar);

        $ar['pic']['title'] = 'Фото';

        $ar['title']['title'] =
        $ar['en_title']['title'] =
            'ФИО';

        $ar['position']['title'] =
        $ar['en_position']['title'] =
            'Род занятий';

        // $ar = self::setFieldsHidden($ar, ['short_content', 'en_short_content']);

        $ar['content']['type'] =
        $ar['en_content']['type'] =
            'text';

        $ar['short_content']['options']['rows'] =
        $ar['en_short_content']['options']['rows'] =
            2;

        return $ar;
    }

    public function getLocalFields()
    {
        return [
            'slug' => [
                'type' => 'string',
                'default' => '',
            ],

            'pic_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic_h' => [
                'type' => 'int',
                'default' => '',
            ],

            'order_num' => [
                'type' => 'order_num',
                'default' => '',
                'direction' => -1,
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Отзывы';
    }
}
