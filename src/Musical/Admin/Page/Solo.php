<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use diCore\Admin\Base;
use diCore\Admin\Submit;
use diCore\Helper\ArrayHelper;
use diCore\Tool\CollectionCache;
use Musical\Entity\Character\Collection as Characters;
use Musical\Entity\Solo\Model;

class Solo extends BaseTroupe
{
    public function __construct(Base $X)
    {
        parent::__construct($X);

        CollectionCache::add([
            Characters::create()->orderByOrderNum(),
        ]);
    }

    protected function initTable()
    {
        $this->setTable('solo');
    }

    protected function setupFilters()
    {
        parent::setupFilters();

        $this->getFilters()
            ->addFilter([
                'field' => 'character_id',
                'type' => 'int',
            ])
            ->buildQuery()
            ->setSelectFromCollectionInput(
                'character_id',
                CollectionCache::get(Characters::type),
                [
                    0 => 'Все',
                ]
            );
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            '#href' => [],
            'pic' => $this->getPicListEntity(),
            'pic2' => $this->getPicListEntity('pic2'),
            // 'pic3' => $this->getPicListEntity('pic3'),
            'title' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
                'title' => 'Имя (Eng)',
            ],
            'character_id' => [
                'headAttrs' => [
                    'width' => '40%',
                ],
                'value' => function(Model $m) {
                    return $m->getCharacter()->getTitle();
                },
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
        ]);

        if (!$this->getFilters()->getData('character_id') || true) {
            $this->getList()->addColumns([
                '#up' => '',
                '#down' => '',
            ]);
        }
    }

    public function renderForm()
    {
        $this->getForm()
            ->setSelectFromCollectionInput('character_id', CollectionCache::get(Characters::type));
    }

    protected function getSubmittedPicFields()
    {
        return array_merge(parent::getSubmittedPicFields(), ['pic3']);
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar['title']['title'] = 'Имя';
        $ar['en_title']['title'] = 'Имя';

        $ar = extend([
            'character_id' => [
                'type' => 'int',
                'title' => 'Персонаж',
                'default' => '',
            ],

            'home_css_class' => [
                'type' => 'string',
                'title' => 'CSS-класс на главной странице',
                'default' => '',
            ],

            'show_pic' => [
                'type' => 'checkbox',
                'title' => 'Отображать фото и ссылку',
                'default' => 1,
            ],
        ], $ar);

        /*
        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'pic2', [
            'pic3' => [
                'type' => 'pic',
                'title' => 'Изображение для страницы солистов',
                'default' => '',
                'naming' => Submit::FILE_NAMING_ORIGINAL,
            ],
        ]);
        */

        return $ar;
    }

    public function getModuleCaption()
    {
        return 'Солисты';
    }
}
