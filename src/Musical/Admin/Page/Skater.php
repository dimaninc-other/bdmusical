<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use Musical\Entity\Skater\Model;

class Skater extends BaseTroupe
{
    const CAPTION_LIST = 'Трюки'; // 'Фигуристы'
    const CAPTION_ADD = 'Добавить каскадёра'; // 'Добавить фигуриста'
    const NEEDED = true;

    protected function initTable()
    {
        $this->setTable('skater');
    }

    public function getModuleCaption()
    {
        return static::CAPTION_LIST;
    }
}
