<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 13.04.2021
 * Time: 18:57
 */

namespace Musical\Admin\Page;

use diCore\Admin\FilterRule;
use diCore\Admin\Submit;
use diCore\Helper\ArrayHelper;
use Musical\Admin\AdminFormHelper;

class BaseEntity extends \diCore\Admin\BasePage
{
    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'order_num',
                'dir' => 'ASC',
            ],
        ],
    ];

    protected function setupFilters()
    {
        $this->getFilters()
            ->addFilter([
                'field' => 'title',
                'type' => 'string',
                'rule' => FilterRule::contains,
            ])
            ->buildQuery();
    }

    protected function getPicListEntity($field = 'pic', $previewIdx = 1)
    {
        return [
            'bodyAttrs' => [
                'class' => 'no-padding',
            ],
            'value' => function (\diModel $model) use ($field, $previewIdx) {
                $src = $model->has($field)
                    ? '/' . $model->wrapFileWithPath($model->get($field), $previewIdx)
                    : '';

                return $src
                    ? "<img src=\"$src\" style=\"max-width: 200px; max-height: 200px\" alt=\"\">"
                    : '';
            },
        ];
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            '#href' => [],
            'title' => [
                'headAttrs' => [
                    'width' => '50%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '50%',
                ],
                'title' => 'Название (Eng)',
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    protected function getSubmittedPicFields()
    {
        return ['pic'];
    }

    public function submitForm()
    {
        $this->getSubmit()
            ->makeSlug()
            ->storeImage($this->getSubmittedPicFields());

        if ($this->getCurrentModel()->exists('en_slug')) {
            $this->getSubmit()
                ->makeSlug(
                    $this->getCurrentModel()->getEnTitle() ?: $this->getCurrentModel()->getTitle(),
                    'en_slug');
        }
    }

    public function getFormTabs()
    {
        return AdminFormHelper::getMainRuEnTabs();
    }

    protected function hasPositionField()
    {
        $m = \diModel::createForTableNoStrict($this->getTable());

        return !!$m::getFieldType('position');
    }

    protected static function addPic2Field($ar)
    {
        return ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'pic', [
            'pic2' => [
                'type' => 'pic',
                'title' => 'Изображение в полный рост',
                'default' => '',
                'naming' => Submit::FILE_NAMING_ORIGINAL,
            ],
        ]);
    }

    protected static function addCssClassField($ar)
    {
        return ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'slug_source', [
            'css_class' => [
                'type' => 'string',
                'title' => 'CSS-класс',
                'default' => '',
            ],
        ]);
    }

    public function getFormFields()
    {
        $ar = [
            'slug_source' => [
                'type' => 'string',
                'default' => '',
                //'tab' => 'ru',
            ],

            'title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'ru',
            ],

            'short_content' => [
                'type' => 'text',
                'default' => '',
                'tab' => 'ru',
            ],

            'content' => [
                'type' => 'wysiwyg',
                'default' => '',
                'tab' => 'ru',
            ],

            'meta_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_keywords' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_description' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'en_slug_source' => [
                'type' => 'string',
                'title' => 'Название для URL',
                'default' => '',
                'tab' => 'en',
                'flags' => ['hidden'],
            ],

            'en_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en',
            ],

            'en_short_content' => [
                'type' => 'text',
                'default' => '',
                'tab' => 'en',
            ],

            'en_content' => [
                'type' => 'wysiwyg',
                'default' => '',
                'tab' => 'en',
            ],

            'en_meta_title' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_keywords' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_description' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'pic' => [
                'type' => 'pic',
                'default' => '',
                'naming' => Submit::FILE_NAMING_ORIGINAL,
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],
        ];

        if ($this->hasPositionField()) {
            $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'title', [
                'position' => [
                    'type' => 'string',
                    'title' => 'Должность',
                    'default' => '',
                    'tab' => 'ru',
                ],
            ]);

            $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'en_title', [
                'en_position' => [
                    'type' => 'string',
                    'title' => 'Должность',
                    'default' => '',
                    'tab' => 'en',
                ],
            ]);
        }

        return $ar;
    }

    protected static function orderNumDirection()
    {
        return -1;
    }

    public function getLocalFields()
    {
        return [
            'slug' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'en_slug' => [
                'type' => 'string',
                'default' => '',
            ],

            'pic_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic_h' => [
                'type' => 'int',
                'default' => '',
            ],

            'order_num' => [
                'type' => 'order_num',
                'default' => '',
                'direction' => static::orderNumDirection(),
            ],
        ];
    }

    public function useEditLog()
    {
        return true;
    }
}
