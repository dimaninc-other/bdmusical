<?php
/**
 * Created by \diAdminPagesManager
 * Date: 20.05.2021
 * Time: 16:53
 */

namespace Musical\Admin\Page;

use diCore\Admin\FilterRule;
use diCore\Traits\Admin\TargetInside;
use Musical\Entity\Media\Model;

class Media extends \diCore\Admin\BasePage
{
    use TargetInside;

    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'id',
                'dir' => 'DESC',
            ],
        ],
    ];

    protected function initTable()
    {
        $this->setTable('media');
    }

    protected function setupFilters()
    {
        $this->getFilters()
            ->addFilter([
                'field' => 'title',
                'type' => 'string',
                'rule' => FilterRule::contains,
            ])
            ->buildQuery();
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            '#href' => [],
            'target' => [
                'headAttrs' => [
                    'width' => '10%',
                ],
                'value' => function(Model $m) {
                    return $m->getTargetTypeName() . '#' . $m->getTargetId();
                },
            ],
            'title' => [
                'headAttrs' => [
                    'width' => '70%',
                ],
            ],
            'pic' => [],
            'date' => [
                'value' => function(Model $m) {
                    return \diDateTime::simpleFormat($m->getDate());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
                'bodyAttrs' => [
                    'class' => 'dt',
                ],
            ],
            'created_at' => [
                'value' => function(Model $m) {
                    return \diDateTime::simpleFormat($m->getCreatedAt());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
                'bodyAttrs' => [
                    'class' => 'dt',
                ],
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
        ]);
    }

    public function renderForm()
    {
        $this->tiAddToForm($this, array_keys(Model::$targetTypes));
    }

    public function submitForm()
    {
    }

    public function getFormTabs()
    {
        return [];
    }

    public function getFormFields()
    {
        return [
            'season_id' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],

            'pic_type' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],

            'title' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'content' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'short_content' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'visible' => [
                'type' => 'checkbox',
                'title' => '',
                'default' => '',
            ],

            'top' => [
                'type' => 'checkbox',
                'title' => '',
                'default' => '',
            ],

            'en_slug' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'en_title' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'en_content' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'en_short_content' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'en_visible' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],

            'en_top' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],

            'vendor' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],

            'vendor_video_uid' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'pic' => [
                'type' => 'pic',
                'title' => '',
                'default' => '',
            ],

            'date' => [
                'type' => 'datetime_str',
                'title' => '',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'title' => '',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],
        ];
    }

    public function getLocalFields()
    {
        return [
            'slug' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'pic_w' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],

            'pic_h' => [
                'type' => 'int',
                'title' => '',
                'default' => '',
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Медиа (агрегатор)';
    }

    public function addButtonNeededInCaption()
    {
        return false;
    }
}