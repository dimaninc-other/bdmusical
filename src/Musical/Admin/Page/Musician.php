<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use Musical\Entity\Musician\Model;

class Musician extends Dancer
{
    const NEEDED = true;

    protected function initTable()
    {
        $this->setTable('musician');
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar['short_content']['title'] = 'Инструменты';
        $ar['en_short_content']['title'] = 'Инструменты';

        return $ar;
    }

    public function getModuleCaption()
    {
        return 'Музыканты оркестра';
    }
}
