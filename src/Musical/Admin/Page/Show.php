<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use Musical\Entity\Character\Collection as Characters;
use Musical\Entity\Show\Model;
use Musical\Entity\Solo\Collection as Solos;

class Show extends \diCore\Admin\BasePage
{
    protected $options = [
        'showControlPanel' => true,
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'date',
                'dir' => 'DESC',
            ],
        ],
    ];

    protected function initTable()
    {
        $this->setTable('show');
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            '_checkbox' => [],
            'id' => 'ID',
            'widget_id' => [
                'title' => 'W#',
            ],
            'revaluation' => [
                'title' => 'Дин. цена',
                'value' => function (Model $m) {
                    return $m->hasRevaluation() ? '+' : '';
                },
            ],
            'moved' => [
                'title' => 'Перенос',
                'value' => function (Model $m) {
                    return $m->hasMoved() ? '→' : '';
                },
            ],
            'qr' => [
                'title' => 'QR',
                'value' => function (Model $m) {
                    return $m->hasQr() ? '◲' : '';
                },
            ],
            'date' => [
                'title' => 'Дата',
                'value' => function (Model $m) {
                    return \diDateTime::simpleDateFormat($m->getDate());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
            ],
            'time' => [
                'title' => 'Время',
                'value' => function (Model $m) {
                    return \diDateTime::simpleTimeFormat($m->getDate());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
            ],
            'staff_status' => [
                'headAttrs' => [
                    'width' => '40%',
                ],
                'value' => function (Model $m) {
                    return $m->getStaffStatusStr();
                },
            ],
            'ticket_status' => [
                'headAttrs' => [
                    'width' => '40%',
                ],
                'value' => function (Model $m) {
                    return $m->getTicketStatusStr();
                },
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
        ]);
    }

    public function renderForm()
    {
        /** @var Model $m */
        $m = $this->getForm()->getModel();

        if (!$m->hasWidgetId()) {
            $this->getForm()
                ->setHiddenInput('widget_id');
        }

        $this->injectJs('Show');

        $this->getForm()
            ->setTwigInput('staff', 'admin/show/staff', [
                'model' => $this->getCurrentModel(),
                'characters' => Characters::create()->orderByOrderNum(),
                'solos' => Solos::create(),
                'show_staff_ids' => Solos::createByShowId($this->getId())->map('id'),
            ])
            ->setSelectFromArrayInput('flag', Model::$flags)
            ->setSelectFromArrayInput('staff_status', Model::$staffStatuses)
            ->setSelectFromArrayInput('ticket_status', Model::$ticketStatuses);
    }

    public function submitForm()
    {
    }

    /*
    protected function redirectAfterSubmit()
    {
        // return parent::redirectAfterSubmit();
        return $this;
    }
    */

    protected function afterSubmitForm()
    {
        parent::afterSubmitForm();

        /** @var Model $m */
        $m = $this->getCurrentModel();
        $m->storeStaff($this->isNew());
    }

    public function getFormTabs()
    {
        return [];
    }

    public function getFormFields()
    {
        return [
            'date' => [
                'type' => 'datetime_str',
                'title' => 'Дата и время',
                'default' => 'NOW()',
            ],

            'ticket_status' => [
                'type' => 'int',
                'title' => 'Билеты',
                'default' => '',
            ],

            'revaluation' => [
                'type' => 'checkbox',
                'title' => 'Динамическая цена',
                'default' => 0,
            ],

            'moved' => [
                'type' => 'checkbox',
                'title' => 'Спектакль перенесён',
                'default' => 0,
            ],

            'qr' => [
                'type' => 'checkbox',
                'title' => 'Необходим QR-код',
                'default' => 0,
            ],

            'staff_status' => [
                'type' => 'int',
                'title' => 'Состав',
                'default' => '',
            ],

            'staff' => [
                'type' => 'string',
                'title' => 'Выберите актеров',
                'default' => '',
                'flags' => ['virtual'],
            ],

            'staff_comment' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
                'flags' => ['hidden'],
            ],

            'widget_id' => [
                'type' => 'int',
                'title' => 'ID в виджете',
                'default' => '',
                'flags' => ['static'],
            ],

            'flag' => [
                'type' => 'int',
                'title' => 'Лейбл в календаре',
                'default' => '',
            ],

            'flag_caption' => [
                'type' => 'string',
                'title' => 'Текст лейбла (Рус)',
                'default' => '',
            ],

            'flag_href' => [
                'type' => 'string',
                'title' => 'Ссылка лейбла (Рус)',
                'default' => '',
            ],

            'en_flag_caption' => [
                'type' => 'string',
                'title' => 'Текст лейбла (Eng)',
                'default' => '',
            ],

            'en_flag_href' => [
                'type' => 'string',
                'title' => 'Ссылка лейбла (Eng)',
                'default' => '',
            ],
        ];
    }

    public function getLocalFields()
    {
        return [];
    }

    public function getModuleCaption()
    {
        return 'Спектакли';
    }

    public function useEditLog()
    {
        return true;
    }
}
