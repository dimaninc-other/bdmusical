<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use Musical\Entity\Ballet\Model;

class Ballet extends BaseTroupe
{
    protected function initTable()
    {
        $this->setTable('ballet');
    }

    public function getModuleCaption()
    {
        return 'Танцоры балета';
    }
}
