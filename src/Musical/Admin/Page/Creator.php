<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use Musical\Entity\Creator\Model;

class Creator extends Creative
{
    protected function initTable()
    {
        $this->setTable('creator');
    }

    public function getModuleCaption()
    {
        return 'Создатели';
    }
}
