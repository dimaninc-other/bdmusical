<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use diCore\Helper\ArrayHelper;
use Musical\Entity\Action\Model;
use Musical\Entity\Action\Purpose;

class Action extends BaseEntity
{
    protected function initTable()
    {
        $this->setTable('action');
    }

    public function renderList()
    {
        parent::renderList();

        $this->getList()->insertColumnsAfter('#href', [
            'pic' => $this->getPicListEntity(),
            'purpose' => [
                'bodyAttrs' => [
                    'class' => 'lite',
                ],
                'value' => function (Model $m) {
                    return $m->getPurposeTitle();
                },
            ],
        ]);
    }

    public function renderForm()
    {
        parent::renderForm();

        $this->getForm()
            ->setSelectFromArrayInput('purpose', Purpose::$titles, [
                0 => '&mdash;',
            ]);
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'slug_source', [
            'purpose' => [
                'type' => 'int',
                'title' => 'Предназначение',
                'default' => 0,
            ],
        ]);

        return $ar;
    }

    public function getModuleCaption()
    {
        return 'Акции';
    }
}
