<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use diCore\Helper\ArrayHelper;
use Musical\Entity\Photo\Model;

class Photos extends \diCore\Admin\Page\Photos
{
    protected $filters = [
        'album_id' => [
            'field' => 'album_id',
            'type' => 'int',
            'title' => 'Альбом',
            'strict' => true,
            'showAllOption' => false,
        ]
    ];

    public function getPicStoreOptions()
    {
        return Model::$picOptions;
    }

    protected function setupFilters()
    {
        $uploadAreaHtml = $this->getTpl()
            ->define('`photos/list', [
                'filters_upload_area'
            ])
            ->parse('filters_upload_area');

        $this->getFilters()
            ->setButtonsSuffix($uploadAreaHtml);

        parent::setupFilters();
    }

    public function renderList()
    {
        parent::renderList();

        $this->getGrid()
            ->insertButtonsAfter('visible', [
                'en_visible' => '',
            ]);
    }

    public function getFormTabs()
    {
        return extend(parent::getFormTabs(), [
            //'ru' => 'Рус',
            //'en' => 'Eng',
            'meta' => 'SEO Рус',
            'en_meta' => 'SEO Eng',
        ]);
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar = self::setFieldsEditable($ar, ['date']);
        $ar = self::setFieldsHidden($ar, ['comments_enabled', 'comments_count', 'comments_last_date', 'token']);
        $ar = self::setFieldsHidden($ar, ['content', 'slug_source']); //'title',

        $ar = extend($ar, [
            'meta_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_keywords' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_description' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'en_meta_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_keywords' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_description' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],
        ]);

        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'title', [
            'en_title' => [
                'type' => 'string',
                'title' => 'Название (Eng)',
                'default' => '',
                //'tab' => 'en',
            ],
        ]);

        return $ar;
    }
}
