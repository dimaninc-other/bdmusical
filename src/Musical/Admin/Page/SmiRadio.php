<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use diCore\Helper\ArrayHelper;
use diCore\Tool\CollectionCache;
use Musical\Entity\Season\Collection as Seasons;
use Musical\Entity\SmiRadio\Model;

class SmiRadio extends BaseSmi
{
    protected function initTable()
    {
        $this->setTable('smi_radio');
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            '#href' => [],
            'season_id' => [
                'headAttrs' => [
                    'width' => '10%',
                ],
                'value' => function (Model $m) {
                    return $m->getSeason()->getTitle() ?: '&ndash;';
                },
            ],
            'title' => [
                'headAttrs' => [
                    'width' => '25%',
                ],
            ],
            'source' => [
                'headAttrs' => [
                    'width' => '15%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '25%',
                ],
                'title' => 'Название (Eng)',
            ],
            'en_source' => [
                'headAttrs' => [
                    'width' => '15%',
                ],
                'title' => 'Источник (Eng)',
            ],
            'date' => [
                'value' => function(Model $m) {
                    return \diDateTime::simpleFormat($m->getDate());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
                'bodyAttrs' => [
                    'class' => 'dt',
                ],
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    public function renderForm()
    {
        $this->getForm()
            ->setSelectFromArrayInput('vendor', ArrayHelper::removeByValue(\diAudioVendors::$titles, \diAudioVendors::Own))
            ->setSelectFromCollectionInput('season_id', CollectionCache::get(Seasons::type));
    }

    public function submitForm()
    {
        parent::submitForm();

        /** @var Model $m */
        $m = $this->getSubmit()->getModel();

        if ($m->getVendor() != \diAudioVendors::Own) {
            $audioInfo = \diAudioVendors::extractInfoFromEmbed(stripslashes($m->getEmbed()));

            if ($audioInfo['audio_uid']) {
                $m->setVendorAudioUid($audioInfo['audio_uid']);
            }

            if ($audioInfo['vendor']) {
                $m->setVendor($audioInfo['vendor']);
            }
        }
    }

    public function getFormTabs()
    {
        return ArrayHelper::addItemsToAssocArrayAfterKey(parent::getFormTabs(), 'en', [
            'audio' => 'Аудио',
        ]);
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        return extend($ar, [
            'vendor' => [
                'type' => 'int',
                'title' => 'Аудио-хостинг',
                'default' => \diAudioVendors::MixCloud,
                'tab' => 'audio',
            ],

            'embed' => [
                'type' => 'text',
                'title' => 'EMBED-код аудио',
                'default' => '',
                'tab' => 'audio',
            ],

            'vendor_audio_uid' => [
                'type' => 'string',
                'title' => 'UID аудио',
                'default' => '',
                'tab' => 'audio',
            ],

            'source' => [
                'type' => 'string',
                'title' => 'Источник',
                'default' => '',
                'tab' => 'ru',
            ],

            'en_source' => [
                'type' => 'string',
                'title' => 'Источник',
                'default' => '',
                'tab' => 'en',
            ],

            'views_count' => [
                'type' => 'int',
                'title' => 'Количество прослушиваний',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],

            'date' => [
                'type' => 'datetime_str',
                'title' => 'Дата эфира',
                'default' => '',
                //'flags' => ['hidden'],
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],
        ]);
    }

    public function getLocalFields()
    {
        return [
            'slug' => [
                'type' => 'string',
                'default' => '',
            ],

            'en_slug' => [
                'type' => 'string',
                'default' => '',
            ],

            'pic_t' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic_h' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic_tn_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic_tn_h' => [
                'type' => 'int',
                'default' => '',
            ],

            'order_num' => [
                'type' => 'order_num',
                'default' => '',
                'direction' => 1,
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Радиосюжеты';
    }
}
