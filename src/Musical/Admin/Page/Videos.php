<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:34
 */

namespace Musical\Admin\Page;

use diCore\Admin\Base;
use diCore\Admin\FilterRule;
use diCore\Entity\Video\Vendor;
use diCore\Helper\ArrayHelper;
use diCore\Tool\CollectionCache;
use Musical\Admin\AdminFormHelper;
use Musical\Data\Video\Purpose;
use Musical\Entity\Season\Collection as Seasons;
use Musical\Entity\Video\Model;

class Videos extends \diCore\Admin\Page\Videos
{
    protected $listMode = self::LIST_LIST;

    public function __construct(Base $X)
    {
        parent::__construct($X);

        CollectionCache::add([
            Seasons::create()->orderByOrderNum(),
        ]);
    }

    protected function setupFilters()
    {
        $this->getFilters()
            ->addFilter([
                'field' => 'title',
                'type' => 'string',
                'rule' => FilterRule::contains,
            ]);

        if ($this->purposeNeeded()) {
            $this->getFilters()
                ->addFilter([
                    'field' => 'purpose',
                    'type' => 'int',
                    'where_tpl' => 'diaf_minus_one',
                ]);
        }

        $this->getFilters()
            ->buildQuery()
            ->setSelectFromArrayInput('purpose',
                extend(
                    [
                        -1 => Purpose::title(0),
                    ],
                    Purpose::$titles
                ),
                [
                    0 => $this->getLanguage() == 'ru' ? 'Все' : 'All',
                ]
            );
    }

    protected function renderListTable()
    {
        parent::renderListTable();

        $this->getList()
            ->insertColumnsAfter('#visible', [
                '#en_visible' => '',
                //'#top' => '',
            ])
            ->insertColumnsAfter('title', [
                'en_title' => [
                    'headAttrs' => [
                        'width' => '45%',
                    ],
                    'title' => 'Название (Eng)',
                ],
            ])
            ->setColumnAttr('title', [
                'headAttrs' => [
                    'width' => '45%',
                ],
            ]);

        if ($this->purposeNeeded()) {
            $this->getList()
                ->insertColumnsAfter('date', [
                    'purpose' => [
                        'bodyAttrs' => [
                            'class' => 'lite',
                        ],
                        'value' => function(Model $m) {
                            return Purpose::name($m->getPurpose());
                        },
                    ],
                ]);
        }
    }

    protected function purposeNeeded()
    {
        return $this->getTable() == 'videos';
    }

    public function renderForm()
    {
        parent::renderForm();

        $vendors = Vendor::$titles;
        unset($vendors[Vendor::Own]);

        $this->getForm()
            ->setSelectFromArrayInput('vendor', $vendors)
            ->setSelectFromCollectionInput('season_id', CollectionCache::get(Seasons::type));

        if ($this->purposeNeeded()) {
            $this->getForm()
                ->setSelectFromArrayInput('purpose', Purpose::$titles);
        }
    }

    public function submitForm()
    {
        parent::submitForm();

        /** @var Model $m */
        $m = $this->getSubmit()->getModel();

        $this->getSubmit()
            ->makeSlug()
            ->makeSlug($m->getEnSlugSource() ?: $m->getEnTitle() ?: $m->getTitle(), 'en_slug');
    }

    public function getFormTabs()
    {
        return extend(parent::getFormTabs(), AdminFormHelper::getMainRuEnTabs());
    }

    public function getFormFields()
    {
        $ar = self::setFieldsHidden(parent::getFormFields(), [
            'video_mp4',
            'video_m4v',
            'video_ogv',
            'video_webm',
            //'pic',
            'comments_enabled',
            'comments_count',
            'comments_last_date',
            'token',
        ]);

        $ar = self::setFieldsEditable($ar, ['date']);

        if ($this->purposeNeeded()) {
            $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'album_id', [
                'purpose' => [
                    'type' => 'int',
                    'title' => 'Назначение',
                    'default' => 0,
                ],
            ]);
        }

        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'date', [
            'season_id' => [
                'type' => 'int',
                'title' => 'Сезон',
                'default' => '',
            ],

            'meta_title' => [
                'type' => 'string',
                'title' => 'Meta-заголовок',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_keywords' => [
                'type' => 'string',
                'title' => 'Meta-ключевые слова',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_description' => [
                'type' => 'string',
                'title' => 'Meta-описание',
                'default' => '',
                'tab' => 'meta',
            ],

            'en_meta_title' => [
                'type' => 'string',
                'title' => 'Meta-заголовок',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_keywords' => [
                'type' => 'string',
                'title' => 'Meta-ключевые слова',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_description' => [
                'type' => 'string',
                'title' => 'Meta-описание',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_title' => [
                'type' => 'string',
                'title' => 'Название',
                'default' => '',
                'tab' => 'en',
            ],

            'en_content' => [
                'type' => 'wysiwyg',
                'title' => 'Описание',
                'default' => '',
                'tab' => 'en',
            ],

            'en_visible' => [
                'type' => 'checkbox',
                'title' => 'Отображать на сайте',
                'default' => 1,
                'tab' => 'en',
            ],
        ]);

        $ar['title']['tab'] =
        $ar['content']['tab'] =
        $ar['visible']['tab'] =
            'ru';

        return $ar;
    }

    public function getLocalFields()
    {
        return extend(parent::getLocalFields(), [
            'en_slug' => [
                'type' => 'string',
                'default' => '',
            ],
        ]);
    }

    public function useEditLog()
    {
        return true;
    }
}
