<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use Musical\Entity\Price\Model;

class Price extends \diCore\Admin\BasePage
{
    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'id',
                'dir' => 'DESC',
            ],
        ],
    ];

    protected function initTable()
    {
        $this->setTable('price');
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            'date_type' => [
                'headAttrs' => [
                    'width' => '100%',
                ],
                'value' => function (Model $m) {
                    return $m->getDateTypeStr();
                },
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
        ]);
    }

    public function renderForm()
    {
        $this->getForm()
            ->setTwigInput('prices', 'admin/price/prices', [
                'model' => $this->getCurrentModel(),
                'columns' => Model::$layouts[Model::LAYOUT_DEFAULT]['columns'],
                'rows' => Model::$layouts[Model::LAYOUT_DEFAULT]['rows'],
            ])
            ->setSelectFromArrayInput('date_type', Model::$dateTypes);
    }

    protected function beforeSubmitForm()
    {
        if (isset($_POST['prices']) && is_array($_POST['prices'])) {
            foreach ($_POST['prices'] as $k1 => $ar1) {
                foreach ($_POST['prices'][$k1] as $k2 => $v) {
                    $_POST['prices'][$k1][$k2] = (int)$v;
                }
            }

            $_POST['prices'] = json_encode($_POST['prices']);
        }

        return parent::beforeSubmitForm();
    }

    public function submitForm()
    {
    }

    public function getFormTabs()
    {
        return [];
    }

    public function getFormFields()
    {
        return [
            'date_type' => [
                'type' => 'int',
                'title' => 'Тип цен',
                'default' => '',
                'flags' => ['static'],
            ],

            'layout_id' => [
                'type' => 'int',
                'title' => 'Распоясовка зала',
                'default' => '',
                'flags' => ['hidden'],
            ],

            'date' => [
                'type' => 'datetime_str',
                'title' => 'Привязка к дате',
                'default' => '',
                'flags' => ['untouchable', 'hidden'],
            ],

            'prices' => [
                'type' => 'string',
                'title' => 'Цены',
                'default' => '',
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],
        ];
    }

    public function getLocalFields()
    {
        return [];
    }

    public function getModuleCaption()
    {
        return 'Цены';
    }

    public function useEditLog()
    {
        return true;
    }
}
