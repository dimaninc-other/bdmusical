<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use diCore\Helper\ArrayHelper;
use Musical\Admin\AdminFormHelper;
use Musical\Entity\Content\Model;

class Content extends \diCore\Admin\Page\Content
{
    const MAX_LEVEL_NUM = 2;

    public function renderList()
    {
        parent::renderList();

        $this->getList()
            ->setColumnWidth('title', '40%')
            ->insertColumnsAfter('title', [
                'en_title' => [
                    'title' => 'Название (Eng)',
                    'headAttrs' => [
                        'width' => '40%',
                    ],
                ],
            ]);
    }

    protected function getButtonsArForList()
    {
        $ar = parent::getButtonsArForList();

        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, '#visible', [
            '#en_visible' => '',
            '#to_show_content' => [
                'active' => function (Model $m) {
                    return $m->getLevelNum() == 0;
                },
            ],
        ]);

        return $ar;
    }

    public function getFormTabs()
    {
        return AdminFormHelper::getMainRuEnTabs();
    }

    public function renderForm()
    {
        parent::renderForm();

        $this->injectJs('Content');
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar['caption']['flags'] = [];
        $ar['caption']['tab'] = 'ru';
        $ar = self::setFieldsVisible($ar, ['short_content', 'ico']);

        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'content', [
            'en_html_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_html_keywords' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_html_description' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en',
            ],

            'en_caption' => [
                'type' => 'string',
                'title' => $this->localized([
                    'ru' => 'Заголовок для меню',
                    'en' => 'Menu caption',
                ]),
                'default' => '',
                'tab' => 'en',
            ],

            'en_short_content' => [
                'type' => 'wysiwyg',
                'default' => '',
                'flags' => 'hidden',
                'tab' => 'en',
            ],

            'en_content' => [
                'type' => 'wysiwyg',
                'default' => '',
                'tab' => 'en',
            ],

            /*
            'en_slug_source' => [
                'type' => 'string',
                'title' => 'Название для URL',
                'default' => '',
                'flags' => ['hidden'],
                'tab' => 'en',
            ],
            */
        ]);

        $ar['title']['tab'] =
        $ar['short_content']['tab'] =
        $ar['content']['tab'] =
            //$ar['slug_source']['tab'] =
            'ru';

        return $ar;
    }

    public function useEditLog()
    {
        return true;
    }
}
