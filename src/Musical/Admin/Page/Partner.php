<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use diCore\Admin\Submit;
use Musical\Entity\Partner\Category;
use Musical\Entity\Partner\Model;

class Partner extends \diCore\Admin\BasePage
{
    protected $options = [
        'filters' => [
            'defaultSorter' => [
                'sortBy' => 'order_num',
                'dir' => 'ASC',
            ],
        ],
    ];

    protected function initTable()
    {
        $this->setTable('partner');
    }

    protected function setupFilters()
    {
        $this->getFilters()
            ->addFilter([
                'field' => 'category_id',
                'type' => 'int',
                'title' => 'Категория',
                'strict' => true,
            ])
            ->buildQuery()
            ->setSelectFromArrayInput('category_id', Category::getLocalizedTitles());
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            'pic' => [
                'bodyAttrs' => [
                    'class' => 'no-padding',
                ],
                'value' => function (Model $model) {
                    return $model->hasPic()
                        ? "<img src=\"/{$model->wrapFileWithPath($model->getPic())}\" width=\"200\">"
                        : '';
                },
            ],
            'title' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'href' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'created_at' => [
                'value' => function(Model $m) {
                    return \diDateTime::simpleFormat($m->getCreatedAt());
                },
                'headAttrs' => [
                    'width' => '10%',
                ],
                'bodyAttrs' => [
                    'class' => 'dt',
                ],
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    public function renderForm()
    {
        $this->getForm()
            ->setSelectFromArrayInput('category_id', Category::getLocalizedTitles());
    }

    public function submitForm()
    {
        $this->getSubmit()
            ->storeImage(['pic'], [
                [
                    'type' => Submit::IMAGE_TYPE_MAIN,
                ],
            ]);
    }

    public function getFormTabs()
    {
        return [];
    }

    public function getFormFields()
    {
        return [
            'title' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'en_title' => [
                'type' => 'string',
                'title' => 'Название (Eng)',
                'default' => '',
            ],

            'href' => [
                'type' => 'string',
                'title' => '',
                'default' => '',
            ],

            'category_id' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic' => [
                'type' => 'pic',
                'title' => 'Логотип',
                'default' => '',
            ],

            'created_at' => [
                'type' => 'datetime_str',
                'default' => '',
                'flags' => ['static', 'untouchable', 'initially_hidden'],
            ],
        ];
    }

    public function getLocalFields()
    {
        return [
            'pic_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'pic_h' => [
                'type' => 'int',
                'default' => '',
            ],

            'order_num' => [
                'type' => 'order_num',
                'default' => '',
                'direction' => 1,
            ],
        ];
    }

    public function getModuleCaption()
    {
        return 'Партнеры';
    }

    public function useEditLog()
    {
        return true;
    }
}
