<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use Musical\Entity\Creative\Model;

class Creative extends BaseTroupe
{
    protected function initTable()
    {
        $this->setTable('creative');
    }

    public function renderList()
    {
        $this->getList()->addColumns([
            'id' => 'ID',
            '#href' => [],
            'pic' => $this->getPicListEntity(),
            'title' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'position' => [
                'headAttrs' => [
                    'width' => '20%',
                ],
            ],
            'en_title' => [
                'headAttrs' => [
                    'width' => '30%',
                ],
            ],
            'en_position' => [
                'headAttrs' => [
                    'width' => '20%',
                ],
            ],
            '#edit' => '',
            '#del' => '',
            '#visible' => '',
            '#en_visible' => '',
            '#up' => '',
            '#down' => '',
        ]);
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar = self::setFieldsHidden($ar, ['pic2']);

        $ar['title']['title'] = 'ФИО';
        $ar['en_title']['title'] = 'ФИО';

        return $ar;
    }

    public function getModuleCaption()
    {
        return 'Творческая группа';
    }
}
