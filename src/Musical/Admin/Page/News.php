<?php
/**
 * Created by \diAdminPagesManager
 * Date: 12.04.2021
 * Time: 18:33
 */

namespace Musical\Admin\Page;

use diCore\Admin\Base;
use diCore\Admin\Submit;
use diCore\Helper\ArrayHelper;
use diCore\Tool\CollectionCache;
use Musical\Admin\AdminFormHelper;
use Musical\Entity\News\Model;
use Musical\Entity\Season\Collection as Seasons;

class News extends \diCore\Admin\Page\News
{
    protected $slugFieldName = 'slug';
    protected $slugSourceFieldName = 'slug_source';

    public function __construct(Base $X)
    {
        parent::__construct($X);

        CollectionCache::add([
            Seasons::create()->orderByOrderNum(),
        ]);
    }

    protected function setupFilters()
    {
        parent::setupFilters();

        $this->getFilters()
            ->addFilter([
                'field' => 'season_id',
                'type' => 'int',
                'title' => 'Сезон',
            ])
            ->buildQuery()
            ->setSelectFromCollectionInput(
                'season_id',
                CollectionCache::get(Seasons::type),
                [0 => 'Все сезоны']
            );
    }

    public function renderList()
    {
        parent::renderList();

        $this->getList()
            ->insertColumnsAfter('#visible', [
                '#en_visible' => [],
                '#top' => [],
                //'#en_top' => [],
            ])
            ->insertColumnsAfter('date', [
                'season_id' => [
                    'headAttrs' => [
                        'width' => '10%',
                    ],
                    'bodyAttrs' => [
                        'class' => 'lite',
                    ],
                    'value' => function (Model $m) {
                        return $m->getSeason()->getTitle();
                    },
                ],
                'pic' => [
                    'bodyAttrs' => [
                        'class' => 'no-padding',
                    ],
                    'value' => function (Model $model) {
                        $src = $model->hasPic()
                            ? '/' . $model->wrapFileWithPath($model->getPic(), 1)
                            : '';

                        return $src
                            ? "<img src=\"$src\" style=\"max-width: 200px; max-height: 200px\" alt=\"\">"
                            : '';
                    },
                ]
            ])
            ->insertColumnsAfter('title', [
                'en_title' => [
                    'title' => 'Заголовок (Eng)',
                    'headAttrs' => [
                        'width' => '40%',
                    ],
                ],
            ])
            ->setColumnAttr('title', [
                'headAttrs' => [
                    'width' => '40%',
                ],
            ]);
    }

    public function getFormTabs()
    {
        return AdminFormHelper::getMainRuEnTabs();
    }

    public function renderForm()
    {
        parent::renderForm();

        $this->getForm()
            ->setSelectFromCollectionInput('season_id', CollectionCache::get(Seasons::type));
    }

    public function submitForm()
    {
        parent::submitForm();

        $this->getSubmit()
            ->storeImage('en_pic');
    }

    public function getFormFields()
    {
        $ar = parent::getFormFields();

        $ar = self::setFieldsHidden($ar, ['short_content']); //, 'pic'
        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'date', [
            'season_id' => [
                'type' => 'int',
                'title' => 'Сезон',
                'default' => '',
            ],
        ]);

        unset($ar['html_title'], $ar['html_keywords'], $ar['html_description']);

        $ar = ArrayHelper::addItemsToAssocArrayAfterKey($ar, 'content', [
            'visible' => [
                'type' => 'checkbox',
                //'title' => 'Видна на сайте',
                'default' => 1,
                'tab' => 'ru',
            ],

            'top' => [
                'type' => 'checkbox',
                'title' => 'Показывать на главной',
                'default' => 1,
                'tab' => 'ru',
            ],

            'meta_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_keywords' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'meta_description' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'meta',
            ],

            'en_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en',
            ],

            'en_short_content' => [
                'type' => 'text',
                'default' => '',
                'flags' => ['hidden'],
                'tab' => 'en',
            ],

            'en_content' => [
                'type' => 'wysiwyg',
                'default' => '',
                'tab' => 'en',
            ],

            'en_visible' => [
                'type' => 'checkbox',
                //'title' => 'Видна на сайте (Eng)',
                'default' => 1,
                'tab' => 'en',
            ],

            'en_top' => [
                'type' => 'checkbox',
                'title' => 'Показывать на главной',
                'default' => 1,
                'tab' => 'en',
            ],

            'en_pic' => [
                'type' => 'pic',
                'title' => 'Фото для англ.версии',
                'naming' => Submit::FILE_NAMING_ORIGINAL,
                'default' => '',
                'tab' => 'en',
            ],

            'en_meta_title' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_keywords' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],

            'en_meta_description' => [
                'type' => 'string',
                'default' => '',
                'tab' => 'en_meta',
            ],
        ]);

        $ar['title']['title'] = '';

        $ar['title']['tab'] =
        $ar['short_content']['tab'] =
        $ar['content']['tab'] =
        //$ar['pic']['tab'] =
            'ru';

        $ar['pic']['naming'] = Submit::FILE_NAMING_ORIGINAL;

        return $ar;
    }

    public function getLocalFields()
    {
        $ar = parent::getLocalFields();

        $ar = extend($ar, [
            'en_pic_t' => [
                'type' => 'int',
                'default' => '',
            ],

            'en_pic_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'en_pic_h' => [
                'type' => 'int',
                'default' => '',
            ],

            'en_pic_tn_w' => [
                'type' => 'int',
                'default' => '',
            ],

            'en_pic_tn_h' => [
                'type' => 'int',
                'default' => '',
            ],
        ]);

        return $ar;
    }

    public function useEditLog()
    {
        return true;
    }
}
