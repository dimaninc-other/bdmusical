<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 13.04.2021
 * Time: 19:33
 */

namespace Musical\Helper;

use diCore\Data\Config;
use diCore\Data\Configuration;

class ConfigurationHelper
{
    public static function create()
    {
        $cfg = new Configuration();
        $cfg->setTabsAr(ConfigurationHelper::getTabs())->setInitialData(extend(
            ConfigurationHelper::basic(),
            ConfigurationHelper::counts(),
            ConfigurationHelper::pics(),
            ConfigurationHelper::groups(),
            ConfigurationHelper::socials(),
            ConfigurationHelper::contacts(),
            ConfigurationHelper::counters()
        ))->loadCache();

        return $cfg;
    }

    public static function getTabs()
    {
        return [
            'general' => 'Основное',
            'groups' => 'Групп.заявки',
            'pics' => 'Картинки',
            'counts' => 'Отображение',
            'socials' => 'Соц.сети',
            'counters' => 'Счётчики',
        ];
    }

    public static function basic()
    {
        return [
            'phone_main' => [
                'title' => 'Телефон в шапке',
                'type' => 'string',
                'value' => '+7 (495) 925-50-50',
                'tab' => 'texts',
            ],

            'sender_email' => [
                'title' => 'E-mail для исходящих писем',
                'type' => 'string',
                'value' => 'noreply@' . Config::getMainDomain(),
                'tab' => 'general',
            ],

            'auto_save_timeout' => [
                'title' => 'Период автосохранения, сек',
                'type' => 'int',
                'value' => 0,
                'notes' => [
                    'Автосохранение работает в каждой форме Админки. Если 0, то автосохранение отключено',
                ],
            ],

            'open_graph_default_pic' => [
                'title' => 'Картинка Open graph по умолчанию',
                'type' => 'pic',
                'value' => '',
                'tab' => 'pics',
            ],
        ];
    }

    public static function pics()
    {
        return  [
            'photos_width' => [
                'title' => 'Ширина фотографий',
                'type' => 'int',
                'value' => 1920,
                'tab' => 'pics',
            ],

            'photos_height' => [
                'title' => 'Высота фотографий',
                'type' => 'int',
                'value' => 1080,
                'tab' => 'pics',
            ],

            'photos_tn_width' => [
                'title' => 'Ширина превью фотографий',
                'type' => 'int',
                'value' => 500,
                'tab' => 'pics',
            ],

            'photos_tn_height' => [
                'title' => 'Высота превью фотографий',
                'type' => 'int',
                'value' => 280,
                'tab' => 'pics',
            ],

            'news_width' => [
                'title' => 'Ширина фотографий новостей',
                'type' => 'int',
                'value' => 1920,
                'tab' => 'pics',
            ],

            'news_height' => [
                'title' => 'Высота фотографий новостей',
                'type' => 'int',
                'value' => 1200,
                'tab' => 'pics',
            ],

            'news_tn_width' => [
                'title' => 'Ширина превью фотографий новостей',
                'type' => 'int',
                'value' => 500,
                'tab' => 'pics',
            ],

            'news_tn_height' => [
                'title' => 'Высота превью фотографий новостей',
                'type' => 'int',
                'value' => 280,
                'tab' => 'pics',
            ],

            'videos_width' => [
                'title' => 'Ширина превью видео',
                'type' => 'int',
                'value' => 640,
                'tab' => 'pics',
            ],

            'videos_height' => [
                'title' => 'Высота превью видео',
                'type' => 'int',
                'value' => 360,
                'tab' => 'pics',
            ],

            'smi_press_width' => [
                'title' => 'Ширина фотографий СМИ: Пресса',
                'type' => 'int',
                'value' => 1920,
                'tab' => 'pics',
            ],

            'smi_press_height' => [
                'title' => 'Высота фотографий СМИ: Пресса',
                'type' => 'int',
                'value' => 1080,
                'tab' => 'pics',
            ],

            'smi_press_tn_width' => [
                'title' => 'Ширина превью фотографий СМИ: Пресса',
                'type' => 'int',
                'value' => 500,
                'tab' => 'pics',
            ],

            'smi_press_tn_height' => [
                'title' => 'Высота превью фотографий СМИ: Пресса',
                'type' => 'int',
                'value' => 280,
                'tab' => 'pics',
            ],

            'smi_radio_width' => [
                'title' => 'Ширина фотографий СМИ: Радио',
                'type' => 'int',
                'value' => 1920,
                'tab' => 'pics',
            ],

            'smi_radio_height' => [
                'title' => 'Высота фотографий СМИ: Радио',
                'type' => 'int',
                'value' => 1080,
                'tab' => 'pics',
            ],

            'smi_radio_tn_width' => [
                'title' => 'Ширина превью фотографий СМИ: Радио',
                'type' => 'int',
                'value' => 500,
                'tab' => 'pics',
            ],

            'smi_radio_tn_height' => [
                'title' => 'Высота превью фотографий СМИ: Радио',
                'type' => 'int',
                'value' => 280,
                'tab' => 'pics',
            ],

            'review_tn_width' => [
                'title' => 'Ширина превью отзыва',
                'type' => 'int',
                'value' => 500,
                'tab' => 'pics',
            ],

            'review_tn_height' => [
                'title' => 'Высота превью отзыва',
                'type' => 'int',
                'value' => 280,
                'tab' => 'pics',
            ],

            'solo_width' => [
                'title' => 'Ширина фотографий солистов',
                'type' => 'int',
                'value' => 1200,
                'tab' => 'pics',
            ],

            'solo_height' => [
                'title' => 'Высота фотографий солистов',
                'type' => 'int',
                'value' => 1200,
                'tab' => 'pics',
            ],

            'solo_tn_width' => [
                'title' => 'Ширина превью фотографий солистов',
                'type' => 'int',
                'value' => 370,
                'tab' => 'pics',
            ],

            'solo_tn_height' => [
                'title' => 'Высота превью фотографий солистов',
                'type' => 'int',
                'value' => 260,
                'tab' => 'pics',
            ],

            'creator_width' => [
                'title' => 'Ширина фотографий создателей',
                'type' => 'int',
                'value' => 1200,
                'tab' => 'pics',
            ],

            'creator_height' => [
                'title' => 'Высота фотографий создателей',
                'type' => 'int',
                'value' => 1200,
                'tab' => 'pics',
            ],

            'creator_tn_width' => [
                'title' => 'Ширина превью фотографий создателей',
                'type' => 'int',
                'value' => 370,
                'tab' => 'pics',
            ],

            'creator_tn_height' => [
                'title' => 'Высота превью фотографий создателей',
                'type' => 'int',
                'value' => 260,
                'tab' => 'pics',
            ],

            'creative_width' => [
                'title' => 'Ширина фотографий творч.группы',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'creative_height' => [
                'title' => 'Высота фотографий творч.группы',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'creative_tn_width' => [
                'title' => 'Ширина превью фотографий творч.группы',
                'type' => 'int',
                'value' => 370,
                'tab' => 'pics',
            ],

            'creative_tn_height' => [
                'title' => 'Высота превью фотографий творч.группы',
                'type' => 'int',
                'value' => 260,
                'tab' => 'pics',
            ],

            'ballet_width' => [
                'title' => 'Ширина фотографий балета',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'ballet_height' => [
                'title' => 'Высота фотографий балета',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'ballet_tn_width' => [
                'title' => 'Ширина превью фотографий балета',
                'type' => 'int',
                'value' => 370,
                'tab' => 'pics',
            ],

            'ballet_tn_height' => [
                'title' => 'Высота превью фотографий балета',
                'type' => 'int',
                'value' => 260,
                'tab' => 'pics',
            ],

            'skater_width' => [
                'title' => 'Ширина фотографий фигуриста',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'skater_height' => [
                'title' => 'Высота фотографий фигуриста',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'skater_tn_width' => [
                'title' => 'Ширина превью фотографий фигуриста',
                'type' => 'int',
                'value' => 370,
                'tab' => 'pics',
            ],

            'skater_tn_height' => [
                'title' => 'Высота превью фотографий фигуриста',
                'type' => 'int',
                'value' => 260,
                'tab' => 'pics',
            ],

            'dancer_width' => [
                'title' => 'Ширина фотографий ансамбля',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'dancer_height' => [
                'title' => 'Высота фотографий ансамбля',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'dancer_tn_width' => [
                'title' => 'Ширина превью фотографий ансамбля',
                'type' => 'int',
                'value' => 370,
                'tab' => 'pics',
            ],

            'dancer_tn_height' => [
                'title' => 'Высота превью фотографий ансамбля',
                'type' => 'int',
                'value' => 260,
                'tab' => 'pics',
            ],

            'musician_width' => [
                'title' => 'Ширина фотографий оркестра',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'musician_height' => [
                'title' => 'Высота фотографий оркестра',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'musician_tn_width' => [
                'title' => 'Ширина превью фотографий оркестра',
                'type' => 'int',
                'value' => 370,
                'tab' => 'pics',
            ],

            'musician_tn_height' => [
                'title' => 'Высота превью фотографий оркестра',
                'type' => 'int',
                'value' => 260,
                'tab' => 'pics',
            ],

            'action_width' => [
                'title' => 'Ширина фотографий акций',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'action_height' => [
                'title' => 'Высота фотографий акций',
                'type' => 'int',
                'value' => 1000,
                'tab' => 'pics',
            ],

            'action_tn_width' => [
                'title' => 'Ширина превью фотографий акций',
                'type' => 'int',
                'value' => 370,
                'tab' => 'pics',
            ],

            'action_tn_height' => [
                'title' => 'Высота превью фотографий акций',
                'type' => 'int',
                'value' => 260,
                'tab' => 'pics',
            ],
        ];
    }

    public static function groups()
    {
        return [
            'groups_presentation_show' => [
                'title' => 'Показывать презентацию в групповых заявках',
                'type' => 'checkbox',
                'value' => true,
                'tab' => 'groups',
            ],

            'groups_presentation' => [
                'title' => 'Презентация в групповых заявках (Рус)',
                'type' => 'file',
                'value' => '',
                'tab' => 'groups',
            ],

            'en_groups_presentation' => [
                'title' => 'Презентация в групповых заявках (Eng)',
                'type' => 'file',
                'value' => '',
                'tab' => 'groups',
            ],
        ];
    }

    public static function contacts()
    {
        return [
            'contacts_block_01_title' => [
                'title' => 'Отдел №1: название',
                'type' => 'string',
                'value' => 'Групповые заказы',
                'tab' => 'contacts',
            ],

            'contacts_block_01_phone' => [
                'title' => 'Отдел №1: телефон',
                'type' => 'string',
                'value' => '+7 (495) 646-74-20',
                'tab' => 'contacts',
            ],

            'contacts_block_01_name_01' => [
                'title' => 'Отдел №1: ФИО сотрудника',
                'type' => 'string',
                'value' => 'Кристина Скорохватова',
                'tab' => 'contacts',
            ],

            'contacts_block_01_email_01' => [
                'title' => 'Отдел №1: E-mail сотрудника',
                'type' => 'string',
                'value' => 'kristina@karenina-musical.ru',
                'tab' => 'contacts',
            ],

            'contacts_block_01_name_02' => [
                'title' => 'Отдел №1: ФИО сотрудника',
                'type' => 'string',
                'value' => 'Ольга Епифанцева',
                'tab' => 'contacts',
            ],

            'contacts_block_01_email_02' => [
                'title' => 'Отдел №1: E-mail сотрудника',
                'type' => 'string',
                'value' => 'epifantseva@karenina-musical.ru',
                'tab' => 'contacts',
            ],

            'contacts_block_02_title' => [
                'title' => 'Отдел №2: название',
                'type' => 'string',
                'value' => 'PR-служба',
                'tab' => 'contacts',
            ],

            'contacts_block_02_phone' => [
                'title' => 'Отдел №2: телефон',
                'type' => 'string',
                'value' => '+7 (495) 646-74-20',
                'tab' => 'contacts',
            ],

            'contacts_block_02_name_01' => [
                'title' => 'Отдел №2: ФИО сотрудника',
                'type' => 'string',
                'value' => 'Кристина Скорохватова',
                'tab' => 'contacts',
            ],

            'contacts_block_02_email_01' => [
                'title' => 'Отдел №2: E-mail сотрудника',
                'type' => 'string',
                'value' => 'kristina@karenina-musical.ru',
                'tab' => 'contacts',
            ],

            'contacts_block_02_name_02' => [
                'title' => 'Отдел №2: ФИО сотрудника',
                'type' => 'string',
                'value' => 'Ольга Епифанцева',
                'tab' => 'contacts',
            ],

            'contacts_block_02_email_02' => [
                'title' => 'Отдел №2: E-mail сотрудника',
                'type' => 'string',
                'value' => 'epifantseva@karenina-musical.ru',
                'tab' => 'contacts',
            ],

            'contacts_block_03_title' => [
                'title' => 'Отдел №3: название',
                'type' => 'string',
                'value' => 'Сотрудничество и партнерство',
                'tab' => 'contacts',
            ],

            'contacts_block_03_phone' => [
                'title' => 'Отдел №3: телефон',
                'type' => 'string',
                'value' => '+7 (495) 646-74-20',
                'tab' => 'contacts',
            ],

            'contacts_block_03_name_01' => [
                'title' => 'Отдел №3: ФИО сотрудника',
                'type' => 'string',
                'value' => '',
                'tab' => 'contacts',
            ],

            'contacts_block_03_email_01' => [
                'title' => 'Отдел №3: E-mail сотрудника',
                'type' => 'string',
                'value' => 'kristina@karenina-musical.ru',
                'tab' => 'contacts',
            ],

            'contacts_block_03_name_02' => [
                'title' => 'Отдел №3: ФИО сотрудника',
                'type' => 'string',
                'value' => '',
                'tab' => 'contacts',
            ],

            'contacts_block_03_email_02' => [
                'title' => 'Отдел №3: E-mail сотрудника',
                'type' => 'string',
                'value' => '',
                'tab' => 'contacts',
            ],

            'en_contacts_block_01_title' => [
                'title' => 'Отдел №1: название',
                'type' => 'string',
                'value' => 'Групповые заказы',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_01_phone' => [
                'title' => 'Отдел №1: телефон',
                'type' => 'string',
                'value' => '+7 (495) 646-74-20',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_01_name_01' => [
                'title' => 'Отдел №1: ФИО сотрудника',
                'type' => 'string',
                'value' => 'Кристина Скорохватова',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_01_email_01' => [
                'title' => 'Отдел №1: E-mail сотрудника',
                'type' => 'string',
                'value' => 'kristina@karenina-musical.ru',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_01_name_02' => [
                'title' => 'Отдел №1: ФИО сотрудника',
                'type' => 'string',
                'value' => 'Ольга Епифанцева',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_01_email_02' => [
                'title' => 'Отдел №1: E-mail сотрудника',
                'type' => 'string',
                'value' => 'epifantseva@karenina-musical.ru',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_02_title' => [
                'title' => 'Отдел №2: название',
                'type' => 'string',
                'value' => 'PR-служба',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_02_phone' => [
                'title' => 'Отдел №2: телефон',
                'type' => 'string',
                'value' => '+7 (495) 646-74-20',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_02_name_01' => [
                'title' => 'Отдел №2: ФИО сотрудника',
                'type' => 'string',
                'value' => 'Кристина Скорохватова',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_02_email_01' => [
                'title' => 'Отдел №2: E-mail сотрудника',
                'type' => 'string',
                'value' => 'kristina@karenina-musical.ru',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_02_name_02' => [
                'title' => 'Отдел №2: ФИО сотрудника',
                'type' => 'string',
                'value' => 'Ольга Епифанцева',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_02_email_02' => [
                'title' => 'Отдел №2: E-mail сотрудника',
                'type' => 'string',
                'value' => 'epifantseva@karenina-musical.ru',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_03_title' => [
                'title' => 'Отдел №3: название',
                'type' => 'string',
                'value' => 'Сотрудничество и партнерство',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_03_phone' => [
                'title' => 'Отдел №3: телефон',
                'type' => 'string',
                'value' => '+7 (495) 646-74-20',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_03_name_01' => [
                'title' => 'Отдел №3: ФИО сотрудника',
                'type' => 'string',
                'value' => '',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_03_email_01' => [
                'title' => 'Отдел №3: E-mail сотрудника',
                'type' => 'string',
                'value' => 'kristina@karenina-musical.ru',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_03_name_02' => [
                'title' => 'Отдел №3: ФИО сотрудника',
                'type' => 'string',
                'value' => '',
                'tab' => 'en_contacts',
            ],

            'en_contacts_block_03_email_02' => [
                'title' => 'Отдел №3: E-mail сотрудника',
                'type' => 'string',
                'value' => '',
                'tab' => 'en_contacts',
            ],
        ];
    }

    public static function counts()
    {
        return [
            'per_page[news]' => [
                'title' => 'Новостей на странице',
                'type' => 'int',
                'value' => 9,
                'tab' => 'counts',
            ],

            'per_page[media]' => [
                'title' => 'Медиа на странице',
                'type' => 'int',
                'value' => 9,
                'tab' => 'counts',
            ],

            'admin_per_page[admins]' => [
                'title' => 'Админов на странице (в админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],

            'admin_per_page[mail_queue]' => [
                'title' => 'Писем в очереди на странице (в админке)',
                'type' => 'int',
                'value' => 30,
                'tab' => 'counts',
            ],

            'admin_per_page[partner]' => [
                'title' => 'Партнеров на странице (в Админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],

            'admin_per_page[show]' => [
                'title' => 'Спектаклей на странице (в Админке)',
                'type' => 'int',
                'value' => 30,
                'tab' => 'counts',
            ],

            'admin_per_page[photos]' => [
                'title' => 'Фото на странице (в Админке)',
                'type' => 'int',
                'value' => 30,
                'tab' => 'counts',
            ],

            'admin_per_page[videos]' => [
                'title' => 'Видео на странице (в Админке)',
                'type' => 'int',
                'value' => 10,
                'tab' => 'counts',
            ],

            'admin_per_page[news]' => [
                'title' => 'Новостей на странице (в Админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],

            'admin_per_page[smi_press]' => [
                'title' => 'СМИ пресса на странице (в Админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],

            'admin_per_page[smi_radio]' => [
                'title' => 'СМИ радио на странице (в Админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],

            'admin_per_page[smi_tv]' => [
                'title' => 'СМИ ТВ на странице (в Админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],

            'admin_per_page[media]' => [
                'title' => 'Медиа на странице (в Админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],

            'admin_per_page[action]' => [
                'title' => 'Акций на странице (в Админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],

            'admin_per_page[review]' => [
                'title' => 'Отзывов на странице (в Админке)',
                'type' => 'int',
                'value' => 20,
                'tab' => 'counts',
            ],
        ];
    }

    public static function socials()
    {
        return [
            'socials[facebook]' => [
                'title' => 'Facebook',
                'type' => 'string',
                'value' => '',
                'tab' => 'socials',
            ],

            'socials[instagram]' => [
                'title' => 'Instagram',
                'type' => 'string',
                'value' => '',
                'tab' => 'socials',
            ],

            'socials[youtube]' => [
                'title' => 'YouTube',
                'type' => 'string',
                'value' => '',
                'tab' => 'socials',
            ],

            'socials[vk]' => [
                'title' => 'VK',
                'type' => 'string',
                'tab' => 'socials',
            ],

            'socials[tiktok]' => [
                'title' => 'Tik-tok',
                'type' => 'string',
                'tab' => 'socials',
            ],

            'socials[telegram]' => [
                'title' => 'Telegram',
                'type' => 'string',
                'tab' => 'socials',
            ],
        ];
    }

    public static function counters()
    {
        return [
            'counters[metrika_uid]' => [
                'title' => 'UID Яндекс.Метрики',
                'type' => 'string',
                'value' => '',
                'tab' => 'counters',
            ],

            'counters[head_bottom]' => [
                'title' => 'Счётчики внутри тега HEAD',
                'type' => 'text',
                'value' => '',
                'tab' => 'counters',
            ],

            'counters[body_top]' => [
                'title' => 'Счётчики внутри тега BODY (в начале)',
                'type' => 'text',
                'value' => '',
                'tab' => 'counters',
            ],

            'counters[body_bottom]' => [
                'title' => 'Счётчики внутри тега BODY (в конце)',
                'type' => 'text',
                'value' => '',
                'tab' => 'counters',
            ],
        ];
    }
}
