<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 22.06.2021
 * Time: 19:49
 */

namespace Musical\Helper;

class Util
{
    public static function stripUrlPath($url)
    {
        $urlParts = parse_url($url);

        if (!$urlParts) {
            $pattern = '#^([a-zA-Z0-9]+://[^/]+)#';
            preg_match($pattern, $url, $matches);

            return $matches[1] . '/';
        }

        $newUrl = $urlParts['scheme'] . '://' . $urlParts['host'] . '/';

        return $newUrl;
    }
}