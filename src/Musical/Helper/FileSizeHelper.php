<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 28.05.2021
 * Time: 15:38
 */

namespace Musical\Helper;

class FileSizeHelper
{
    public static $sizes = [
        'ru' => [
            'm' => ' М',
            'k' => ' К',
            'b' => ' байт',
        ],
        'en' => [
            'm' => 'Mb',
            'k' => 'kb',
            'b' => ' bytes',
        ],
    ];

    public static function toStr($size, $l)
    {
        return size_in_bytes($size, self::$sizes[$l]['m'], self::$sizes[$l]['k'], self::$sizes[$l]['b']);
    }
}