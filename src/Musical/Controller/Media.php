<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 20.05.2021
 * Time: 17:05
 */

namespace Musical\Controller;

use Musical\Base\CMS;
use Musical\Entity\Media\Collection;

class Media extends \diBaseController
{
    public function rebuildAction()
    {
        Collection::create()->hardDestroy();
        $stat = Collection::rebuild();

        return $stat;
    }

    public function loadMoreAction()
    {
        global $Z;

        $page = \diRequest::get('page', 1);
        $lang = \diRequest::get('lang', CMS::$defaultLanguage);

        $Z = CMS::fastInitiate($lang);

        $col = \Musical\Module\Media::getCol()
            ->setPageSize(\Musical\Module\Media::pageSize())
            ->setPageNumber($page);

        $left = \Musical\Module\Media::getCol()
            ->setSkip(\Musical\Module\Media::pageSize() * $page)
            ->getFirstItem();

        $rows = [];

        foreach ($col as $media) {
            $rows[] = $this->getTwig()->parse('media/row', [
                'media' => $media,
            ]);
        }

        return [
            'rows' => $rows,
            'left' => $left->exists(),
        ];
    }
}
