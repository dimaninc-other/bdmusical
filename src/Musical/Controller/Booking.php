<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 15.04.21
 * Time: 16:25
 */

namespace Musical\Controller;

use diCore\Tool\Logger;
use diCore\Traits\BasicCreate;
use Musical\Data\Config;
use Musical\Entity\Show\Collection as Shows;
use Musical\Entity\Show\Model as Show;

class Booking extends \diBaseController
{
    use BasicCreate;

    const outer_show_id_token = '{{-show-id-}}';
    const widget_id_token = '{{-widget-id-}}';
	const TEST_WIDGET_ID = 0;
	const WIDGET_ID = 0;

	protected static $uri = 'https://mosoperetta.ru/index.php/tools/blocks/dilyaver/calendar_json.php?showid=%d&date=%s';
	protected static $testUri = 'https://test.mosoperetta.ru/index.php/tools/blocks/dilyaver/calendar_json.php?showid=%d&date=%s';

	const widget_uid_token = '{{-uid-}}';
	const widget_uid = '...';
    const widget_uid_en = '...-en';

	protected static $widgetUri = [
	    // prod
	    true => [
            Config::vendor_dilyaver => [
                'ru' => 'https://mosoperetta.ru/bilety/{{-uid-}}/?step=hall&performance=%d',
                'en' => 'https://mosoperetta.ru/bilety/{{-uid-}}/?step=hall&en=1&performance=%d',
            ],
            Config::vendor_profticket => [
                'ru' => 'https://spa.profticket.ru/customer/155/shows/{{-widget-id-}}/?frameContainerId=widget-popup&widgetType=iframe#{{-show-id-}}',
                'en' => 'https://spa.profticket.ru/customer/155/shows/{{-widget-id-}}/?frameContainerId=widget-popup&widgetType=iframe#{{-show-id-}}',
            ],
        ],

        // test
        false => [
            Config::vendor_dilyaver => [
                'ru' => 'https://test.mosoperetta.ru/bilety/{{-uid-}}/?step=hall&performance=%d',
                'en' => 'https://test.mosoperetta.ru/bilety/{{-uid-}}/?step=hall&en=1&performance=%d',
            ],
            Config::vendor_profticket => [
                'ru' => 'https://spa.profticket.ru/customer/155/shows/{{-widget-id-}}/?frameContainerId=widget-popup&widgetType=iframe#{{-show-id-}}',
                'en' => 'https://spa.profticket.ru/customer/155/shows/{{-widget-id-}}/?frameContainerId=widget-popup&widgetType=iframe#{{-show-id-}}',
            ],
        ],
    ];

	public static function getWidgetUri($lang = 'ru')
	{
        /** @var self $class */
	    $class = static::getClass();

	    $base = $class::$widgetUri[!Config::getBookingTestMode()][Config::getBookingVendor()][$lang];
	    $uid = $lang === 'ru'
            ? $class::widget_uid
            : $class::widget_uid_en;

		return str_replace([
		    $class::widget_uid_token,
            $class::widget_id_token,
        ], [
            $uid,
            Config::getBookingTestMode() ? $class::TEST_WIDGET_ID : $class::WIDGET_ID,
        ], $base);
	}

	protected function getUri()
	{
        /** @var self $class */
        $class = static::getClass();

        return sprintf(
            Config::getBookingTestMode() ? $class::$testUri : $class::$uri,
            Config::getBookingTestMode() ? $class::TEST_WIDGET_ID : $class::WIDGET_ID,
			\diDateTime::timestamp()
		);
	}

	public function getAction()
	{
		$data = $this->getData($this->getUri());
		$showsSource = json_decode($data);

		$defaultQr = 1;
        $defaultRevaluation = 0;
        $defaultPushkinCard = 0;

        Logger::getInstance()->log('Fetching data from ' . $this->getUri());
        Logger::getInstance()->variable($showsSource);

		foreach ($showsSource as $show_r) {
			$date = \diDateTime::sqlFormat($show_r->perfdate);

			/** @var Show $show */
			$show = Shows::create()
                ->filterByDate($date)
                ->getFirstItem();

			if (!$show->exists()) {
				$show
					->setStaffStatus(0)
					->setTicketStatus(Show::TICKETS_ON_SALE)
					->setStaffComment('')
					->setDate($date);
			}

			$show
                ->setQr(isset($show_r->isQRCodeOnly) ? (int)$show_r->isQRCodeOnly : $defaultQr)
				->setRevaluation(isset($show_r->isRevaluation) ? (int)$show_r->isRevaluation : $defaultRevaluation)
                ->setPushkinCard(isset($show_r->isPushkinCardPermitted) ? (int)$show_r->isPushkinCardPermitted : $defaultPushkinCard)
				->setWidgetId((int)$show_r->idperformance)
				->save();
		}

		Logger::getInstance()->log('sync w/widget done');

		$this->redirect();
	}

	protected function getData($uri)
	{
        $auth = base64_encode('operetta:uyZq#5^*pUhd');
	    $arrContextOptions = [
            'http' => [
                'header' => 'Authorization: Basic ' . $auth,
            ],
	        'ssl' => [
				'verify_peer' => false,
				'verify_peer_name' => false,
			],
		];
		$context = stream_context_create($arrContextOptions);

		// $this->jsonRemoveUnicodeSequences
		return file_get_contents($uri, false, $context);
	}

	protected function jsonRemoveUnicodeSequences($data)
	{
	    return $data;
        /*
		return preg_replace_callback("/\\\\u([a-f0-9]{4})/", function($r) {
			return iconv('UCS-4LE', 'UTF-8', pack('V', hexdec('U' . $r[1])));
		}, $data);
        */
	}
}
