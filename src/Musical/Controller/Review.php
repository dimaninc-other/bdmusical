<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 15.04.21
 * Time: 12:56
 */

namespace Musical\Controller;

use Musical\Entity\Review\Model;

class Review extends \diBaseController
{
	public function _postAddAction()
	{
        Model::create()
            ->setType(Model::TYPE_GENERAL)
            ->setTitle(\diRequest::request('name', ''))
            ->setEmail(\diRequest::request('email', ''))
            ->setContent(\diRequest::request('content', ''))
            ->setShortContent('')
            ->setVisible(0)
            ->setEnVisible(0)
            ->generateSlug()
            ->calculateAndSetOrderNum(1)
            ->save();

		return [
            'ok' => true,
        ];
	}
}
