<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 15.06.2021
 * Time: 19:01
 */

namespace Musical\Controller;

use Musical\Base\CMS;
use Musical\Entity\News\Collection;

class News extends \diBaseController
{
    public function loadMoreAction()
    {
        global $Z;

        $page = \diRequest::get('page', 1);
        $lang = \diRequest::get('lang', CMS::$defaultLanguage);

        $Z = CMS::fastInitiate($lang);

        $col = \Musical\Module\News::getCol()
            ->setPageNumber($page);

        $left = Collection::create()
            ->filterByLocalizedVisible(1)
            ->setSkip(\Musical\Module\News::pageSize() * $page)
            ->getFirstItem();

        $rows = [];

        foreach ($col as $model) {
            $rows[] = $this->getTwig()->parse('news/row', [
                'n' => $model,
            ]);
        }

        return [
            'rows' => $rows,
            'left' => $left->exists(),
        ];
    }
}
