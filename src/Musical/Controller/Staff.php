<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 15.04.21
 * Time: 11:44
 */

namespace Musical\Controller;

use diCore\Tool\CollectionCache;
use Musical\Entity\Character\Collection as Characters;
use Musical\Entity\Character\Model as Character;
use Musical\Entity\Show\Collection as Shows;
use Musical\Entity\Show\Model as Show;
use Musical\Entity\ShowSoloLink\Collection as Links;
use Musical\Entity\ShowSoloLink\Model as Link;
use Musical\Entity\Solo\Collection as Solos;
use Musical\Entity\Solo\Model as Solo;

class Staff extends \diBaseController
{
	public function getAction()
	{
		$res = [];

		$dt1 = strtotime(\diRequest::get('dt1')) ?: time();
		$dt2 = strtotime(\diRequest::get('dt2')) ?: $dt1;

        $characters = Characters::create();
		$solos = Solos::create();
		$shows = Shows::create()
			->filterByDate(\diDateTime::sqlFormat($dt1), '>=')
			->filterByDate(\diDateTime::sqlFormat($dt2), '<=')
			->orderById('desc');

		/** @var Show $show */
		foreach ($shows as $show) {
			$staff = [];

			$links = $show->isStaffDefined()
                ? Links::create()->filterByShowId($show->getId())
                : [];
			/** @var Link $link */
			foreach ($links as $link) {
				/** @var Solo $solo */
				$solo = $solos[$link->getSoloId()];
				/** @var Character $character */
				$character = $characters[$solo->getCharacterId()];

				$staff[$character->getTitle()] = $solo->getTitle();
			}

			$res[] = [
				'date' => \diDateTime::simpleFormat($show->getDate()),
				'staff' => $staff,
			];
		}

		return $res;
	}

	public function _getSoloAction()
    {
        $res = [];

        $characters = \Musical\Module\Solos::getCharacters();
        CollectionCache::add([
            Solos::create(),
        ]);
        /** @var Character $character */
        foreach ($characters as $character) {
            $res[] = [
                'role_name' => $character->getTitle(),
                'actors' => $character->getSolos()->map(function (Solo $solo) {
                    return [
                        'name' => $solo->getTitle(),
                        'image' => \diPaths::defaultHttp() . '/' . $solo->wrapFileWithPath($solo->getPic()),
                        'description' => strip_tags($solo->getContent() ?: ''),
                        'link' => $solo->getFullHref(),
                    ];
                }),
            ];
        }

        return $res;
    }
}
