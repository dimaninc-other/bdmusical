<?php
/**
 * Created by PhpStorm.
 * User: dimaninc
 * Date: 09.06.2021
 * Time: 16:27
 */

namespace Musical\Controller;

class Db extends \diCore\Controller\Db
{
    const FOLDER_MUSICAL_SQL = 11;

    public static $customFoldersIdsAr = [
        self::FOLDER_MUSICAL_SQL,
    ];

    public static function getFolderById($id)
    {
        if ($id == self::FOLDER_MUSICAL_SQL) {
            return static::getMusicalSqlFolder();
        }

        return parent::getFolderById($id);
    }

    public static function getMusicalSqlFolder()
    {
        return dirname(__FILE__, 4) . '/sql/';
    }
}