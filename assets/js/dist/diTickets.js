"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var diTickets = /*#__PURE__*/function () {
  function diTickets() {
    _classCallCheck(this, diTickets);

    this.showsSelector = [// monte + orlov
    '.calendar table tbody tr td .time .item a', // karenina
    'table.calendar-table tbody tr td .times a'].join(',');
    this.e = {
      $table: $('.calendar table'),
      $pad: $('.list.js-tabs, nav.buy-tabs'),
      $iframe: $('iframe#widget'),
      $iframePopupWrapper: $('.widget-popup-wrapper'),
      $iframePopup: $('.widget-popup-wrapper #widget-popup'),
      $loading: $('.loading'),
      $shows: null
    };
    this.constructor.uid = {
      metrika: $('.body-counters').data('uid-metrika')
    };
    this.refreshShows().setupIFrame().setupCalendar().setupStartPopup();
  }

  _createClass(diTickets, [{
    key: "isMobile",
    value: function isMobile() {
      return $(window).width() < 768;
    }
  }, {
    key: "widgetNeeded",
    value: function widgetNeeded() {
      return !['staff'].includes($(document.body).data('page-type'));
    }
  }, {
    key: "refreshShows",
    value: function refreshShows() {
      this.e.$shows = $(this.showsSelector);
      return this;
    }
  }, {
    key: "setupStartPopup",
    value: function setupStartPopup() {
      if (!this.widgetNeeded()) {
        return;
      }

      var p = parse_uri_params(null, '#', ';', ':'); //console.log('uri params:', p);

      if (typeof p.show !== 'undefined') {
        this.refreshShows();
        this.loadWidget(this.e.$shows.filter('[data-widget-id="{0}"]'.format(p.show)));
      }

      return this;
    }
  }, {
    key: "disableScroll",
    value: function disableScroll() {
      $('html').addClass('no-scrolling');
    }
  }, {
    key: "enableScroll",
    value: function enableScroll() {
      $('html').removeClass('no-scrolling');
    }
  }, {
    key: "loadWidget",
    value: function loadWidget(el) {
      var _this = this;

      el = $(el);

      if ($(document.body).data('page-type') === 'home') {
        window.location.href = el.attr('href');
        return this;
      }

      var performanceId = el.data('widget-id');
      var uriBase = this.e.$table.data('widget-uri');
      var uri = uriBase.replace('{{-show-id-}}', performanceId).replace(/%d/, performanceId); // console.log(uri, performanceId);

      var initPopup = function initPopup() {
        // console.log('init');
        _this.disableScroll();

        _this.e.$iframePopup.empty();

        profTicketWidgetApi.iFrame('widget-popup', {
          src: uri
        });

        _this.e.$iframePopupWrapper.addClass('visible');
      };

      this.e.$loading.show();

      if (this.e.$iframe.length) {
        this.e.$pad.addClass('under-widget');
        this.e.$iframe.attr('src', uri).show();
      }

      if (this.e.$iframePopup.length) {
        $('.widget-close').show();

        if (typeof profTicketWidgetApi !== 'undefined') {
          initPopup();
        } else {
          if (!window.profTicketWidgetApiInitCallbacks) {
            window.profTicketWidgetApiInitCallbacks = [];
          }

          window.profTicketWidgetApiInitCallbacks.push(initPopup);
        }
      }

      return this;
    }
  }, {
    key: "setupCalendar",
    value: function setupCalendar() {
      var _this2 = this;

      if (!this.widgetNeeded()) {
        return this;
      }

      var $header = $('header.header');
      var self = this;
      this.e.$iframe.on('load', function () {
        window.scroll(0, $header.offset().top || 0);
      });
      $(document.body).on('click', this.showsSelector, function () {
        return self.loadWidget(this);
      });
      $('.widget-close, .widget-popup-wrapper .overlay').click(function () {
        if (_this2.e.$iframe.length) {
          _this2.e.$pad.removeClass('under-widget');
        }

        _this2.enableScroll();

        $('iframe#widget,.widget-close').hide();

        _this2.e.$iframePopupWrapper.removeClass('visible');
      });
      return this;
    }
  }, {
    key: "setupIFrame",
    value: function setupIFrame() {
      var _this3 = this;

      var cb = function cb(event) {
        var data = event.data || {};
        console.log('iframe', data);

        switch (data.type) {
          case 'order_step':
            _this3.sendCounterEvent(data);

            break;

          /*
          case 'finish':
            const hasOrderId = !!data.orderId;
            if (hasOrderId) {
              typeof ym !== 'undefined' &&
                this.constructor.uid.metrika &&
                ym(this.constructor.uid.metrika, 'reachGoal', 'purchase');
              typeof fbq !== 'undefined' &&
                fbq('track', 'Purchase', { value: 1.0, currency: 'RUB' });
            }
            break;
           */

          case 'document_height':
            // console.log('document_height', data.document_height);
            var height = data.document_height;
            var delta = _this3.e.$iframe.data('delta') || {};
            var device = _this3.isMobile() ? 'mobile' : 'desktop'; // const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

            var isMobileSafari = false; // isSafari && this.isMobile() &&

            var mobSafariFrameDelta = isMobileSafari ? 120 : 0;
            var mobSafariPadDelta = isMobileSafari ? -80 : 0;
            var frameDelta = ['bytimeout', 'hall-captcha'].includes(data.step) ? 150 : 0;
            delta = _objectSpread({
              pad: {
                desktop: {
                  h: 0
                },
                mobile: {
                  h: 0
                }
              },
              frame: {
                desktop: {
                  h: 0
                },
                mobile: {
                  h: 0
                }
              }
            }, delta); // console.log({ delta, mobSafariFrameDelta, mobSafariPadDelta });

            _this3.e.$pad.css({
              height: height + delta.pad[device].h + mobSafariPadDelta
            });

            _this3.e.$iframe.css({
              height: height + delta.frame[device].h + mobSafariFrameDelta + frameDelta
            });

            break;
        }
      };

      window.addEventListener('message', cb);
      return this;
    } // https://developers.google.com/analytics/devguides/collection/ga4/ecommerce?client_type=gtag

  }, {
    key: "sendCounterEvent",
    value: function sendCounterEvent(data) {
      switch (data.step) {
        case 'hall':
          this.constructor.gaGoal('view_item_list', this.convertGaData(data));
          break;

        case 'check':
          this.constructor.gaGoal('begin_checkout', this.convertGaData(data));
          break;

        case 'payment':
          this.constructor.gaGoal('add_payment_info', this.convertGaData(data));
          break;

        case 'bank':
          this.constructor.gaGoal('go_to_bank', this.convertGaData(data));
          break;

        case 'finish':
          this.constructor.yandexGoal('purchase');
          this.constructor.gaGoal('purchase', this.convertGaData(data)); // this.constructor.fbGoal('Purchase', { value: 1.0, currency: 'RUB' });

          break;

        case 'error':
          break;
      }

      return this;
    }
  }, {
    key: "convertGaData",
    value: function convertGaData(data) {
      if (!data || _typeof(data.order) !== 'object') {
        return {};
      }

      return {
        transaction_id: data.order.reservID,
        value: data.order.PlacesSum,
        currency: 'RUB',
        items: (data.order.Places || []).map(function (p) {
          return {
            item_id: [p.SectorName, p.Row, p.Seat].join('/'),
            item_name: ['Сектор ' + p.SectorName, 'Ряд ' + p.Row, 'Место ' + p.Seat, p.Comment].join(', '),
            item_brand: p.SectorName,
            item_category: 'Ряд ' + p.Row,
            item_category2: 'Место ' + p.Seat,
            item_category3: p.Comment,
            price: p.Price,
            quantity: 1
          };
        }),
        location_id: data.order.sessionID
      };
    }
  }], [{
    key: "yandexGoal",
    value: function yandexGoal(action) {
      typeof ym !== 'undefined' && this.uid.metrika && ym(this.uid.metrika, 'reachGoal', action); // console.log(['yandexGoal', action, data]);
    }
  }, {
    key: "fbGoal",
    value: function fbGoal(action, data) {
      typeof fbq !== 'undefined' && fbq('track', action, data || {}); // console.log(['fbGoal', action, data]);
    }
  }, {
    key: "gaGoal",
    value: function gaGoal(action, data) {
      typeof gtag !== 'undefined' && gtag('event', action, data || {});
      console.log(['gaGoal', action, data]);
    }
  }]);

  return diTickets;
}();

_defineProperty(diTickets, "uid", void 0);
//# sourceMappingURL=diTickets.js.map