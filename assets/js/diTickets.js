class diTickets {
  static uid;

  constructor() {
    this.showsSelector = [
      // monte + orlov
      '.calendar table tbody tr td .time .item a',
      // karenina
      'table.calendar-table tbody tr td .times a',
    ].join(',');

    this.e = {
      $table: $('.calendar table'),
      $pad: $('.list.js-tabs, nav.buy-tabs'),
      $iframe: $('iframe#widget'),
      $iframePopupWrapper: $('.widget-popup-wrapper'),
      $iframePopup: $('.widget-popup-wrapper #widget-popup'),
      $loading: $('.loading'),
      $shows: null,
    };
    this.constructor.uid = {
      metrika: $('.body-counters').data('uid-metrika'),
    };

    this.refreshShows().setupIFrame().setupCalendar().setupStartPopup();
  }

  isMobile() {
    return $(window).width() < 768;
  }

  widgetNeeded() {
    return !['staff'].includes($(document.body).data('page-type'));
  }

  refreshShows() {
    this.e.$shows = $(this.showsSelector);

    return this;
  }

  setupStartPopup() {
    if (!this.widgetNeeded()) {
      return;
    }

    const p = parse_uri_params(null, '#', ';', ':');

    //console.log('uri params:', p);

    if (typeof p.show !== 'undefined') {
      this.refreshShows();

      this.loadWidget(
        this.e.$shows.filter('[data-widget-id="{0}"]'.format(p.show))
      );
    }

    return this;
  }

  disableScroll() {
    $('html').addClass('no-scrolling');
  }

  enableScroll() {
    $('html').removeClass('no-scrolling');
  }

  loadWidget(el) {
    el = $(el);

    if ($(document.body).data('page-type') === 'home') {
      window.location.href = el.attr('href');

      return this;
    }

    const performanceId = el.data('widget-id');
    const uriBase = this.e.$table.data('widget-uri');
    const uri = uriBase
      .replace('{{-show-id-}}', performanceId)
      .replace(/%d/, performanceId);
    // console.log(uri, performanceId);

    const initPopup = () => {
      // console.log('init');
      this.disableScroll();
      this.e.$iframePopup.empty();
      profTicketWidgetApi.iFrame('widget-popup', { src: uri });
      this.e.$iframePopupWrapper.addClass('visible');
    };

    this.e.$loading.show();

    if (this.e.$iframe.length) {
      this.e.$pad.addClass('under-widget');
      this.e.$iframe.attr('src', uri).show();
    }

    if (this.e.$iframePopup.length) {
      $('.widget-close').show();

      if (typeof profTicketWidgetApi !== 'undefined') {
        initPopup();
      } else {
        if (!window.profTicketWidgetApiInitCallbacks) {
          window.profTicketWidgetApiInitCallbacks = [];
        }

        window.profTicketWidgetApiInitCallbacks.push(initPopup);
      }
    }

    return this;
  }

  setupCalendar() {
    if (!this.widgetNeeded()) {
      return this;
    }

    const $header = $('header.header');
    const self = this;

    this.e.$iframe.on('load', function () {
      window.scroll(0, $header.offset().top || 0);
    });

    $(document.body).on('click', this.showsSelector, function () {
      return self.loadWidget(this);
    });

    $('.widget-close, .widget-popup-wrapper .overlay').click(() => {
      if (this.e.$iframe.length) {
        this.e.$pad.removeClass('under-widget');
      }

      this.enableScroll();

      $('iframe#widget,.widget-close').hide();

      this.e.$iframePopupWrapper.removeClass('visible');
    });

    return this;
  }

  setupIFrame() {
    const cb = (event) => {
      const data = event.data || {};
      console.log('iframe', data);

      switch (data.type) {
        case 'order_step':
          this.sendCounterEvent(data);
          break;

        /*
        case 'finish':
          const hasOrderId = !!data.orderId;
          if (hasOrderId) {
            typeof ym !== 'undefined' &&
              this.constructor.uid.metrika &&
              ym(this.constructor.uid.metrika, 'reachGoal', 'purchase');
            typeof fbq !== 'undefined' &&
              fbq('track', 'Purchase', { value: 1.0, currency: 'RUB' });
          }
          break;
         */

        case 'document_height':
          // console.log('document_height', data.document_height);

          const height = data.document_height;
          let delta = this.e.$iframe.data('delta') || {};
          const device = this.isMobile() ? 'mobile' : 'desktop';
          // const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
          const isMobileSafari = false; // isSafari && this.isMobile() &&
          const mobSafariFrameDelta = isMobileSafari ? 120 : 0;
          const mobSafariPadDelta = isMobileSafari ? -80 : 0;
          const frameDelta = ['bytimeout', 'hall-captcha'].includes(data.step)
            ? 150
            : 0;

          delta = {
            pad: {
              desktop: {
                h: 0,
              },
              mobile: {
                h: 0,
              },
            },
            frame: {
              desktop: {
                h: 0,
              },
              mobile: {
                h: 0,
              },
            },
            ...delta,
          };

          // console.log({ delta, mobSafariFrameDelta, mobSafariPadDelta });

          this.e.$pad.css({
            height: height + delta.pad[device].h + mobSafariPadDelta,
          });
          this.e.$iframe.css({
            height:
              height + delta.frame[device].h + mobSafariFrameDelta + frameDelta,
          });
          break;
      }
    };

    window.addEventListener('message', cb);

    return this;
  }

  // https://developers.google.com/analytics/devguides/collection/ga4/ecommerce?client_type=gtag
  sendCounterEvent(data) {
    switch (data.step) {
      case 'hall':
        this.constructor.gaGoal('view_item_list', this.convertGaData(data));
        break;

      case 'check':
        this.constructor.gaGoal('begin_checkout', this.convertGaData(data));
        break;

      case 'payment':
        this.constructor.gaGoal('add_payment_info', this.convertGaData(data));
        break;

      case 'bank':
        this.constructor.gaGoal('go_to_bank', this.convertGaData(data));
        break;

      case 'finish':
        this.constructor.yandexGoal('purchase');
        this.constructor.gaGoal('purchase', this.convertGaData(data));
        // this.constructor.fbGoal('Purchase', { value: 1.0, currency: 'RUB' });
        break;

      case 'error':
        break;
    }

    return this;
  }

  convertGaData(data) {
    if (!data || typeof data.order !== 'object') {
      return {};
    }

    return {
      transaction_id: data.order.reservID,
      value: data.order.PlacesSum,
      currency: 'RUB',
      items: (data.order.Places || []).map((p) => ({
        item_id: [p.SectorName, p.Row, p.Seat].join('/'),
        item_name: [
          'Сектор ' + p.SectorName,
          'Ряд ' + p.Row,
          'Место ' + p.Seat,
          p.Comment,
        ].join(', '),
        item_brand: p.SectorName,
        item_category: 'Ряд ' + p.Row,
        item_category2: 'Место ' + p.Seat,
        item_category3: p.Comment,
        price: p.Price,
        quantity: 1,
      })),
      location_id: data.order.sessionID,
    };
  }

  static yandexGoal(action) {
    typeof ym !== 'undefined' &&
      this.uid.metrika &&
      ym(this.uid.metrika, 'reachGoal', action);
    // console.log(['yandexGoal', action, data]);
  }

  static fbGoal(action, data) {
    typeof fbq !== 'undefined' && fbq('track', action, data || {});
    // console.log(['fbGoal', action, data]);
  }

  static gaGoal(action, data) {
    typeof gtag !== 'undefined' && gtag('event', action, data || {});
    console.log(['gaGoal', action, data]);
  }
}
