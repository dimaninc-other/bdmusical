export function parse_uri_params(uri/* = location.href*/, start/* = '#'*/, delimiter/* = '&'*/, equal/* = '='*/) {
    uri = uri || window.location.href;
    start = start || '#';

    var ar = {};

    if (uri.indexOf(start) > -1) {
        uri = uri.substr(uri.indexOf(start) + 1);

        if (start == '?' && uri.indexOf('#') > -1) {
            uri = uri.substr(0, uri.indexOf('#'));
        }

        var ar2 = uri.split(delimiter || '&');

        for (var i = 0; i < ar2.length; i++) {
            var ar3 = ar2[i].split(equal || '=');

            ar[ar3[0]] = ar3[1];
        }
    }

    return ar;
}

export function setupUtm() {
    var params = parse_uri_params(null, '?');
    var endingAr = [];
    var ending, key, value;

    for (key in params) {
        value = params[key];

        if (key && key.substr(0, 4) === 'utm_') {
            endingAr.push(key + '=' + value);
        }
    }
    ending = endingAr.join('&');

    if (!ending) {
        return this;
    }

    var skipLink = function(href) {
        return !href || href.match(/^[^:]+:\/\//);
    };

    var getLinkEnding = function(glue) {
        return glue + ending;
    };

    var addLinkEnding = function(href) {
        var glue, hashIdx, href1, href2;
        glue = href.indexOf('?') === -1 ? '?' : '&';
        if ((hashIdx = href.indexOf('#')) !== -1) {
            href1 = href.substr(0, hashIdx);
            href2 = href.substr(hashIdx);
            href = href1 + getLinkEnding(glue) + href2;
        } else {
            href += getLinkEnding(glue);
        }
        return href;
    };

    $('a,[data-href]').each(function() {
        var $a, href;
        $a = $(this);
        href = $a.attr('href') || $a.data('href');

        if (skipLink(href)) {
            return true;
        }

        if ($a.attr('href')) {
            $a.attr('href', addLinkEnding(href));
        } else {
            $a.attr('data-href', addLinkEnding(href)).data('href', addLinkEnding(href));
        }
    });

    return this;
}
