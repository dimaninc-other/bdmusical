export function initTabsCalendar(obj) {
  var $tabs = $(obj);
  var $prev = $tabs.find('.arrow_prev');
  var $next = $tabs.find('.arrow_next');
  var $items = $tabs.find('.item').hide();
  var itemsLength = $items.length;

  if (itemsLength < 3) {
    $prev.hide();
    $next.hide();
  }

  $items.each(function (index) {
    $(this).attr('data-index', index);
  });

  var $active = $tabs.find('.active');
  var $activeParent = $active.closest('.item');
  var index = 0;

  function hideShowOther() {
    $items.hide();

    var $parent = $active.parent().show();
    var $prevAll = $parent.prevAll('[data-index]');
    var $nextAll = $parent.nextAll('[data-index]');
    index = parseInt($parent.attr('data-index'));

    if (index === 0) {
      $nextAll.slice(0, 2).show();
    } else if (index === 1) {
      $prevAll.slice(0, 1).show();
      $nextAll.slice(0, 1).show();
    } else if (index > 1 && index < itemsLength - 1) {
      $prevAll.slice(0, 1).show();
      $nextAll.slice(0, 1).show();
    } else if (index === itemsLength - 1) {
      $prevAll.slice(0, 2).show();
    }

    $tabs.css('display', 'table');

    if (index === 0) {
      $prev.hide();
    } else {
      if (itemsLength > 2) {
        $prev.show();
      }
    }

    if (index === $items.length - 1) {
      $next.hide();
    } else {
      if (itemsLength > 2) {
        $next.show();
      }
    }
  }

  hideShowOther();

  $tabs.on('click', '.arrow_next .link', function (e) {
    e.preventDefault();
    $items
      .filter(
        '[data-index=' +
          (index < $items.length - 1 ? index + 1 : $items.length - 1) +
          ']'
      )
      .find('.link')
      .trigger('click');
  });

  $tabs.on('click', '.arrow_prev .link', function (e) {
    e.preventDefault();
    $items
      .filter('[data-index=' + (index > 0 ? index - 1 : 0) + ']')
      .find('.link')
      .trigger('click');
  });

  $tabs.on('click', '.item .link', function (event) {
    event.preventDefault();
    $activeParent.removeClass('active2');
    $active.removeClass('active');
    var selectorOfHidden = $active.data('anchor') || $active.attr('href');
    $(selectorOfHidden).addClass('hidden');
    $active = $(this).addClass('active');
    $activeParent = $active.closest('.item');
    $activeParent.addClass('active2');
    var selectorOfVisible = $active.data('anchor') || $active.attr('href');
    $(selectorOfVisible).removeClass('hidden');
    hideShowOther();

    var url = $active.attr('href');
    if (url && $(document.body).data('page-type') === 'tickets_online') {
      history.pushState({}, '', url)
    }
  });
}
