CREATE TABLE IF NOT EXISTS season
(
    id           int unsigned NOT NULL auto_increment,
    slug         varchar(255),
    slug_source  varchar(255),
    title        varchar(255),
    content      text,
    date1        date,
    date2        date,
    `created_at` timestamp default CURRENT_TIMESTAMP,
    order_num    int,
    visible      tinyint   DEFAULT '1',
    INDEX idx (title, visible, order_num),
    UNIQUE INDEX slug_idx (slug),
    PRIMARY KEY (id)
)
    DEFAULT CHARSET = 'utf8'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;
