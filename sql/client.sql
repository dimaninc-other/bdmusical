CREATE TABLE IF NOT EXISTS client
(
    id           int unsigned not null primary key auto_increment,
    href         varchar(255),
    title        varchar(255),
    caption      varchar(255) DEFAULT '',
    content      text,
    visible      tinyint      default '1',
    en_title     varchar(255),
    en_caption   varchar(255) DEFAULT '',
    en_content   text,
    en_visible   tinyint      default '1',
    pic          varchar(40),
    pic_w        int,
    pic_h        int,
    `created_at` timestamp    default CURRENT_TIMESTAMP,
    order_num    int,
    key visible_idx (visible, en_visible, order_num)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    COLLATE = utf8_general_ci;
