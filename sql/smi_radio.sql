CREATE TABLE IF NOT EXISTS smi_radio
(
    id                  int unsigned not null auto_increment,
    old_id              int,
    slug                varchar(255),
    slug_source         varchar(255),
    season_id           INT          DEFAULT '0',
    vendor              tinyint      default '0',
    vendor_audio_uid    varchar(255) DEFAULT NULL,
    title               varchar(255),
    source              varchar(255),
    short_content       text,
    content             text,
    meta_title          varchar(255) default '',
    meta_keywords       varchar(255) default '',
    meta_description    varchar(255) default '',
    visible             tinyint      default '1',
    `en_slug`           VARCHAR(255),
    `en_slug_source`    VARCHAR(255),
    `en_title`          VARCHAR(255),
    en_source           varchar(255),
    `en_short_content`  TEXT,
    `en_content`        MEDIUMTEXT,
    en_meta_title       varchar(255) default '',
    en_meta_keywords    varchar(255) default '',
    en_meta_description varchar(255) default '',
    `en_visible`        TINYINT      DEFAULT '1',
    `embed`             text,
    views_count         int          default '0',
    pic                 varchar(50),
    pic_w               int,
    pic_h               int,
    pic_t               tinyint,
    pic_tn_w            int,
    pic_tn_h            int,
    date                datetime,
    `created_at`        timestamp    default CURRENT_TIMESTAMP,
    order_num           int,
    primary key (id),
    UNIQUE old_id_idx (old_id),
    unique slug (slug),
    UNIQUE INDEX `en_slug_idx` (`en_slug`),
    key visible_idx (`season_id`, visible, en_visible, date, created_at, order_num)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    COLLATE = utf8_general_ci;
