# [diCMS] database backup
# http://www.cadr25.ru
#
# Database: orlov
# Database Server: localhost
#
# Backup Date: 2021/05/30 12:11:26
DROP TABLE IF EXISTS `solo`;
CREATE TABLE `solo` (
	`id` int unsigned NOT NULL auto_increment,
	`old_id` int NULL,
	`character_id` int unsigned NULL DEFAULT '0',
	`slug` varchar(255) NULL,
	`slug_source` varchar(255) NULL,
	`home_css_class` varchar(100) NULL,
	`title` varchar(255) NULL,
	`short_content` text NULL,
	`content` text NULL,
	`meta_title` varchar(255) NULL,
	`meta_keywords` varchar(255) NULL,
	`meta_description` varchar(255) NULL,
	`visible` tinyint NULL DEFAULT '0',
	`active` tinyint NULL DEFAULT '1',
	`en_slug` varchar(255) NULL,
	`en_slug_source` varchar(255) NULL,
	`en_title` varchar(255) NULL,
	`en_short_content` text NULL,
	`en_content` mediumtext NULL,
	`en_meta_title` varchar(255) NULL,
	`en_meta_keywords` varchar(255) NULL,
	`en_meta_description` varchar(255) NULL,
	`en_visible` tinyint NULL DEFAULT '1',
	`en_active` tinyint NULL DEFAULT '1',
	`pic` varchar(50) NULL,
	`pic_w` int unsigned NULL DEFAULT '0',
	`pic_h` int unsigned NULL DEFAULT '0',
	`pic2` varchar(50) NULL,
	`pic2_w` int NULL DEFAULT '0',
	`pic2_h` int NULL DEFAULT '0',
	`created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	`order_num` int NULL,
	`show_pic` tinyint NULL DEFAULT '1',
	PRIMARY KEY (`id`),
	KEY `slug_idx`(`slug`),
	KEY `en_slug_idx`(`en_slug`),
	KEY `old_id_idx`(`old_id`),
	KEY `idx`(`character_id`,`title`,`visible`,`active`,`order_num`),
	KEY `en_idx`(`character_id`,`en_title`,`en_visible`,`en_active`,`order_num`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;

INSERT INTO `solo`(`id`,`old_id`,`character_id`,`slug`,`slug_source`,`home_css_class`,`title`,`short_content`,`content`,`meta_title`,`meta_keywords`,`meta_description`,`visible`,`active`,`en_slug`,`en_slug_source`,`en_title`,`en_short_content`,`en_content`,`en_meta_title`,`en_meta_keywords`,`en_meta_description`,`en_visible`,`en_active`,`pic`,`pic_w`,`pic_h`,`pic2`,`pic2_w`,`pic2_h`,`created_at`,`order_num`,`show_pic`) VALUES('1','21','1','igor-balalaev',NULL,NULL,'Игорь Балалаев',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3balalaev.png','784','750','balalaev_big.png','285','1000','2021-04-28 12:52:57','2','1'),
('2','4','1','andrey-belyavskiy',NULL,NULL,'Андрей Белявский',NULL,NULL,'','','','0','1','andrey-belyavskiy',NULL,NULL,NULL,NULL,'','','','0','1','image_2021-05-30_114853.png','940','900','image_2021-05-30_114903.png','411','1200','2021-04-28 12:52:57','3','1'),
('3','9','1','aleksandr-ragulin',NULL,NULL,'Александр Рагулин',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3ragulin.png','784','750','01_orlov_ragulin_dsc3027_2.png','318','1000','2021-04-28 12:52:57','4','1'),
('4','10','2','teona-dolnikova',NULL,NULL,'Теона Дольникова ',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3dolnikova.png','784','750','02_elizaveta_dolnikova_dsc4505_2.png','555','1000','2021-04-28 12:52:57','5','1'),
('5','11','2','valeriya-lanskaya',NULL,'lanskaya','Валерия Ланская ',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3lanskaya.png','784','750','02_elizaveta_lanskaya_dsc3970_2.png','571','1000','2021-04-28 12:52:57','6','1'),
('6','12','2','aglaya-shilovskaya',NULL,NULL,'Аглая Шиловская ',NULL,NULL,'','','','0','1','aglaya-shilovskaya',NULL,NULL,NULL,NULL,'','','','0','1','image_2021-05-30_120851.png','940','900','image_2021-05-30_120859.png','651','1200','2021-04-28 12:52:57','7','1'),
('7','13','3','ecaterina-guseva',NULL,NULL,'Екатерина Гусева ',NULL,NULL,'','','','1','1','ecaterina-guseva',NULL,NULL,NULL,NULL,'','','','1','1','3guseva.png','940','900','03_ecaterinaii_guseva_dsc8907.png','843','1200','2021-04-28 12:52:57','8','1'),
('8','14','3','lika-rulla',NULL,'Rulla','Лика Рулла ',NULL,NULL,'','','','1','1','lika-rulla',NULL,NULL,NULL,NULL,'','','','1','1','3rulla.png','940','900','03_ecaterinaii_rulla_dsc4175.png','747','1200','2021-04-28 12:52:57','9','1'),
('9','15','3','natalya-sidortsova',NULL,NULL,'Наталья Сидорцова',NULL,NULL,'','','','1','1','natalya-sidortsova',NULL,NULL,NULL,NULL,'','','','1','1','3sidorceva.png','940','900','03_ecaterinaii_sidorceva_dsc9539.png','846','1200','2021-04-28 12:52:57','10','1'),
('10','16','4','aleksandr-golubev',NULL,'golubev','Александр Голубев',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3golubev.png','703','750','04_radzivill_golubev_dsc4328_2.png','480','1000','2021-04-28 12:52:57','11','1'),
('11','17','4','vladimir-dybskiy',NULL,NULL,'Владимир Дыбский',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3dybskiy.png','723','750','04_radzivill_dybskiy_dsc4054_2.png','540','1000','2021-04-28 12:52:57','13','1'),
('12','18','4','aleksandr-marakulin',NULL,NULL,'Александр Маракулин',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3marakulin.png','784','750','04_radzivill_marakulin_dsc2608_2.png','393','1000','2021-04-28 12:52:57','12','1'),
('13','19','5','aleksandr-markelov',NULL,'Markelov','Александр Маркелов ',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3markelov.png','784','747','05_golitsyn_markelov_dsc3912.png','428','1000','2021-04-28 12:52:57','14','1'),
('14','20','5','vyacheslav-shlyahtov',NULL,'Shlyahtov','Вячеслав Шляхтов',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3shlyahtov.png','784','747','05_golitsyn_shlyahtov_dsc2905.png','349','1000','2021-04-28 12:52:57','15','1'),
('15','22','6','sergey-li',NULL,'Li','Сергей Ли',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3li.png','784','747','06_domanskiĭ_li_dsc4143.png','258','1000','2021-04-28 12:52:57','16','1'),
('16','28','6','vasiliy-remchukov',NULL,'Remchukov','Василий Ремчуков',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3remchukov.png','784','747','06_domanskiĭ_remchukov_dsc4413.png','265','1000','2021-04-28 12:52:57','17','1'),
('17','29','6','igor-balalaev-1',NULL,NULL,'Игорь Балалаев',NULL,NULL,'','','','0','1',NULL,NULL,NULL,NULL,NULL,'','','','0','1',NULL,'0','0',NULL,'0','0','2021-04-28 12:52:57','18','1'),
('18','23','7','vladislav-kiryuhin',NULL,NULL,'Владислав Кирюхин',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3kiruhin.png','703','750','07_ivan_kiruhin_dsc2834.png','341','1000','2021-04-28 12:52:57','19','1'),
('19','24','7','aleksandr-postolenko',NULL,'Postolenko','Александр Постоленко',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3postolenko.png','784','750','07_ivan_postolenko_dsc7284.png','443','1000','2021-04-28 12:52:57','20','1'),
('20','25','7','evgeniy-toloconnikov',NULL,NULL,'Евгений Толоконников',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3tolokonnikov.png','723','750','07_ivan_tolokonnikov_dsc4260.png','283','1000','2021-04-28 12:52:57','21','1'),
('21','26','8','karine-asiryan',NULL,'Asiryan','Карине Асирян',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3asiryan.png','784','750','08_tsyganka_asiryan_dsc2723.png','736','1000','2021-04-28 12:52:57','22','1'),
('22','27','8','diana-saveleva',NULL,'Savelieva','Диана Савельева',NULL,NULL,'','','','1','1',NULL,NULL,NULL,NULL,NULL,'','','','1','1','3savelieva.png','784','750','08_tsyganka_savelieva_dsc3206.png','389','1000','2021-04-28 12:52:57','23','1');

