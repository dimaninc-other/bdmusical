CREATE TABLE IF NOT EXISTS albums
(
    id                  int unsigned not null auto_increment,
    old_id              int,
    season_id           INT               DEFAULT '0',
    purpose             INT               DEFAULT 0,
    slug                varchar(255),
    slug_source         varchar(255),
    title               varchar(255),
    content             text,
    meta_title          varchar(255)      default '',
    meta_keywords       varchar(255)      default '',
    meta_description    varchar(255)      default '',
    visible             tinyint           default '1',
    top                 tinyint           default '0',
    en_slug             varchar(255),
    en_slug_source      varchar(255),
    en_title            varchar(255),
    en_content          text,
    en_meta_title       varchar(255)      default '',
    en_meta_keywords    varchar(255)      default '',
    en_meta_description varchar(255)      default '',
    en_visible          tinyint           default '1',
    en_top              tinyint           default '0',
    cover_photo_id      int               default '0',
    pic                 varchar(50)       default '',
    pic_w               int               default '0',
    pic_h               int               default '0',
    pic_t               tinyint           default '0',
    date                timestamp    NULL DEFAULT CURRENT_TIMESTAMP,
    order_num           int,
    comments_enabled    tinyint           default '1',
    comments_last_date  datetime,
    comments_count      int               default '0',
    photos_count        int               default '0',
    videos_count        int               default '0',
    views_count         int               default '0',
    UNIQUE old_id_idx (old_id),
    unique slug_idx (slug),
    unique en_slug_idx (en_slug),
    key visible_idx (`season_id`, visible, en_visible, order_num, date, photos_count, videos_count, comments_count),
    key purpose_idx (purpose),
    primary key (id)
)
    DEFAULT CHARSET = 'utf8'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;
