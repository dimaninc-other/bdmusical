CREATE TABLE IF NOT EXISTS price(
	id int unsigned not null auto_increment,
	date_type tinyint default '0',
	layout_id tinyint default '0',
	date datetime default null,
	prices text,
    `created_at` timestamp default CURRENT_TIMESTAMP,
	visible tinyint default '1',
	key visible_idx(date_type,layout_id,visible,date),
	primary key(id)
)
DEFAULT CHARSET='utf8'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
