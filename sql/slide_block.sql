CREATE TABLE IF NOT EXISTS slide_block
(
    id           int unsigned not null auto_increment,
    title        varchar(255),
    order_num    int,
    `created_at` timestamp default CURRENT_TIMESTAMP,
    key visible_idx (title, order_num),
    primary key (id)
)
    DEFAULT CHARSET = 'utf8'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;
