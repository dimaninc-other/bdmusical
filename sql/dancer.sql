CREATE TABLE IF NOT EXISTS `dancer`
(
    `id`                INT NOT NULL AUTO_INCREMENT,
    `slug`              VARCHAR(255),
    `slug_source`       VARCHAR(255),
    css_class      varchar(100),
    `title`             VARCHAR(255),
    `short_content`     TEXT,
    `content`           MEDIUMTEXT,
    meta_title          varchar(255) default '',
    meta_keywords       varchar(255) default '',
    meta_description    varchar(255) default '',
    `visible`           TINYINT      DEFAULT '1',
    `en_slug`           VARCHAR(255),
    `en_slug_source`    VARCHAR(255),
    `en_title`          VARCHAR(255),
    `en_short_content`  TEXT,
    `en_content`        MEDIUMTEXT,
    en_meta_title       varchar(255) default '',
    en_meta_keywords    varchar(255) default '',
    en_meta_description varchar(255) default '',
    `en_visible`        TINYINT      DEFAULT '1',
    `pic`               VARCHAR(50),
    `pic_w`             INT          DEFAULT '0',
    `pic_h`             INT          DEFAULT '0',
    pic2                varchar(50),
    pic2_w              int          DEFAULT '0',
    pic2_h              int          DEFAULT '0',
    `created_at`        timestamp    default CURRENT_TIMESTAMP,
    `order_num`         INT,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `slug_idx` (`slug`),
    UNIQUE INDEX `en_slug_idx` (`en_slug`),
    index idx (visible, title, order_num),
    index en_idx (en_visible, en_title, order_num)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    COLLATE = utf8_general_ci;
