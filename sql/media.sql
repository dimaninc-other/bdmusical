CREATE TABLE IF NOT EXISTS media
(
    id               int unsigned not null auto_increment,
    target_type      INT,
    target_id        INT,
    pic_type         INT,
    season_id        INT         DEFAULT '0',
    slug             varchar(255),
    title            varchar(255),
    content          text,
    short_content    TEXT,
    visible          tinyint     default '1',
    top              tinyint     default '0',
    en_slug          varchar(255),
    en_title         varchar(255),
    en_content       text,
    en_short_content TEXT,
    en_visible       tinyint     default '1',
    en_top           tinyint     default '0',
    vendor           tinyint     default '0',
    vendor_video_uid varchar(50) default '',
    pic              varchar(50) default '',
    pic_w            int         default '0',
    pic_h            int         default '0',
    date             datetime,
    `created_at`     timestamp   default CURRENT_TIMESTAMP,
    unique slug_idx (target_type, slug),
    UNIQUE INDEX `en_slug_idx` (target_type, `en_slug`),
    key visible_idx (target_type, target_id, `season_id`, visible, en_visible, top, en_top, date, created_at),
    key top_idx (top, en_top),
    primary key (id)
)
    DEFAULT CHARSET = 'utf8'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;
