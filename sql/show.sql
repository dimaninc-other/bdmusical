CREATE TABLE IF NOT EXISTS `show`
(
    id              bigint not null auto_increment,
    staff_status    tinyint      default '0',
    ticket_status   tinyint      default '0',
    staff_comment   text,
    date            datetime,
    widget_id       bigint       default '0',
    flag            tinyint      default '0',
    flag_caption    text,
    flag_href       varchar(255) default '',
    visible         tinyint      default '1',
    en_flag_caption text,
    en_flag_href    varchar(255) default '',
    en_visible      tinyint      default '1',
    revaluation     TINYINT      DEFAULT 0,
    moved           tinyint      default '0',
    qr              tinyint default '0',
    `created_at`    timestamp    default CURRENT_TIMESTAMP,
    index widget_idx (widget_id),
    key visible_idx (visible, date),
    primary key (id)
)
    DEFAULT CHARSET = 'utf8'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;
