CREATE TABLE IF NOT EXISTS show_solo_link
(
    show_id bigint,
    solo_id bigint,
    key tag_idx (show_id, solo_id)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    COLLATE = utf8_general_ci;
