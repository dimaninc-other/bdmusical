CREATE TABLE IF NOT EXISTS `character`
(
    id                  int unsigned not null auto_increment,
    home_css_class      varchar(100),
    title               varchar(255),
    `short_content`     TEXT         NOT NULL,
    `content`           MEDIUMTEXT   NOT NULL,
    meta_title          varchar(255) default '',
    meta_keywords       varchar(255) default '',
    meta_description    varchar(255) default '',
    visible             tinyint      default '1',
    en_title            varchar(255),
    `en_short_content`  TEXT         NOT NULL,
    `en_content`        MEDIUMTEXT   NOT NULL,
    en_meta_title       varchar(255) default '',
    en_meta_keywords    varchar(255) default '',
    en_meta_description varchar(255) default '',
    en_visible          tinyint      default '1',
    `created_at`        timestamp    default CURRENT_TIMESTAMP,
    order_num           int,
    key visible_idx (visible, en_visible, order_num),
    primary key (id)
)
    DEFAULT CHARSET = 'utf8'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;
