CREATE TABLE IF NOT EXISTS partner
(
    id           int unsigned not null primary key auto_increment,
    title        varchar(255),
    visible      tinyint   default '1',
    en_title     varchar(255),
    en_visible   tinyint   default '1',
    href         varchar(255),
    category_id  int,
    pic          varchar(40),
    pic_w        int,
    pic_h        int,
    `created_at` timestamp default CURRENT_TIMESTAMP,
    order_num    int,
    key visible_idx (visible, en_visible, category_id, order_num)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    COLLATE = utf8_general_ci;
