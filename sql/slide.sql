CREATE TABLE IF NOT EXISTS slide
(
    id           int unsigned not null auto_increment,
    block_id     int,
    title        varchar(255),
    href         varchar(255),
    content      text,
    visible      tinyint   default '1',
    en_title     varchar(255),
    en_href      varchar(255),
    en_content   text,
    en_visible   tinyint   default '1',
    order_num    int,
    `created_at` timestamp default CURRENT_TIMESTAMP,
    key visible_idx (block_id, visible, en_visible, order_num),
    primary key (id)
)
    DEFAULT CHARSET = 'utf8'
    COLLATE = 'utf8_general_ci'
    ENGINE = InnoDB;
